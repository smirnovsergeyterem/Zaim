package ru.zaimme.zaim.fsm;

import android.app.NotificationChannel;
import android.app.NotificationManager;
import android.app.PendingIntent;
import android.content.Context;
import android.content.Intent;
import android.graphics.BitmapFactory;
import android.graphics.Color;
import android.media.RingtoneManager;
import android.net.Uri;
import android.os.Build;
import android.support.v4.app.NotificationCompat;
import android.support.v4.content.ContextCompat;
import java.util.Objects;
import ru.zaimme.zaim.MainActivity;
import ru.zaimme.zaim.R;

public class MyNotificationManager {

    private static MyNotificationManager mInstance;

    private static Context mCtx;

    public static synchronized MyNotificationManager getInstance(Context context) {
        if (mInstance == null) {
            mInstance = new MyNotificationManager(context);
        }
        return mInstance;
    }

    private MyNotificationManager(Context context) {
        this.mCtx = context.getApplicationContext();
//        mCtx = context;
    }

    public void displayNotification(String title, String body) {
        Uri defaultSoundUri = RingtoneManager.getDefaultUri(RingtoneManager.TYPE_NOTIFICATION);

        Intent intent = new Intent(mCtx, MainActivity.class);
        intent.addFlags(Intent.FLAG_ACTIVITY_CLEAR_TOP);

        PendingIntent pendingIntent = PendingIntent.getActivity(mCtx, 0, intent,
                PendingIntent.FLAG_ONE_SHOT);

        NotificationCompat.Builder mBuilder = new NotificationCompat.Builder(mCtx,
                mCtx.getResources().getString(R.string.CHANNEL_ID))
                .setContentTitle(title)
                .setColorized(true)
                .setColor(ContextCompat.getColor(mCtx, R.color.colorPrimary))
                .setContentText(body)
                .setAutoCancel(true)
                .setPriority(NotificationCompat.PRIORITY_HIGH)
                .setSound(defaultSoundUri)
                .setContentIntent(pendingIntent);

        if (Build.VERSION.SDK_INT == Build.VERSION_CODES.KITKAT) {
            mBuilder.setSmallIcon(R.mipmap.ic_launcher);
        } else {
            mBuilder.setLargeIcon(
                    BitmapFactory.decodeResource(mCtx.getResources(), R.drawable.ic_notifications_active_black_24dp))
                    .setSmallIcon(R.drawable.ic_notifications_active_black_24dp);
        }

        NotificationManager mNotificationManager = (NotificationManager) mCtx
                .getSystemService(Context.NOTIFICATION_SERVICE);

        if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.O) {
            NotificationChannel mChannel = new NotificationChannel(mCtx.getResources().getString(R.string.CHANNEL_ID),
                    mCtx.getResources().getString(R.string.CHANNEL_NAME), NotificationManager.IMPORTANCE_HIGH);

            mChannel.setDescription(mCtx.getResources().getString(R.string.CHANNEL_DESCRIPTION));
            mChannel.enableLights(true);
            mChannel.setLightColor(Color.RED);
            mChannel.enableVibration(true);
            mChannel.setVibrationPattern(new long[]{100, 200, 300, 400, 500, 400, 300, 200, 400});

            Objects.requireNonNull(mNotificationManager).createNotificationChannel(mChannel);
        }

        if (mNotificationManager != null) {
            mNotificationManager.notify(1, mBuilder.build());
        }
    }
}
