package ru.zaimme.zaim;

import android.content.Intent;
import android.graphics.Color;
import android.os.Bundle;
import android.support.annotation.NonNull;
import android.support.design.widget.TabLayout;
import android.support.design.widget.TabLayout.OnTabSelectedListener;
import android.support.design.widget.TabLayout.Tab;
import android.support.v7.app.AppCompatActivity;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.support.v7.widget.Toolbar;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.MenuItem;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Toast;
import com.bumptech.glide.Glide;
import com.bumptech.glide.request.RequestOptions;
import com.firebase.ui.database.FirebaseRecyclerAdapter;
import com.firebase.ui.database.FirebaseRecyclerOptions;
import com.google.firebase.database.DataSnapshot;
import com.google.firebase.database.DatabaseError;
import com.google.firebase.database.DatabaseReference;
import com.google.firebase.database.FirebaseDatabase;
import com.google.firebase.database.Query;
import com.google.firebase.database.ValueEventListener;
import com.mikepenz.fontawesome_typeface_library.FontAwesome.Icon;
import com.mikepenz.iconics.IconicsDrawable;
import com.synnapps.carouselview.CarouselView;
import com.synnapps.carouselview.ImageListener;
import java.util.ArrayList;
import java.util.Objects;
import ru.zaimme.zaim.Model.OfferCategoryItem;
import ru.zaimme.zaim.Model.OfferDetailItem;
import ru.zaimme.zaim.Model.newCategory;
import ru.zaimme.zaim.ViewHolder.OfferViewHolder;

public class Loans extends AppCompatActivity {

    private static final String TAG = "Loans";

    private CarouselView carouselView;

    private final ArrayList<String> mAdsImagesList = new ArrayList<>();

    private DatabaseReference mAdsRef;

    private ImageListener mImageListener;

    private DatabaseReference mOfferDetail, mOfferCategory;

    private FirebaseRecyclerAdapter<OfferCategoryItem, OfferViewHolder> mOffersAdapter;

    private RecyclerView mRecyclerView;

    private TabLayout mTabLayout;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_loans);

        // Handle Toolbar
        Toolbar toolbar = findViewById(R.id.toolbar);
        setSupportActionBar(toolbar);
        //set the back arrow in the toolbar
        Objects.requireNonNull(getSupportActionBar()).setDisplayHomeAsUpEnabled(true);
        getSupportActionBar().setTitle("Займы");
        getSupportActionBar().setHomeAsUpIndicator(
                new IconicsDrawable(this, Icon.faw_arrow_left)
                        .color(Color.WHITE)
                        .sizeDp(IconicsDrawable.TOOLBAR_ICON_SIZE)
                        .paddingDp(IconicsDrawable.TOOLBAR_ICON_PADDING)
        );

        initFirebase();
        initCarousel();
        initRecyclerView();
        initTabs();

        //Первичное заполнение офферов и категорий в FireBase
        initValues();

        //По умолчанию первичная загрузка по названию первого таба
        getOffersByCategoryName(Objects.requireNonNull(
                Objects.requireNonNull(mTabLayout.getTabAt(mTabLayout.getSelectedTabPosition())).getText())
                .toString());
    }

    @Override
    protected void onResume() {
        super.onResume();
        if (mOffersAdapter != null) {
            mOffersAdapter.startListening();
        }
    }

    @Override
    protected void onPostResume() {
        super.onPostResume();
        carouselView.playCarousel();
    }

    @Override
    protected void onPause() {
        carouselView.pauseCarousel();
        super.onPause();
    }

    @Override
    protected void onStop() {
        super.onStop();
        if (mOffersAdapter != null) {
            mOffersAdapter.stopListening();
        }
    }

    @Override
    protected void onDestroy() {
        super.onDestroy();
        Glide.get(this).clearMemory();
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        switch (item.getItemId()) {
            case android.R.id.home:
                onBackPressed();
                return true;

            default:
                return super.onOptionsItemSelected(item);
        }
    }

    @SuppressWarnings("unused")
    private void fillOfferDetailKruglosutochno() {
        //Заполнение офферов в категорию Онлайн
        Log.d(TAG, "fillOfferDetail: Запущено заполнения OfferDetailItem");
        String key;
        ArrayList<OfferDetailItem> listOfferDetailItem = new ArrayList<>();
        ArrayList<OfferCategoryItem> listOfferCategory = new ArrayList<>();
        listOfferDetailItem
                .add(new OfferDetailItem("Описание", "https://zaimme.ru/img/moneza-logo.png", "до 30 000 руб.", "Имя",
                        "от 0,87%", "до 30 дней", "https://zaimme.ru/company/moneza_online_zayavka.php"));
        listOfferDetailItem
                .add(new OfferDetailItem("Описание", "https://zaimme.ru/img/ekapusta-logo.png", "до 30 000 руб.",
                        "Имя", "от 0%", "до 21 дня", "https://zaimme.ru/company/e_kapusta_online_zayavka.php"));
        listOfferDetailItem
                .add(new OfferDetailItem("Описание", "https://zaimme.ru/img/cr911-logo.png", "до 100 000 руб.", "Имя",
                        "2%", "до 24 недель", "https://zaimme.ru/company/cr_911_online_zayavka.php"));
        listOfferDetailItem
                .add(new OfferDetailItem("Описание", "https://zaimme.ru/img/e-zaem-logo.png", "до 15 000 руб.", "Имя",
                        "0%", "до 30 дней", "https://zaimme.ru/company/e_zaem_online_zayavka.php"));
        listOfferDetailItem
                .add(new OfferDetailItem("Описание", "https://zaimme.ru/img/moneyman-logo.png", "до 70 000 руб.",
                        "Имя", "от 0,76%", "до 4 месяцев", "https://zaimme.ru/company/moneyman_online_zayavka.php"));
        listOfferDetailItem
                .add(new OfferDetailItem("Описание", "https://zaimme.ru/img/migcredit-logo.png", "до 99 500 руб.",
                        "Имя", "от 0,27%", "до 48 недель",
                        "https://zaimme.ru/company/mig_credit_online_zayavka.php"));
        listOfferDetailItem
                .add(new OfferDetailItem("Описание", "https://zaimme.ru/img/bistrodengi-logo.png", "до 25 000 руб.",
                        "Имя", "2%", "1 месяц", "https://zaimme.ru/company/bistrodengi_online_zayavka.php"));
        listOfferDetailItem
                .add(new OfferDetailItem("Описание", "https://zaimme.ru/img/cheslovo-logo.png", "до 30 000 руб.",
                        "Имя", "1%", "до 20 дней", "https://zaimme.ru/company/chestnoe_slovo_online_zayavka.php"));
        listOfferDetailItem
                .add(new OfferDetailItem("Описание", "https://zaimme.ru/img/creditplus-logo.png", "до 15 000 руб.",
                        "Имя", "0%", "1 месяц", "https://zaimme.ru/company/credit_plus_online_zayavka.php"));
        listOfferDetailItem
                .add(new OfferDetailItem("Описание", "https://zaimme.ru/img/kredito24-logo.png", "до 15 000 руб.",
                        "Имя", "1.9%", "до 1 месяца", "https://zaimme.ru/company/kredito24_online_zayavka.php"));
        listOfferDetailItem
                .add(new OfferDetailItem("Описание", "https://zaimme.ru/img/lime-logo.png", "до 100 000 руб.", "Имя",
                        "от 0,77%", "до 168 дней", "https://zaimme.ru/company/lime_online_zayavka.php"));
        listOfferDetailItem
                .add(new OfferDetailItem("Описание", "https://zaimme.ru/img/konga-logo.png", "до 12 000 руб.", "Имя",
                        "2%", "до 30 дней", "https://zaimme.ru/company/konga_online_zayavka.php"));
        listOfferDetailItem
                .add(new OfferDetailItem("Описание", "https://zaimme.ru/img/mili-logo.png", "до 30 000 руб.", "Имя",
                        "2%", "до 1 месяца", "https://zaimme.ru/company/mili_online_zayavka.php"));
        listOfferDetailItem
                .add(new OfferDetailItem("Описание", "https://zaimme.ru/img/oneclickmoney-logo.png", "до 25 000 руб.",
                        "Имя", "2,24%", "до 21 дня", "https://zaimme.ru/company/one_click_money_online_zayavka.php"));
        listOfferDetailItem
                .add(new OfferDetailItem("Описание", "https://zaimme.ru/img/payps-logo.png", "до 11 000 руб.", "Имя",
                        "1,9%", "до 25 дней", "https://zaimme.ru/company/pay_ps_online_zayavka.php"));
        listOfferDetailItem
                .add(new OfferDetailItem("Описание", "https://zaimme.ru/img/platiza-logo.png", "до 15 000 руб.",
                        "Имя", "1%", "до 30 дней", "https://zaimme.ru/company/platiza_online_zayavka.php"));
        listOfferDetailItem
                .add(new OfferDetailItem("Описание", "https://zaimme.ru/img/smart-credit-logo.png", "до 30 000 руб.",
                        "Имя", "от 1,9%", "до 30 дней", "https://zaimme.ru/company/smart_credit_online_zayavka.php"));
        listOfferDetailItem
                .add(new OfferDetailItem("Описание", "https://zaimme.ru/img/smsfinance-logo.png", "до 30 000 руб.",
                        "Имя", "1,6%", "до 30 дней", "https://zaimme.ru/company/sms_finance_online_zayavka.php"));
        listOfferDetailItem
                .add(new OfferDetailItem("Описание", "https://zaimme.ru/img/turbozaim-logo.png", "до 15 000 руб.",
                        "Имя", "2,2%", "до 30 дней", "https://zaimme.ru/company/turbozaim_online_zayavka.php"));
        listOfferDetailItem
                .add(new OfferDetailItem("Описание", "https://zaimme.ru/img/webbankir-logo.png", "до 15 000 руб.",
                        "Имя", "от 1,2%", "до 30 дней", "https://zaimme.ru/company/webbankir_online_zayavka.php"));
        listOfferDetailItem
                .add(new OfferDetailItem("Описание", "https://zaimme.ru/img/vivus-logo.png", "до 15 000 руб.", "Имя",
                        "от 1,12%", "до 30 дней", "https://zaimme.ru/company/vivus_online_zayavka.php"));
        listOfferDetailItem
                .add(new OfferDetailItem("Описание", "https://zaimme.ru/img/zaimon-logo.png", "до 11 000 руб.", "Имя",
                        "от 1%", "до 31 дня", "https://zaimme.ru/company/zaimon_online_zayavka.php"));
        listOfferDetailItem
                .add(new OfferDetailItem("Описание", "https://zaimme.ru/img/zimer-logo.png", "до 30 000 руб.", "Имя",
                        "от 0,63%", "до 30 дней", "https://zaimme.ru/company/zaymer_online_zayavka.php"));
        for (OfferDetailItem item : listOfferDetailItem) {
            key = mOfferDetail.push().getKey();
            Log.d(TAG, "В OfferDetailItem будет добавлен узел с ключом " + key);
            listOfferCategory.add(new OfferCategoryItem(item.getDescription(), key, item.getImage(),
                    item.getMoney(), item.getName(), item.getPercent(), item.getTime(), item.getUrl()));
            mOfferDetail.child(Objects.requireNonNull(key)).setValue(item);
        }
        mOfferCategory.push().setValue(new newCategory("Круглосуточные", listOfferCategory));
        Log.d(TAG, "fillOfferDetail: Закончено заполнения OfferDetailItem");
    }

    @SuppressWarnings("unused")
    private void fillOfferDetailMgnovenney() {
        //Заполнение офферов в категорию Онлайн
        Log.d(TAG, "fillOfferDetail: Запущено заполнения OfferDetailItem");
        String key;
        ArrayList<OfferDetailItem> listOfferDetailItem = new ArrayList<>();
        ArrayList<OfferCategoryItem> listOfferCategory = new ArrayList<>();
        listOfferDetailItem
                .add(new OfferDetailItem("Описание", "https://zaimme.ru/img/moneza-logo.png", "до 30 000 руб.", "Имя",
                        "от 0,87%", "до 30 дней", "https://zaimme.ru/company/moneza_online_zayavka.php"));
        listOfferDetailItem
                .add(new OfferDetailItem("Описание", "https://zaimme.ru/img/ekapusta-logo.png", "до 30 000 руб.",
                        "Имя", "от 0%", "до 21 дня", "https://zaimme.ru/company/e_kapusta_online_zayavka.php"));
        listOfferDetailItem
                .add(new OfferDetailItem("Описание", "https://zaimme.ru/img/cr911-logo.png", "до 100 000 руб.", "Имя",
                        "2%", "до 24 недель", "https://zaimme.ru/company/cr_911_online_zayavka.php"));
        listOfferDetailItem
                .add(new OfferDetailItem("Описание", "https://zaimme.ru/img/e-zaem-logo.png", "до 15 000 руб.", "Имя",
                        "0%", "до 30 дней", "https://zaimme.ru/company/e_zaem_online_zayavka.php"));
        listOfferDetailItem
                .add(new OfferDetailItem("Описание", "https://zaimme.ru/img/moneyman-logo.png", "до 70 000 руб.",
                        "Имя", "от 0,76%", "до 4 месяцев", "https://zaimme.ru/company/moneyman_online_zayavka.php"));
        listOfferDetailItem
                .add(new OfferDetailItem("Описание", "https://zaimme.ru/img/migcredit-logo.png", "до 99 500 руб.",
                        "Имя", "от 0,27%", "до 48 недель",
                        "https://zaimme.ru/company/mig_credit_online_zayavka.php"));
        listOfferDetailItem
                .add(new OfferDetailItem("Описание", "https://zaimme.ru/img/bistrodengi-logo.png", "до 25 000 руб.",
                        "Имя", "2%", "1 месяц", "https://zaimme.ru/company/bistrodengi_online_zayavka.php"));
        listOfferDetailItem
                .add(new OfferDetailItem("Описание", "https://zaimme.ru/img/cheslovo-logo.png", "до 30 000 руб.",
                        "Имя", "1%", "до 20 дней", "https://zaimme.ru/company/chestnoe_slovo_online_zayavka.php"));
        listOfferDetailItem
                .add(new OfferDetailItem("Описание", "https://zaimme.ru/img/creditplus-logo.png", "до 15 000 руб.",
                        "Имя", "0%", "1 месяц", "https://zaimme.ru/company/credit_plus_online_zayavka.php"));
        listOfferDetailItem
                .add(new OfferDetailItem("Описание", "https://zaimme.ru/img/kredito24-logo.png", "до 15 000 руб.",
                        "Имя", "1.9%", "до 1 месяца", "https://zaimme.ru/company/kredito24_online_zayavka.php"));
        listOfferDetailItem
                .add(new OfferDetailItem("Описание", "https://zaimme.ru/img/lime-logo.png", "до 100 000 руб.", "Имя",
                        "от 0,77%", "до 168 дней", "https://zaimme.ru/company/lime_online_zayavka.php"));
        listOfferDetailItem
                .add(new OfferDetailItem("Описание", "https://zaimme.ru/img/konga-logo.png", "до 12 000 руб.", "Имя",
                        "2%", "до 30 дней", "https://zaimme.ru/company/konga_online_zayavka.php"));
        listOfferDetailItem
                .add(new OfferDetailItem("Описание", "https://zaimme.ru/img/mili-logo.png", "до 30 000 руб.", "Имя",
                        "2%", "до 1 месяца", "https://zaimme.ru/company/mili_online_zayavka.php"));
        listOfferDetailItem
                .add(new OfferDetailItem("Описание", "https://zaimme.ru/img/oneclickmoney-logo.png", "до 25 000 руб.",
                        "Имя", "2,24%", "до 21 дня", "https://zaimme.ru/company/one_click_money_online_zayavka.php"));
        listOfferDetailItem
                .add(new OfferDetailItem("Описание", "https://zaimme.ru/img/payps-logo.png", "до 11 000 руб.", "Имя",
                        "1,9%", "до 25 дней", "https://zaimme.ru/company/pay_ps_online_zayavka.php"));
        listOfferDetailItem
                .add(new OfferDetailItem("Описание", "https://zaimme.ru/img/platiza-logo.png", "до 15 000 руб.",
                        "Имя", "1%", "до 30 дней", "https://zaimme.ru/company/platiza_online_zayavka.php"));
        listOfferDetailItem
                .add(new OfferDetailItem("Описание", "https://zaimme.ru/img/smart-credit-logo.png", "до 30 000 руб.",
                        "Имя", "от 1,9%", "до 30 дней", "https://zaimme.ru/company/smart_credit_online_zayavka.php"));
        listOfferDetailItem
                .add(new OfferDetailItem("Описание", "https://zaimme.ru/img/smsfinance-logo.png", "до 30 000 руб.",
                        "Имя", "1,6%", "до 30 дней", "https://zaimme.ru/company/sms_finance_online_zayavka.php"));
        listOfferDetailItem
                .add(new OfferDetailItem("Описание", "https://zaimme.ru/img/turbozaim-logo.png", "до 15 000 руб.",
                        "Имя", "2,2%", "до 30 дней", "https://zaimme.ru/company/turbozaim_online_zayavka.php"));
        listOfferDetailItem
                .add(new OfferDetailItem("Описание", "https://zaimme.ru/img/webbankir-logo.png", "до 15 000 руб.",
                        "Имя", "от 1,2%", "до 30 дней", "https://zaimme.ru/company/webbankir_online_zayavka.php"));
        listOfferDetailItem
                .add(new OfferDetailItem("Описание", "https://zaimme.ru/img/vivus-logo.png", "до 15 000 руб.", "Имя",
                        "от 1,12%", "до 30 дней", "https://zaimme.ru/company/vivus_online_zayavka.php"));
        listOfferDetailItem
                .add(new OfferDetailItem("Описание", "https://zaimme.ru/img/zaimon-logo.png", "до 11 000 руб.", "Имя",
                        "от 1%", "до 31 дня", "https://zaimme.ru/company/zaimon_online_zayavka.php"));
        listOfferDetailItem
                .add(new OfferDetailItem("Описание", "https://zaimme.ru/img/zimer-logo.png", "до 30 000 руб.", "Имя",
                        "от 0,63%", "до 30 дней", "https://zaimme.ru/company/zaymer_online_zayavka.php"));
        for (OfferDetailItem item : listOfferDetailItem) {
            key = mOfferDetail.push().getKey();
            Log.d(TAG, "В OfferDetailItem будет добавлен узел с ключом " + key);
            listOfferCategory.add(new OfferCategoryItem(item.getDescription(), key, item.getImage(),
                    item.getMoney(), item.getName(), item.getPercent(), item.getTime(), item.getUrl()));
            mOfferDetail.child(Objects.requireNonNull(key)).setValue(item);
        }
        mOfferCategory.push().setValue(new newCategory("Мгновенные", listOfferCategory));
        Log.d(TAG, "fillOfferDetail: Закончено заполнения OfferDetailItem");
    }

    @SuppressWarnings("unused")
    private void fillOfferDetailOnline() {
        //Заполнение офферов в категорию Онлайн
        Log.d(TAG, "fillOfferDetail: Запущено заполнения OfferDetailItem");
        String key;
        ArrayList<OfferDetailItem> listOfferDetailItem = new ArrayList<>();
        ArrayList<OfferCategoryItem> listOfferCategory = new ArrayList<>();
        listOfferDetailItem
                .add(new OfferDetailItem("Описание", "https://zaimme.ru/img/moneza-logo.png", "до 30 000 руб.", "Имя",
                        "от 0,87%", "до 30 дней", "https://zaimme.ru/company/moneza_online_zayavka.php"));
        listOfferDetailItem
                .add(new OfferDetailItem("Описание", "https://zaimme.ru/img/ekapusta-logo.png", "до 30 000 руб.",
                        "Имя",
                        "от 0%", "до 21 дня", "https://zaimme.ru/company/e_kapusta_online_zayavka.php"));
        listOfferDetailItem
                .add(new OfferDetailItem("Описание", "https://zaimme.ru/img/cr911-logo.png", "до 100 000 руб.", "Имя",
                        "2%",
                        "до 24 недель", "https://zaimme.ru/company/cr_911_online_zayavka.php"));
        listOfferDetailItem
                .add(new OfferDetailItem("Описание", "https://zaimme.ru/img/e-zaem-logo.png", "до 15 000 руб.", "Имя",
                        "0%",
                        "до 30 дней", "https://zaimme.ru/company/e_zaem_online_zayavka.php"));
        listOfferDetailItem
                .add(new OfferDetailItem("Описание", "https://zaimme.ru/img/moneyman-logo.png", "до 70 000 руб.",
                        "Имя",
                        "от 0,76%", "до 4 месяцев", "https://zaimme.ru/company/moneyman_online_zayavka.php"));
        listOfferDetailItem
                .add(new OfferDetailItem("Описание", "https://zaimme.ru/img/migcredit-logo.png", "до 99 500 руб.",
                        "Имя",
                        "от 0,27%", "до 48 недель", "https://zaimme.ru/company/mig_credit_online_zayavka.php"));
        listOfferDetailItem
                .add(new OfferDetailItem("Описание", "https://zaimme.ru/img/bistrodengi-logo.png", "до 25 000 руб.",
                        "Имя",
                        "2%", "1 месяц", "https://zaimme.ru/company/bistrodengi_online_zayavka.php"));
        listOfferDetailItem
                .add(new OfferDetailItem("Описание", "https://zaimme.ru/img/cheslovo-logo.png", "до 30 000 руб.",
                        "Имя",
                        "1%",
                        "до 20 дней", "https://zaimme.ru/company/chestnoe_slovo_online_zayavka.php"));
        listOfferDetailItem
                .add(new OfferDetailItem("Описание", "https://zaimme.ru/img/creditplus-logo.png", "до 15 000 руб.",
                        "Имя",
                        "0%", "1 месяц", "https://zaimme.ru/company/credit_plus_online_zayavka.php"));
        listOfferDetailItem
                .add(new OfferDetailItem("Описание", "https://zaimme.ru/img/kredito24-logo.png", "до 15 000 руб.",
                        "Имя",
                        "1.9%", "до 1 месяца", "https://zaimme.ru/company/kredito24_online_zayavka.php"));
        listOfferDetailItem
                .add(new OfferDetailItem("Описание", "https://zaimme.ru/img/lime-logo.png", "до 100 000 руб.", "Имя",
                        "от 0,77%", "до 168 дней", "https://zaimme.ru/company/lime_online_zayavka.php"));
        listOfferDetailItem
                .add(new OfferDetailItem("Описание", "https://zaimme.ru/img/konga-logo.png", "до 12 000 руб.", "Имя",
                        "2%",
                        "до 30 дней", "https://zaimme.ru/company/konga_online_zayavka.php"));
        listOfferDetailItem
                .add(new OfferDetailItem("Описание", "https://zaimme.ru/img/mili-logo.png", "до 30 000 руб.", "Имя",
                        "2%",
                        "до 1 месяца", "https://zaimme.ru/company/mili_online_zayavka.php"));
        listOfferDetailItem
                .add(new OfferDetailItem("Описание", "https://zaimme.ru/img/oneclickmoney-logo.png", "до 25 000 руб.",
                        "Имя",
                        "2,24%", "до 21 дня", "https://zaimme.ru/company/one_click_money_online_zayavka.php"));
        listOfferDetailItem
                .add(new OfferDetailItem("Описание", "https://zaimme.ru/img/payps-logo.png", "до 11 000 руб.", "Имя",
                        "1,9%",
                        "до 25 дней", "https://zaimme.ru/company/pay_ps_online_zayavka.php"));
        listOfferDetailItem
                .add(new OfferDetailItem("Описание", "https://zaimme.ru/img/platiza-logo.png", "до 15 000 руб.",
                        "Имя",
                        "1%",
                        "до 30 дней", "https://zaimme.ru/company/platiza_online_zayavka.php"));
        listOfferDetailItem
                .add(new OfferDetailItem("Описание", "https://zaimme.ru/img/smart-credit-logo.png", "до 30 000 руб.",
                        "Имя",
                        "от 1,9%", "до 30 дней", "https://zaimme.ru/company/smart_credit_online_zayavka.php"));
        listOfferDetailItem
                .add(new OfferDetailItem("Описание", "https://zaimme.ru/img/smsfinance-logo.png", "до 30 000 руб.",
                        "Имя",
                        "1,6%", "до 30 дней", "https://zaimme.ru/company/sms_finance_online_zayavka.php"));
        listOfferDetailItem
                .add(new OfferDetailItem("Описание", "https://zaimme.ru/img/turbozaim-logo.png", "до 15 000 руб.",
                        "Имя",
                        "2,2%", "до 30 дней", "https://zaimme.ru/company/turbozaim_online_zayavka.php"));
        listOfferDetailItem
                .add(new OfferDetailItem("Описание", "https://zaimme.ru/img/webbankir-logo.png", "до 15 000 руб.",
                        "Имя",
                        "от 1,2%", "до 30 дней", "https://zaimme.ru/company/webbankir_online_zayavka.php"));
        listOfferDetailItem
                .add(new OfferDetailItem("Описание", "https://zaimme.ru/img/vivus-logo.png", "до 15 000 руб.", "Имя",
                        "от 1,12%", "до 30 дней", "https://zaimme.ru/company/vivus_online_zayavka.php"));

        listOfferDetailItem.add(
                new OfferDetailItem("Описание", "https://zaimme.ru/img/zaimon-logo.png", "до 11 000 руб.", "Имя",
                        "от 1%",
                        "до 31 дня", "https://zaimme.ru/company/zaimon_online_zayavka.php"));
        listOfferDetailItem
                .add(new OfferDetailItem("Описание", "https://zaimme.ru/img/zimer-logo.png", "до 30 000 руб.", "Имя",
                        "от 0,63%", "до 30 дней", "https://zaimme.ru/company/zaymer_online_zayavka.php"));

        for (OfferDetailItem item : listOfferDetailItem) {
            key = mOfferDetail.push().getKey();
            Log.d(TAG, "В OfferDetailItem будет добавлен узел с ключом " + key);
            listOfferCategory.add(new OfferCategoryItem(item.getDescription(), key, item.getImage(),
                    item.getMoney(), item.getName(), item.getPercent(), item.getTime(), item.getUrl()));
            mOfferDetail.child(Objects.requireNonNull(key)).setValue(item);
        }
        mOfferCategory.push().setValue(new newCategory("Онлайн", listOfferCategory));
        Log.d(TAG, "fillOfferDetail: Закончено заполнения OfferDetailItem");
    }

    @SuppressWarnings("unused")
    private void fillOfferDetail_ALL() {
        //Заполнение офферов в категорию Онлайн
        Log.d(TAG, "fillOfferDetail: Запущено заполнения OfferDetailItem");
        String key;
        ArrayList<OfferDetailItem> listOfferDetailItem = new ArrayList<>();
        ArrayList<OfferCategoryItem> listOfferCategory = new ArrayList<>();

        listOfferDetailItem
                .add(new OfferDetailItem("Описание", "https://zaimme.ru/img/zimer-logo.png", "до 30 000 руб.", "Имя",
                        "от 0,63%", "до 30 дней", "https://zaimme.ru/company/zaymer_online_zayavka.php"));
        listOfferDetailItem
                .add(new OfferDetailItem("Описание", "https://zaimme.ru/img/moneza-logo.png", "до 30 000 руб.", "Имя",
                        "от 0,87%", "до 30 дней", "https://zaimme.ru/company/moneza_online_zayavka.php"));
        listOfferDetailItem
                .add(new OfferDetailItem("Описание", "https://zaimme.ru/img/ekapusta-logo.png", "до 30 000 руб.",
                        "Имя", "от 0%", "до 21 дня", "https://zaimme.ru/company/e_kapusta_online_zayavka.php"));
        listOfferDetailItem
                .add(new OfferDetailItem("Описание", "https://zaimme.ru/img/cr911-logo.png", "до 100 000 руб.", "Имя",
                        "2%", "до 24 недель", "https://zaimme.ru/company/cr_911_online_zayavka.php"));
        listOfferDetailItem
                .add(new OfferDetailItem("Описание", "https://zaimme.ru/img/e-zaem-logo.png", "до 15 000 руб.", "Имя",
                        "0%", "до 30 дней", "https://zaimme.ru/company/e_zaem_online_zayavka.php"));
        listOfferDetailItem
                .add(new OfferDetailItem("Описание", "https://zaimme.ru/img/moneyman-logo.png", "до 70 000 руб.",
                        "Имя", "от 0,76%", "до 4 месяцев", "https://zaimme.ru/company/moneyman_online_zayavka.php"));
        listOfferDetailItem
                .add(new OfferDetailItem("Описание", "https://zaimme.ru/img/migcredit-logo.png", "до 99 500 руб.",
                        "Имя", "от 0,27%", "до 48 недель",
                        "https://zaimme.ru/company/mig_credit_online_zayavka.php"));
        listOfferDetailItem
                .add(new OfferDetailItem("Описание", "https://zaimme.ru/img/bistrodengi-logo.png", "до 25 000 руб.",
                        "Имя", "2%", "1 месяц", "https://zaimme.ru/company/bistrodengi_online_zayavka.php"));
        listOfferDetailItem
                .add(new OfferDetailItem("Описание", "https://zaimme.ru/img/cheslovo-logo.png", "до 30 000 руб.",
                        "Имя", "1%", "до 20 дней", "https://zaimme.ru/company/chestnoe_slovo_online_zayavka.php"));
        listOfferDetailItem
                .add(new OfferDetailItem("Описание", "https://zaimme.ru/img/creditplus-logo.png", "до 15 000 руб.",
                        "Имя", "0%", "1 месяц", "https://zaimme.ru/company/credit_plus_online_zayavka.php"));
        listOfferDetailItem
                .add(new OfferDetailItem("Описание", "https://zaimme.ru/img/kredito24-logo.png", "до 15 000 руб.",
                        "Имя", "1.9%", "до 1 месяца", "https://zaimme.ru/company/kredito24_online_zayavka.php"));
        listOfferDetailItem
                .add(new OfferDetailItem("Описание", "https://zaimme.ru/img/lime-logo.png", "до 100 000 руб.", "Имя",
                        "от 0,77%", "до 168 дней", "https://zaimme.ru/company/lime_online_zayavka.php"));
        listOfferDetailItem
                .add(new OfferDetailItem("Описание", "https://zaimme.ru/img/konga-logo.png", "до 12 000 руб.", "Имя",
                        "2%", "до 30 дней", "https://zaimme.ru/company/konga_online_zayavka.php"));
        listOfferDetailItem
                .add(new OfferDetailItem("Описание", "https://zaimme.ru/img/mili-logo.png", "до 30 000 руб.", "Имя",
                        "2%", "до 1 месяца", "https://zaimme.ru/company/mili_online_zayavka.php"));
        listOfferDetailItem
                .add(new OfferDetailItem("Описание", "https://zaimme.ru/img/oneclickmoney-logo.png", "до 25 000 руб.",
                        "Имя", "2,24%", "до 21 дня", "https://zaimme.ru/company/one_click_money_online_zayavka.php"));
        listOfferDetailItem
                .add(new OfferDetailItem("Описание", "https://zaimme.ru/img/payps-logo.png", "до 11 000 руб.", "Имя",
                        "1,9%", "до 25 дней", "https://zaimme.ru/company/pay_ps_online_zayavka.php"));
        listOfferDetailItem
                .add(new OfferDetailItem("Описание", "https://zaimme.ru/img/platiza-logo.png", "до 15 000 руб.",
                        "Имя", "1%", "до 30 дней", "https://zaimme.ru/company/platiza_online_zayavka.php"));
        listOfferDetailItem
                .add(new OfferDetailItem("Описание", "https://zaimme.ru/img/smart-credit-logo.png", "до 30 000 руб.",
                        "Имя", "от 1,9%", "до 30 дней", "https://zaimme.ru/company/smart_credit_online_zayavka.php"));
        listOfferDetailItem
                .add(new OfferDetailItem("Описание", "https://zaimme.ru/img/smsfinance-logo.png", "до 30 000 руб.",
                        "Имя", "1,6%", "до 30 дней", "https://zaimme.ru/company/sms_finance_online_zayavka.php"));
        listOfferDetailItem
                .add(new OfferDetailItem("Описание", "https://zaimme.ru/img/turbozaim-logo.png", "до 15 000 руб.",
                        "Имя", "2,2%", "до 30 дней", "https://zaimme.ru/company/turbozaim_online_zayavka.php"));
        listOfferDetailItem
                .add(new OfferDetailItem("Описание", "https://zaimme.ru/img/webbankir-logo.png", "до 15 000 руб.",
                        "Имя", "от 1,2%", "до 30 дней", "https://zaimme.ru/company/webbankir_online_zayavka.php"));
        listOfferDetailItem
                .add(new OfferDetailItem("Описание", "https://zaimme.ru/img/vivus-logo.png", "до 15 000 руб.", "Имя",
                        "от 1,12%", "до 30 дней", "https://zaimme.ru/company/vivus_online_zayavka.php"));
        listOfferDetailItem
                .add(new OfferDetailItem("Описание", "https://zaimme.ru/img/zaimon-logo.png", "до 11 000 руб.", "Имя",
                        "от 1%", "до 31 дня", "https://zaimme.ru/company/zaimon_online_zayavka.php"));

        for (OfferDetailItem item : listOfferDetailItem) {
            key = mOfferDetail.push().getKey();
            Log.d(TAG, "В OfferDetailItem будет добавлен узел с ключом " + key);
            listOfferCategory.add(new OfferCategoryItem(item.getDescription(), key, item.getImage(),
                    item.getMoney(), item.getName(), item.getPercent(), item.getTime(), item.getUrl()));
            mOfferDetail.child(Objects.requireNonNull(key)).setValue(item);
        }
        mOfferCategory.push().setValue(new newCategory("Все предложения", listOfferCategory));
        Log.d(TAG, "fillOfferDetail: Закончено заполнения OfferDetailItem");
    }

    @SuppressWarnings("unused")
    private void fillOfferDetail_cherez_kontakt() {
        //Заполнение офферов в категорию Онлайн
        Log.d(TAG, "fillOfferDetail: Запущено заполнения OfferDetailItem");
        String key;
        ArrayList<OfferDetailItem> listOfferDetailItem = new ArrayList<>();
        ArrayList<OfferCategoryItem> listOfferCategory = new ArrayList<>();
        listOfferDetailItem
                .add(new OfferDetailItem("Описание", "https://zaimme.ru/img/moneza-logo.png", "до 30 000 руб.", "Имя",
                        "от 0,87%", "до 30 дней", "https://zaimme.ru/company/moneza_online_zayavka.php"));
        listOfferDetailItem
                .add(new OfferDetailItem("Описание", "https://zaimme.ru/img/e-zaem-logo.png", "до 15 000 руб.", "Имя",
                        "0%", "до 30 дней", "https://zaimme.ru/company/e_zaem_online_zayavka.php"));
        listOfferDetailItem
                .add(new OfferDetailItem("Описание", "https://zaimme.ru/img/moneyman-logo.png", "до 70 000 руб.",
                        "Имя", "от 0,76%", "до 4 месяцев", "https://zaimme.ru/company/moneyman_online_zayavka.php"));
        listOfferDetailItem
                .add(new OfferDetailItem("Описание", "https://zaimme.ru/img/migcredit-logo.png", "до 99 500 руб.",
                        "Имя", "от 0,27%", "до 48 недель",
                        "https://zaimme.ru/company/mig_credit_online_zayavka.php"));
        listOfferDetailItem
                .add(new OfferDetailItem("Описание", "https://zaimme.ru/img/creditplus-logo.png", "до 15 000 руб.",
                        "Имя", "0%", "1 месяц", "https://zaimme.ru/company/credit_plus_online_zayavka.php"));
        listOfferDetailItem
                .add(new OfferDetailItem("Описание", "https://zaimme.ru/img/lime-logo.png", "до 100 000 руб.", "Имя",
                        "от 0,77%", "до 168 дней", "https://zaimme.ru/company/lime_online_zayavka.php"));
        listOfferDetailItem
                .add(new OfferDetailItem("Описание", "https://zaimme.ru/img/payps-logo.png", "до 11 000 руб.", "Имя",
                        "1,9%", "до 25 дней", "https://zaimme.ru/company/pay_ps_online_zayavka.php"));
        listOfferDetailItem
                .add(new OfferDetailItem("Описание", "https://zaimme.ru/img/platiza-logo.png", "до 15 000 руб.",
                        "Имя", "1%", "до 30 дней", "https://zaimme.ru/company/platiza_online_zayavka.php"));
        listOfferDetailItem
                .add(new OfferDetailItem("Описание", "https://zaimme.ru/img/smart-credit-logo.png", "до 30 000 руб.",
                        "Имя", "от 1,9%", "до 30 дней", "https://zaimme.ru/company/smart_credit_online_zayavka.php"));
        listOfferDetailItem
                .add(new OfferDetailItem("Описание", "https://zaimme.ru/img/smsfinance-logo.png", "до 30 000 руб.",
                        "Имя", "1,6%", "до 30 дней", "https://zaimme.ru/company/sms_finance_online_zayavka.php"));
        listOfferDetailItem
                .add(new OfferDetailItem("Описание", "https://zaimme.ru/img/webbankir-logo.png", "до 15 000 руб.",
                        "Имя", "от 1,2%", "до 30 дней", "https://zaimme.ru/company/webbankir_online_zayavka.php"));
        listOfferDetailItem
                .add(new OfferDetailItem("Описание", "https://zaimme.ru/img/vivus-logo.png", "до 15 000 руб.", "Имя",
                        "от 1,12%", "до 30 дней", "https://zaimme.ru/company/vivus_online_zayavka.php"));
        listOfferDetailItem
                .add(new OfferDetailItem("Описание", "https://zaimme.ru/img/zaimon-logo.png", "до 11 000 руб.", "Имя",
                        "от 1%", "до 31 дня", "https://zaimme.ru/company/zaimon_online_zayavka.php"));

        for (OfferDetailItem item : listOfferDetailItem) {
            key = mOfferDetail.push().getKey();
            Log.d(TAG, "В OfferDetailItem будет добавлен узел с ключом " + key);
            listOfferCategory.add(new OfferCategoryItem(item.getDescription(), key, item.getImage(),
                    item.getMoney(), item.getName(), item.getPercent(), item.getTime(), item.getUrl()));
            mOfferDetail.child(Objects.requireNonNull(key)).setValue(item);
        }
        mOfferCategory.push().setValue(new newCategory("Через Контакт", listOfferCategory));
        Log.d(TAG, "fillOfferDetail: Закончено заполнения OfferDetailItem");
    }

    @SuppressWarnings("unused")
    private void fillOfferDetail_cherez_unistream() {
        //Заполнение офферов в категорию Онлайн
        Log.d(TAG, "fillOfferDetail: Запущено заполнения OfferDetailItem");
        String key;
        ArrayList<OfferDetailItem> listOfferDetailItem = new ArrayList<>();
        ArrayList<OfferCategoryItem> listOfferCategory = new ArrayList<>();

        listOfferDetailItem
                .add(new OfferDetailItem("Описание", "https://zaimme.ru/img/moneyman-logo.png", "до 70 000 руб.",
                        "Имя", "от 0,76%", "до 4 месяцев", "https://zaimme.ru/company/moneyman_online_zayavka.php"));
        listOfferDetailItem
                .add(new OfferDetailItem("Описание", "https://zaimme.ru/img/migcredit-logo.png", "до 99 500 руб.",
                        "Имя", "от 0,27%", "до 48 недель",
                        "https://zaimme.ru/company/mig_credit_online_zayavka.php"));
        for (OfferDetailItem item : listOfferDetailItem) {
            key = mOfferDetail.push().getKey();
            Log.d(TAG, "В OfferDetailItem будет добавлен узел с ключом " + key);
            listOfferCategory.add(new OfferCategoryItem(item.getDescription(), key, item.getImage(),
                    item.getMoney(), item.getName(), item.getPercent(), item.getTime(), item.getUrl()));
            mOfferDetail.child(Objects.requireNonNull(key)).setValue(item);
        }
        mOfferCategory.push().setValue(new newCategory("Через Юнистрим", listOfferCategory));
        Log.d(TAG, "fillOfferDetail: Закончено заполнения OfferDetailItem");
    }

    @SuppressWarnings("unused")
    private void fillOfferDetail_cherez_zolotaya_korona() {
        //Заполнение офферов в категорию Онлайн
        Log.d(TAG, "fillOfferDetail: Запущено заполнения OfferDetailItem");
        String key;
        ArrayList<OfferDetailItem> listOfferDetailItem = new ArrayList<>();
        ArrayList<OfferCategoryItem> listOfferCategory = new ArrayList<>();
        listOfferDetailItem
                .add(new OfferDetailItem("Описание", "https://zaimme.ru/img/moneyman-logo.png", "до 70 000 руб.",
                        "Имя", "от 0,76%", "до 4 месяцев", "https://zaimme.ru/company/moneyman_online_zayavka.php"));
        listOfferDetailItem
                .add(new OfferDetailItem("Описание", "https://zaimme.ru/img/oneclickmoney-logo.png", "до 25 000 руб.",
                        "Имя", "2,24%", "до 21 дня", "https://zaimme.ru/company/one_click_money_online_zayavka.php"));
        listOfferDetailItem
                .add(new OfferDetailItem("Описание", "https://zaimme.ru/img/migcredit-logo.png", "до 99 500 руб.",
                        "Имя", "от 0,27%", "до 48 недель",
                        "https://zaimme.ru/company/mig_credit_online_zayavka.php"));
        listOfferDetailItem
                .add(new OfferDetailItem("Описание", "https://zaimme.ru/img/mili-logo.png", "до 30 000 руб.", "Имя",
                        "2%", "до 1 месяца", "https://zaimme.ru/company/mili_online_zayavka.php"));
        listOfferDetailItem
                .add(new OfferDetailItem("Описание", "https://zaimme.ru/img/webbankir-logo.png", "до 15 000 руб.",
                        "Имя", "от 1,2%", "до 30 дней", "https://zaimme.ru/company/webbankir_online_zayavka.php"));
        for (OfferDetailItem item : listOfferDetailItem) {
            key = mOfferDetail.push().getKey();
            Log.d(TAG, "В OfferDetailItem будет добавлен узел с ключом " + key);
            listOfferCategory.add(new OfferCategoryItem(item.getDescription(), key, item.getImage(),
                    item.getMoney(), item.getName(), item.getPercent(), item.getTime(), item.getUrl()));
            mOfferDetail.child(Objects.requireNonNull(key)).setValue(item);
        }
        mOfferCategory.push().setValue(new newCategory("Через Золотую Корону", listOfferCategory));
        Log.d(TAG, "fillOfferDetail: Закончено заполнения OfferDetailItem");
    }

    @SuppressWarnings("unused")
    private void fillOfferDetail_na_bankovskiy_schet() {
        //Заполнение офферов в категорию Онлайн
        Log.d(TAG, "fillOfferDetail: Запущено заполнения OfferDetailItem");
        String key;
        ArrayList<OfferDetailItem> listOfferDetailItem = new ArrayList<>();
        ArrayList<OfferCategoryItem> listOfferCategory = new ArrayList<>();

        listOfferDetailItem
                .add(new OfferDetailItem("Описание", "https://zaimme.ru/img/moneza-logo.png", "до 30 000 руб.", "Имя",
                        "от 0,87%", "до 30 дней", "https://zaimme.ru/company/moneza_online_zayavka.php"));
        listOfferDetailItem
                .add(new OfferDetailItem("Описание", "https://zaimme.ru/img/ekapusta-logo.png", "до 30 000 руб.",
                        "Имя", "от 0%", "до 21 дня", "https://zaimme.ru/company/e_kapusta_online_zayavka.php"));
        listOfferDetailItem
                .add(new OfferDetailItem("Описание", "https://zaimme.ru/img/cr911-logo.png", "до 100 000 руб.", "Имя",
                        "2%", "до 24 недель", "https://zaimme.ru/company/cr_911_online_zayavka.php"));
        listOfferDetailItem
                .add(new OfferDetailItem("Описание", "https://zaimme.ru/img/e-zaem-logo.png", "до 15 000 руб.", "Имя",
                        "0%", "до 30 дней", "https://zaimme.ru/company/e_zaem_online_zayavka.php"));
        listOfferDetailItem
                .add(new OfferDetailItem("Описание", "https://zaimme.ru/img/moneyman-logo.png", "до 70 000 руб.",
                        "Имя", "от 0,76%", "до 4 месяцев", "https://zaimme.ru/company/moneyman_online_zayavka.php"));
        listOfferDetailItem
                .add(new OfferDetailItem("Описание", "https://zaimme.ru/img/migcredit-logo.png", "до 99 500 руб.",
                        "Имя", "от 0,27%", "до 48 недель",
                        "https://zaimme.ru/company/mig_credit_online_zayavka.php"));
        listOfferDetailItem
                .add(new OfferDetailItem("Описание", "https://zaimme.ru/img/cheslovo-logo.png", "до 30 000 руб.",
                        "Имя", "1%", "до 20 дней", "https://zaimme.ru/company/chestnoe_slovo_online_zayavka.php"));
        listOfferDetailItem
                .add(new OfferDetailItem("Описание", "https://zaimme.ru/img/creditplus-logo.png", "до 15 000 руб.",
                        "Имя", "0%", "1 месяц", "https://zaimme.ru/company/credit_plus_online_zayavka.php"));
        listOfferDetailItem
                .add(new OfferDetailItem("Описание", "https://zaimme.ru/img/lime-logo.png", "до 100 000 руб.", "Имя",
                        "от 0,77%", "до 168 дней", "https://zaimme.ru/company/lime_online_zayavka.php"));
        listOfferDetailItem
                .add(new OfferDetailItem("Описание", "https://zaimme.ru/img/payps-logo.png", "до 11 000 руб.", "Имя",
                        "1,9%", "до 25 дней", "https://zaimme.ru/company/pay_ps_online_zayavka.php"));
        listOfferDetailItem
                .add(new OfferDetailItem("Описание", "https://zaimme.ru/img/platiza-logo.png", "до 15 000 руб.",
                        "Имя", "1%", "до 30 дней", "https://zaimme.ru/company/platiza_online_zayavka.php"));
        listOfferDetailItem
                .add(new OfferDetailItem("Описание", "https://zaimme.ru/img/smart-credit-logo.png", "до 30 000 руб.",
                        "Имя", "от 1,9%", "до 30 дней", "https://zaimme.ru/company/smart_credit_online_zayavka.php"));
        listOfferDetailItem
                .add(new OfferDetailItem("Описание", "https://zaimme.ru/img/smsfinance-logo.png", "до 30 000 руб.",
                        "Имя", "1,6%", "до 30 дней", "https://zaimme.ru/company/sms_finance_online_zayavka.php"));
        listOfferDetailItem
                .add(new OfferDetailItem("Описание", "https://zaimme.ru/img/vivus-logo.png", "до 15 000 руб.", "Имя",
                        "от 1,12%", "до 30 дней", "https://zaimme.ru/company/vivus_online_zayavka.php"));
        listOfferDetailItem
                .add(new OfferDetailItem("Описание", "https://zaimme.ru/img/zaimon-logo.png", "до 11 000 руб.", "Имя",
                        "от 1%", "до 31 дня", "https://zaimme.ru/company/zaimon_online_zayavka.php"));
        listOfferDetailItem
                .add(new OfferDetailItem("Описание", "https://zaimme.ru/img/zimer-logo.png", "до 30 000 руб.", "Имя",
                        "от 0,63%", "до 30 дней", "https://zaimme.ru/company/zaymer_online_zayavka.php"));
        for (OfferDetailItem item : listOfferDetailItem) {
            key = mOfferDetail.push().getKey();
            Log.d(TAG, "В OfferDetailItem будет добавлен узел с ключом " + key);
            listOfferCategory.add(new OfferCategoryItem(item.getDescription(), key, item.getImage(),
                    item.getMoney(), item.getName(), item.getPercent(), item.getTime(), item.getUrl()));
            mOfferDetail.child(Objects.requireNonNull(key)).setValue(item);
        }
        mOfferCategory.push().setValue(new newCategory("На банковский счет", listOfferCategory));
        Log.d(TAG, "fillOfferDetail: Закончено заполнения OfferDetailItem");
    }

    @SuppressWarnings("unused")
    private void fillOfferDetail_na_kartu() {
        //Заполнение офферов в категорию Онлайн
        Log.d(TAG, "fillOfferDetail: Запущено заполнения OfferDetailItem");
        String key;
        ArrayList<OfferDetailItem> listOfferDetailItem = new ArrayList<>();
        ArrayList<OfferCategoryItem> listOfferCategory = new ArrayList<>();
        listOfferDetailItem
                .add(new OfferDetailItem("Описание", "https://zaimme.ru/img/moneza-logo.png", "до 30 000 руб.", "Имя",
                        "от 0,87%", "до 30 дней", "https://zaimme.ru/company/moneza_online_zayavka.php"));
        listOfferDetailItem
                .add(new OfferDetailItem("Описание", "https://zaimme.ru/img/ekapusta-logo.png", "до 30 000 руб.",
                        "Имя", "от 0%", "до 21 дня", "https://zaimme.ru/company/e_kapusta_online_zayavka.php"));
        listOfferDetailItem
                .add(new OfferDetailItem("Описание", "https://zaimme.ru/img/cr911-logo.png", "до 100 000 руб.", "Имя",
                        "2%", "до 24 недель", "https://zaimme.ru/company/cr_911_online_zayavka.php"));
        listOfferDetailItem
                .add(new OfferDetailItem("Описание", "https://zaimme.ru/img/e-zaem-logo.png", "до 15 000 руб.", "Имя",
                        "0%", "до 30 дней", "https://zaimme.ru/company/e_zaem_online_zayavka.php"));
        listOfferDetailItem
                .add(new OfferDetailItem("Описание", "https://zaimme.ru/img/moneyman-logo.png", "до 70 000 руб.",
                        "Имя", "от 0,76%", "до 4 месяцев", "https://zaimme.ru/company/moneyman_online_zayavka.php"));
        listOfferDetailItem
                .add(new OfferDetailItem("Описание", "https://zaimme.ru/img/migcredit-logo.png", "до 99 500 руб.",
                        "Имя", "от 0,27%", "до 48 недель",
                        "https://zaimme.ru/company/mig_credit_online_zayavka.php"));
        listOfferDetailItem
                .add(new OfferDetailItem("Описание", "https://zaimme.ru/img/bistrodengi-logo.png", "до 25 000 руб.",
                        "Имя", "2%", "1 месяц", "https://zaimme.ru/company/bistrodengi_online_zayavka.php"));
        listOfferDetailItem
                .add(new OfferDetailItem("Описание", "https://zaimme.ru/img/cheslovo-logo.png", "до 30 000 руб.",
                        "Имя", "1%", "до 20 дней", "https://zaimme.ru/company/chestnoe_slovo_online_zayavka.php"));
        listOfferDetailItem
                .add(new OfferDetailItem("Описание", "https://zaimme.ru/img/creditplus-logo.png", "до 15 000 руб.",
                        "Имя", "0%", "1 месяц", "https://zaimme.ru/company/credit_plus_online_zayavka.php"));
        listOfferDetailItem
                .add(new OfferDetailItem("Описание", "https://zaimme.ru/img/kredito24-logo.png", "до 15 000 руб.",
                        "Имя", "1.9%", "до 1 месяца", "https://zaimme.ru/company/kredito24_online_zayavka.php"));
        listOfferDetailItem
                .add(new OfferDetailItem("Описание", "https://zaimme.ru/img/lime-logo.png", "до 100 000 руб.", "Имя",
                        "от 0,77%", "до 168 дней", "https://zaimme.ru/company/lime_online_zayavka.php"));
        listOfferDetailItem
                .add(new OfferDetailItem("Описание", "https://zaimme.ru/img/konga-logo.png", "до 12 000 руб.", "Имя",
                        "2%", "до 30 дней", "https://zaimme.ru/company/konga_online_zayavka.php"));
        listOfferDetailItem
                .add(new OfferDetailItem("Описание", "https://zaimme.ru/img/oneclickmoney-logo.png", "до 25 000 руб.",
                        "Имя", "2,24%", "до 21 дня", "https://zaimme.ru/company/one_click_money_online_zayavka.php"));
        listOfferDetailItem
                .add(new OfferDetailItem("Описание", "https://zaimme.ru/img/payps-logo.png", "до 11 000 руб.", "Имя",
                        "1,9%", "до 25 дней", "https://zaimme.ru/company/pay_ps_online_zayavka.php"));
        listOfferDetailItem
                .add(new OfferDetailItem("Описание", "https://zaimme.ru/img/platiza-logo.png", "до 15 000 руб.",
                        "Имя", "1%", "до 30 дней", "https://zaimme.ru/company/platiza_online_zayavka.php"));
        listOfferDetailItem
                .add(new OfferDetailItem("Описание", "https://zaimme.ru/img/smart-credit-logo.png", "до 30 000 руб.",
                        "Имя", "от 1,9%", "до 30 дней", "https://zaimme.ru/company/smart_credit_online_zayavka.php"));
        listOfferDetailItem
                .add(new OfferDetailItem("Описание", "https://zaimme.ru/img/smsfinance-logo.png", "до 30 000 руб.",
                        "Имя", "1,6%", "до 30 дней", "https://zaimme.ru/company/sms_finance_online_zayavka.php"));
        listOfferDetailItem
                .add(new OfferDetailItem("Описание", "https://zaimme.ru/img/turbozaim-logo.png", "до 15 000 руб.",
                        "Имя", "2,2%", "до 30 дней", "https://zaimme.ru/company/turbozaim_online_zayavka.php"));
        listOfferDetailItem
                .add(new OfferDetailItem("Описание", "https://zaimme.ru/img/webbankir-logo.png", "до 15 000 руб.",
                        "Имя", "от 1,2%", "до 30 дней", "https://zaimme.ru/company/webbankir_online_zayavka.php"));
        listOfferDetailItem
                .add(new OfferDetailItem("Описание", "https://zaimme.ru/img/vivus-logo.png", "до 15 000 руб.", "Имя",
                        "от 1,12%", "до 30 дней", "https://zaimme.ru/company/vivus_online_zayavka.php"));
        listOfferDetailItem
                .add(new OfferDetailItem("Описание", "https://zaimme.ru/img/zaimon-logo.png", "до 11 000 руб.", "Имя",
                        "от 1%", "до 31 дня", "https://zaimme.ru/company/zaimon_online_zayavka.php"));
        listOfferDetailItem
                .add(new OfferDetailItem("Описание", "https://zaimme.ru/img/zimer-logo.png", "до 30 000 руб.", "Имя",
                        "от 0,63%", "до 30 дней", "https://zaimme.ru/company/zaymer_online_zayavka.php"));
        for (OfferDetailItem item : listOfferDetailItem) {
            key = mOfferDetail.push().getKey();
            Log.d(TAG, "В OfferDetailItem будет добавлен узел с ключом " + key);
            listOfferCategory.add(new OfferCategoryItem(item.getDescription(), key, item.getImage(),
                    item.getMoney(), item.getName(), item.getPercent(), item.getTime(), item.getUrl()));
            mOfferDetail.child(Objects.requireNonNull(key)).setValue(item);
        }
        mOfferCategory.push().setValue(new newCategory("На карту", listOfferCategory));
        Log.d(TAG, "fillOfferDetail: Закончено заполнения OfferDetailItem");
    }

    @SuppressWarnings("unused")
    private void fillOfferDetail_na_qiwi_koshelek() {
        //Заполнение офферов в категорию Онлайн
        Log.d(TAG, "fillOfferDetail: Запущено заполнения OfferDetailItem");
        String key;
        ArrayList<OfferDetailItem> listOfferDetailItem = new ArrayList<>();
        ArrayList<OfferCategoryItem> listOfferCategory = new ArrayList<>();
        listOfferDetailItem
                .add(new OfferDetailItem("Описание", "https://zaimme.ru/img/ekapusta-logo.png", "до 30 000 руб.",
                        "Имя", "от 0%", "до 21 дня", "https://zaimme.ru/company/e_kapusta_online_zayavka.php"));
        listOfferDetailItem
                .add(new OfferDetailItem("Описание", "https://zaimme.ru/img/moneza-logo.png", "до 30 000 руб.", "Имя",
                        "от 0,87%", "до 30 дней", "https://zaimme.ru/company/moneza_online_zayavka.php"));
        listOfferDetailItem
                .add(new OfferDetailItem("Описание", "https://zaimme.ru/img/e-zaem-logo.png", "до 15 000 руб.", "Имя",
                        "0%", "до 30 дней", "https://zaimme.ru/company/e_zaem_online_zayavka.php"));
        listOfferDetailItem
                .add(new OfferDetailItem("Описание", "https://zaimme.ru/img/lime-logo.png", "до 100 000 руб.", "Имя",
                        "от 0,77%", "до 168 дней", "https://zaimme.ru/company/lime_online_zayavka.php"));
        listOfferDetailItem
                .add(new OfferDetailItem("Описание", "https://zaimme.ru/img/konga-logo.png", "до 12 000 руб.", "Имя",
                        "2%", "до 30 дней", "https://zaimme.ru/company/konga_online_zayavka.php"));
        listOfferDetailItem
                .add(new OfferDetailItem("Описание", "https://zaimme.ru/img/oneclickmoney-logo.png", "до 25 000 руб.",
                        "Имя", "2,24%", "до 21 дня", "https://zaimme.ru/company/one_click_money_online_zayavka.php"));
        listOfferDetailItem
                .add(new OfferDetailItem("Описание", "https://zaimme.ru/img/platiza-logo.png", "до 15 000 руб.",
                        "Имя", "1%", "до 30 дней", "https://zaimme.ru/company/platiza_online_zayavka.php"));
        listOfferDetailItem
                .add(new OfferDetailItem("Описание", "https://zaimme.ru/img/smart-credit-logo.png", "до 30 000 руб.",
                        "Имя", "от 1,9%", "до 30 дней", "https://zaimme.ru/company/smart_credit_online_zayavka.php"));
        listOfferDetailItem
                .add(new OfferDetailItem("Описание", "https://zaimme.ru/img/smsfinance-logo.png", "до 30 000 руб.",
                        "Имя", "1,6%", "до 30 дней", "https://zaimme.ru/company/sms_finance_online_zayavka.php"));
        listOfferDetailItem
                .add(new OfferDetailItem("Описание", "https://zaimme.ru/img/vivus-logo.png", "до 15 000 руб.", "Имя",
                        "от 1,12%", "до 30 дней", "https://zaimme.ru/company/vivus_online_zayavka.php"));
        listOfferDetailItem
                .add(new OfferDetailItem("Описание", "https://zaimme.ru/img/zimer-logo.png", "до 30 000 руб.", "Имя",
                        "от 0,63%", "до 30 дней", "https://zaimme.ru/company/zaymer_online_zayavka.php"));

        for (OfferDetailItem item : listOfferDetailItem) {
            key = mOfferDetail.push().getKey();
            Log.d(TAG, "В OfferDetailItem будет добавлен узел с ключом " + key);
            listOfferCategory.add(new OfferCategoryItem(item.getDescription(), key, item.getImage(),
                    item.getMoney(), item.getName(), item.getPercent(), item.getTime(), item.getUrl()));
            mOfferDetail.child(Objects.requireNonNull(key)).setValue(item);
        }
        mOfferCategory.push().setValue(new newCategory("На Киви", listOfferCategory));
        Log.d(TAG, "fillOfferDetail: Закончено заполнения OfferDetailItem");
    }

    @SuppressWarnings("unused")
    private void fillOfferDetail_na_yandex_koshelek() {
        //Заполнение офферов в категорию Онлайн
        Log.d(TAG, "fillOfferDetail: Запущено заполнения OfferDetailItem");
        String key;
        ArrayList<OfferDetailItem> listOfferDetailItem = new ArrayList<>();
        ArrayList<OfferCategoryItem> listOfferCategory = new ArrayList<>();
        listOfferDetailItem
                .add(new OfferDetailItem("Описание", "https://zaimme.ru/img/moneyman-logo.png", "до 70 000 руб.",
                        "Имя", "от 0,76%", "до 4 месяцев", "https://zaimme.ru/company/moneyman_online_zayavka.php"));
        listOfferDetailItem
                .add(new OfferDetailItem("Описание", "https://zaimme.ru/img/ekapusta-logo.png", "до 30 000 руб.",
                        "Имя", "от 0%", "до 21 дня", "https://zaimme.ru/company/e_kapusta_online_zayavka.php"));
        listOfferDetailItem
                .add(new OfferDetailItem("Описание", "https://zaimme.ru/img/lime-logo.png", "до 100 000 руб.", "Имя",
                        "от 0,77%", "до 168 дней", "https://zaimme.ru/company/lime_online_zayavka.php"));
        listOfferDetailItem
                .add(new OfferDetailItem("Описание", "https://zaimme.ru/img/konga-logo.png", "до 12 000 руб.", "Имя",
                        "2%", "до 30 дней", "https://zaimme.ru/company/konga_online_zayavka.php"));
        listOfferDetailItem
                .add(new OfferDetailItem("Описание", "https://zaimme.ru/img/payps-logo.png", "до 11 000 руб.", "Имя",
                        "1,9%", "до 25 дней", "https://zaimme.ru/company/pay_ps_online_zayavka.php"));
        listOfferDetailItem
                .add(new OfferDetailItem("Описание", "https://zaimme.ru/img/platiza-logo.png", "до 15 000 руб.",
                        "Имя", "1%", "до 30 дней", "https://zaimme.ru/company/platiza_online_zayavka.php"));
        listOfferDetailItem
                .add(new OfferDetailItem("Описание", "https://zaimme.ru/img/smart-credit-logo.png", "до 30 000 руб.",
                        "Имя", "от 1,9%", "до 30 дней", "https://zaimme.ru/company/smart_credit_online_zayavka.php"));
        listOfferDetailItem
                .add(new OfferDetailItem("Описание", "https://zaimme.ru/img/smsfinance-logo.png", "до 30 000 руб.",
                        "Имя", "1,6%", "до 30 дней", "https://zaimme.ru/company/sms_finance_online_zayavka.php"));
        listOfferDetailItem
                .add(new OfferDetailItem("Описание", "https://zaimme.ru/img/turbozaim-logo.png", "до 15 000 руб.",
                        "Имя", "2,2%", "до 30 дней", "https://zaimme.ru/company/turbozaim_online_zayavka.php"));
        listOfferDetailItem
                .add(new OfferDetailItem("Описание", "https://zaimme.ru/img/webbankir-logo.png", "до 15 000 руб.",
                        "Имя", "от 1,2%", "до 30 дней", "https://zaimme.ru/company/webbankir_online_zayavka.php"));
        listOfferDetailItem
                .add(new OfferDetailItem("Описание", "https://zaimme.ru/img/vivus-logo.png", "до 15 000 руб.", "Имя",
                        "от 1,12%", "до 30 дней", "https://zaimme.ru/company/vivus_online_zayavka.php"));
        listOfferDetailItem
                .add(new OfferDetailItem("Описание", "https://zaimme.ru/img/zaimon-logo.png", "до 11 000 руб.", "Имя",
                        "от 1%", "до 31 дня", "https://zaimme.ru/company/zaimon_online_zayavka.php"));
        listOfferDetailItem
                .add(new OfferDetailItem("Описание", "https://zaimme.ru/img/zimer-logo.png", "до 30 000 руб.", "Имя",
                        "от 0,63%", "до 30 дней", "https://zaimme.ru/company/zaymer_online_zayavka.php"));
        for (OfferDetailItem item : listOfferDetailItem) {
            key = mOfferDetail.push().getKey();
            Log.d(TAG, "В OfferDetailItem будет добавлен узел с ключом " + key);
            listOfferCategory.add(new OfferCategoryItem(item.getDescription(), key, item.getImage(),
                    item.getMoney(), item.getName(), item.getPercent(), item.getTime(), item.getUrl()));
            mOfferDetail.child(Objects.requireNonNull(key)).setValue(item);
        }
        mOfferCategory.push().setValue(new newCategory("На Яндекс", listOfferCategory));
        Log.d(TAG, "fillOfferDetail: Закончено заполнения OfferDetailItem");
    }

    @SuppressWarnings("unused")
    private void fillOfferDetail_nalichnimy() {
        //Заполнение офферов в категорию Онлайн
        Log.d(TAG, "fillOfferDetail: Запущено заполнения OfferDetailItem");
        String key;
        ArrayList<OfferDetailItem> listOfferDetailItem = new ArrayList<>();
        ArrayList<OfferCategoryItem> listOfferCategory = new ArrayList<>();
        listOfferDetailItem
                .add(new OfferDetailItem("Описание", "https://zaimme.ru/img/cr911-logo.png", "до 100 000 руб.", "Имя",
                        "2%", "до 24 недель", "https://zaimme.ru/company/cr_911_online_zayavka.php"));
        listOfferDetailItem
                .add(new OfferDetailItem("Описание", "https://zaimme.ru/img/migcredit-logo.png", "до 99 500 руб.",
                        "Имя", "от 0,27%", "до 48 недель",
                        "https://zaimme.ru/company/mig_credit_online_zayavka.php"));
        listOfferDetailItem
                .add(new OfferDetailItem("Описание", "https://zaimme.ru/img/bistrodengi-logo.png", "до 25 000 руб.",
                        "Имя", "2%", "1 месяц", "https://zaimme.ru/company/bistrodengi_online_zayavka.php"));
        listOfferDetailItem
                .add(new OfferDetailItem("Описание", "https://zaimme.ru/img/creditplus-logo.png", "до 15 000 руб.",
                        "Имя", "0%", "1 месяц", "https://zaimme.ru/company/credit_plus_online_zayavka.php"));
        listOfferDetailItem
                .add(new OfferDetailItem("Описание", "https://zaimme.ru/img/moneza-logo.png", "до 30 000 руб.", "Имя",
                        "от 0,87%", "до 30 дней", "https://zaimme.ru/company/moneza_online_zayavka.php"));
        listOfferDetailItem
                .add(new OfferDetailItem("Описание", "https://zaimme.ru/img/lime-logo.png", "до 100 000 руб.", "Имя",
                        "от 0,77%", "до 168 дней", "https://zaimme.ru/company/lime_online_zayavka.php"));
        listOfferDetailItem
                .add(new OfferDetailItem("Описание", "https://zaimme.ru/img/oneclickmoney-logo.png", "до 25 000 руб.",
                        "Имя", "2,24%", "до 21 дня", "https://zaimme.ru/company/one_click_money_online_zayavka.php"));
        listOfferDetailItem
                .add(new OfferDetailItem("Описание", "https://zaimme.ru/img/payps-logo.png", "до 11 000 руб.", "Имя",
                        "1,9%", "до 25 дней", "https://zaimme.ru/company/pay_ps_online_zayavka.php"));
        listOfferDetailItem
                .add(new OfferDetailItem("Описание", "https://zaimme.ru/img/platiza-logo.png", "до 15 000 руб.",
                        "Имя", "1%", "до 30 дней", "https://zaimme.ru/company/platiza_online_zayavka.php"));
        listOfferDetailItem
                .add(new OfferDetailItem("Описание", "https://zaimme.ru/img/smart-credit-logo.png", "до 30 000 руб.",
                        "Имя", "от 1,9%", "до 30 дней", "https://zaimme.ru/company/smart_credit_online_zayavka.php"));
        listOfferDetailItem
                .add(new OfferDetailItem("Описание", "https://zaimme.ru/img/smsfinance-logo.png", "до 30 000 руб.",
                        "Имя", "1,6%", "до 30 дней", "https://zaimme.ru/company/sms_finance_online_zayavka.php"));
        listOfferDetailItem
                .add(new OfferDetailItem("Описание", "https://zaimme.ru/img/webbankir-logo.png", "до 15 000 руб.",
                        "Имя", "от 1,2%", "до 30 дней", "https://zaimme.ru/company/webbankir_online_zayavka.php"));
        listOfferDetailItem
                .add(new OfferDetailItem("Описание", "https://zaimme.ru/img/vivus-logo.png", "до 15 000 руб.", "Имя",
                        "от 1,12%", "до 30 дней", "https://zaimme.ru/company/vivus_online_zayavka.php"));
        listOfferDetailItem
                .add(new OfferDetailItem("Описание", "https://zaimme.ru/img/zaimon-logo.png", "до 11 000 руб.", "Имя",
                        "от 1%", "до 31 дня", "https://zaimme.ru/company/zaimon_online_zayavka.php"));
        for (OfferDetailItem item : listOfferDetailItem) {
            key = mOfferDetail.push().getKey();
            Log.d(TAG, "В OfferDetailItem будет добавлен узел с ключом " + key);
            listOfferCategory.add(new OfferCategoryItem(item.getDescription(), key, item.getImage(),
                    item.getMoney(), item.getName(), item.getPercent(), item.getTime(), item.getUrl()));
            mOfferDetail.child(Objects.requireNonNull(key)).setValue(item);
        }
        mOfferCategory.push().setValue(new newCategory("Наличными", listOfferCategory));
        Log.d(TAG, "fillOfferDetail: Закончено заполнения OfferDetailItem");
    }

    @SuppressWarnings("unused")
    private void fillOfferDetail_s_plohoi_kreditnoy_istoriey() {
        //Заполнение офферов в категорию Онлайн
        Log.d(TAG, "fillOfferDetail: Запущено заполнения OfferDetailItem");
        String key;
        ArrayList<OfferDetailItem> listOfferDetailItem = new ArrayList<>();
        ArrayList<OfferCategoryItem> listOfferCategory = new ArrayList<>();

        listOfferDetailItem
                .add(new OfferDetailItem("Описание", "https://zaimme.ru/img/moneza-logo.png", "до 30 000 руб.", "Имя",
                        "от 0,87%", "до 30 дней", "https://zaimme.ru/company/moneza_online_zayavka.php"));
        listOfferDetailItem
                .add(new OfferDetailItem("Описание", "https://zaimme.ru/img/ekapusta-logo.png", "до 30 000 руб.",
                        "Имя", "от 0%", "до 21 дня", "https://zaimme.ru/company/e_kapusta_online_zayavka.php"));
        listOfferDetailItem
                .add(new OfferDetailItem("Описание", "https://zaimme.ru/img/cr911-logo.png", "до 100 000 руб.", "Имя",
                        "2%", "до 24 недель", "https://zaimme.ru/company/cr_911_online_zayavka.php"));
        listOfferDetailItem
                .add(new OfferDetailItem("Описание", "https://zaimme.ru/img/e-zaem-logo.png", "до 15 000 руб.", "Имя",
                        "0%", "до 30 дней", "https://zaimme.ru/company/e_zaem_online_zayavka.php"));
        listOfferDetailItem
                .add(new OfferDetailItem("Описание", "https://zaimme.ru/img/moneyman-logo.png", "до 70 000 руб.",
                        "Имя", "от 0,76%", "до 4 месяцев", "https://zaimme.ru/company/moneyman_online_zayavka.php"));
        listOfferDetailItem
                .add(new OfferDetailItem("Описание", "https://zaimme.ru/img/migcredit-logo.png", "до 99 500 руб.",
                        "Имя", "от 0,27%", "до 48 недель",
                        "https://zaimme.ru/company/mig_credit_online_zayavka.php"));
        listOfferDetailItem
                .add(new OfferDetailItem("Описание", "https://zaimme.ru/img/bistrodengi-logo.png", "до 25 000 руб.",
                        "Имя", "2%", "1 месяц", "https://zaimme.ru/company/bistrodengi_online_zayavka.php"));
        listOfferDetailItem
                .add(new OfferDetailItem("Описание", "https://zaimme.ru/img/cheslovo-logo.png", "до 30 000 руб.",
                        "Имя", "1%", "до 20 дней", "https://zaimme.ru/company/chestnoe_slovo_online_zayavka.php"));
        listOfferDetailItem
                .add(new OfferDetailItem("Описание", "https://zaimme.ru/img/creditplus-logo.png", "до 15 000 руб.",
                        "Имя", "0%", "1 месяц", "https://zaimme.ru/company/credit_plus_online_zayavka.php"));
        listOfferDetailItem
                .add(new OfferDetailItem("Описание", "https://zaimme.ru/img/kredito24-logo.png", "до 15 000 руб.",
                        "Имя", "1.9%", "до 1 месяца", "https://zaimme.ru/company/kredito24_online_zayavka.php"));
        listOfferDetailItem
                .add(new OfferDetailItem("Описание", "https://zaimme.ru/img/lime-logo.png", "до 100 000 руб.", "Имя",
                        "от 0,77%", "до 168 дней", "https://zaimme.ru/company/lime_online_zayavka.php"));
        listOfferDetailItem
                .add(new OfferDetailItem("Описание", "https://zaimme.ru/img/konga-logo.png", "до 12 000 руб.", "Имя",
                        "2%", "до 30 дней", "https://zaimme.ru/company/konga_online_zayavka.php"));
        listOfferDetailItem
                .add(new OfferDetailItem("Описание", "https://zaimme.ru/img/mili-logo.png", "до 30 000 руб.", "Имя",
                        "2%", "до 1 месяца", "https://zaimme.ru/company/mili_online_zayavka.php"));
        listOfferDetailItem
                .add(new OfferDetailItem("Описание", "https://zaimme.ru/img/oneclickmoney-logo.png", "до 25 000 руб.",
                        "Имя", "2,24%", "до 21 дня", "https://zaimme.ru/company/one_click_money_online_zayavka.php"));
        listOfferDetailItem
                .add(new OfferDetailItem("Описание", "https://zaimme.ru/img/payps-logo.png", "до 11 000 руб.", "Имя",
                        "1,9%", "до 25 дней", "https://zaimme.ru/company/pay_ps_online_zayavka.php"));
        listOfferDetailItem
                .add(new OfferDetailItem("Описание", "https://zaimme.ru/img/platiza-logo.png", "до 15 000 руб.",
                        "Имя", "1%", "до 30 дней", "https://zaimme.ru/company/platiza_online_zayavka.php"));
        listOfferDetailItem
                .add(new OfferDetailItem("Описание", "https://zaimme.ru/img/smart-credit-logo.png", "до 30 000 руб.",
                        "Имя", "от 1,9%", "до 30 дней", "https://zaimme.ru/company/smart_credit_online_zayavka.php"));
        listOfferDetailItem
                .add(new OfferDetailItem("Описание", "https://zaimme.ru/img/smsfinance-logo.png", "до 30 000 руб.",
                        "Имя", "1,6%", "до 30 дней", "https://zaimme.ru/company/sms_finance_online_zayavka.php"));
        listOfferDetailItem
                .add(new OfferDetailItem("Описание", "https://zaimme.ru/img/turbozaim-logo.png", "до 15 000 руб.",
                        "Имя", "2,2%", "до 30 дней", "https://zaimme.ru/company/turbozaim_online_zayavka.php"));
        listOfferDetailItem
                .add(new OfferDetailItem("Описание", "https://zaimme.ru/img/webbankir-logo.png", "до 15 000 руб.",
                        "Имя", "от 1,2%", "до 30 дней", "https://zaimme.ru/company/webbankir_online_zayavka.php"));
        listOfferDetailItem
                .add(new OfferDetailItem("Описание", "https://zaimme.ru/img/vivus-logo.png", "до 15 000 руб.", "Имя",
                        "от 1,12%", "до 30 дней", "https://zaimme.ru/company/vivus_online_zayavka.php"));
        listOfferDetailItem
                .add(new OfferDetailItem("Описание", "https://zaimme.ru/img/zaimon-logo.png", "до 11 000 руб.", "Имя",
                        "от 1%", "до 31 дня", "https://zaimme.ru/company/zaimon_online_zayavka.php"));
        listOfferDetailItem
                .add(new OfferDetailItem("Описание", "https://zaimme.ru/img/zimer-logo.png", "до 30 000 руб.", "Имя",
                        "от 0,63%", "до 30 дней", "https://zaimme.ru/company/zaymer_online_zayavka.php"));
        for (OfferDetailItem item : listOfferDetailItem) {
            key = mOfferDetail.push().getKey();
            Log.d(TAG, "В OfferDetailItem будет добавлен узел с ключом " + key);
            listOfferCategory.add(new OfferCategoryItem(item.getDescription(), key, item.getImage(),
                    item.getMoney(), item.getName(), item.getPercent(), item.getTime(), item.getUrl()));
            mOfferDetail.child(Objects.requireNonNull(key)).setValue(item);
        }
        mOfferCategory.push().setValue(new newCategory("С плохой кредитной историей", listOfferCategory));
        Log.d(TAG, "fillOfferDetail: Закончено заполнения OfferDetailItem");
    }

    @SuppressWarnings("unused")
    private void fillOfferDetailbesprocentnie() {
        //Заполнение офферов в категорию Онлайн
        Log.d(TAG, "fillOfferDetail: Запущено заполнения OfferDetailItem");
        String key;
        ArrayList<OfferDetailItem> listOfferDetailItem = new ArrayList<>();
        ArrayList<OfferCategoryItem> listOfferCategory = new ArrayList<>();
        listOfferDetailItem
                .add(new OfferDetailItem("Описание", "https://zaimme.ru/img/ekapusta-logo.png", "до 30 000 руб.",
                        "Имя", "0%", "до 21 дня", "https://zaimme.ru/company/e_kapusta_online_zayavka.php"));
        listOfferDetailItem
                .add(new OfferDetailItem("Описание", "https://zaimme.ru/img/e-zaem-logo.png", "до 15 000 руб.", "Имя",
                        "0%", "до 30 дней", "https://zaimme.ru/company/e_zaem_online_zayavka.php"));
        listOfferDetailItem
                .add(new OfferDetailItem("Описание", "https://zaimme.ru/img/creditplus-logo.png", "до 15 000 руб.",
                        "Имя", "0%", "1 месяц", "https://zaimme.ru/company/credit_plus_online_zayavka.php"));
        listOfferDetailItem
                .add(new OfferDetailItem("Описание", "https://zaimme.ru/img/lime-logo.png", "до 20 400 руб.", "Имя",
                        "0%", "до 5 дней", "https://zaimme.ru/company/lime_online_zayavka.php"));
        listOfferDetailItem
                .add(new OfferDetailItem("Описание", "https://zaimme.ru/img/moneza-logo.png", "до 10 000 руб.", "Имя",
                        "0%", "до 30 дней", "https://zaimme.ru/company/moneza_online_zayavka.php"));

        for (OfferDetailItem item : listOfferDetailItem) {
            key = mOfferDetail.push().getKey();
            Log.d(TAG, "В OfferDetailItem будет добавлен узел с ключом " + key);
            listOfferCategory.add(new OfferCategoryItem(item.getDescription(), key, item.getImage(),
                    item.getMoney(), item.getName(), item.getPercent(), item.getTime(), item.getUrl()));
            mOfferDetail.child(Objects.requireNonNull(key)).setValue(item);
        }
        mOfferCategory.push().setValue(new newCategory("Беспроцентные", listOfferCategory));
        Log.d(TAG, "fillOfferDetail: Закончено заполнения OfferDetailItem");
    }

    @SuppressWarnings("unused")
    private void fillOfferDetailbez_proverok() {
        //Заполнение офферов в категорию Онлайн
        Log.d(TAG, "fillOfferDetail: Запущено заполнения OfferDetailItem");
        String key;
        ArrayList<OfferDetailItem> listOfferDetailItem = new ArrayList<>();
        ArrayList<OfferCategoryItem> listOfferCategory = new ArrayList<>();
        listOfferDetailItem
                .add(new OfferDetailItem("Описание", "https://zaimme.ru/img/zimer-logo.png", "до 30 000 руб.", "Имя",
                        "от 0,63%", "до 30 дней", "https://zaimme.ru/company/zaymer_online_zayavka.php"));
        listOfferDetailItem
                .add(new OfferDetailItem("Описание", "https://zaimme.ru/img/moneyman-logo.png", "до 70 000 руб.",
                        "Имя", "от 0,76%", "до 4 месяцев", "https://zaimme.ru/company/moneyman_online_zayavka.php"));
        listOfferDetailItem
                .add(new OfferDetailItem("Описание", "https://zaimme.ru/img/payps-logo.png", "до 11 000 руб.", "Имя",
                        "1,9%", "до 25 дней", "https://zaimme.ru/company/pay_ps_online_zayavka.php"));
        listOfferDetailItem
                .add(new OfferDetailItem("Описание", "https://zaimme.ru/img/turbozaim-logo.png", "до 15 000 руб.",
                        "Имя", "2,2%", "до 30 дней", "https://zaimme.ru/company/turbozaim_online_zayavka.php"));
        listOfferDetailItem
                .add(new OfferDetailItem("Описание", "https://zaimme.ru/img/kredito24-logo.png", "до 15 000 руб.",
                        "Имя", "1.9%", "до 1 месяца", "https://zaimme.ru/company/kredito24_online_zayavka.php"));
        listOfferDetailItem
                .add(new OfferDetailItem("Описание", "https://zaimme.ru/img/platiza-logo.png", "до 15 000 руб.",
                        "Имя", "1%", "до 30 дней", "https://zaimme.ru/company/platiza_online_zayavka.php"));
        listOfferDetailItem
                .add(new OfferDetailItem("Описание", "https://zaimme.ru/img/e-zaem-logo.png", "до 15 000 руб.", "Имя",
                        "0%", "до 30 дней", "https://zaimme.ru/company/e_zaem_online_zayavka.php"));
        listOfferDetailItem
                .add(new OfferDetailItem("Описание", "https://zaimme.ru/img/webbankir-logo.png", "до 15 000 руб.",
                        "Имя", "от 1,2%", "до 30 дней", "https://zaimme.ru/company/webbankir_online_zayavka.php"));
        listOfferDetailItem
                .add(new OfferDetailItem("Описание", "https://zaimme.ru/img/moneza-logo.png", "до 30 000 руб.", "Имя",
                        "от 0,87%", "до 30 дней", "https://zaimme.ru/company/moneza_online_zayavka.php"));
        listOfferDetailItem
                .add(new OfferDetailItem("Описание", "https://zaimme.ru/img/cr911-logo.png", "до 100 000 руб.", "Имя",
                        "2%", "до 24 недель", "https://zaimme.ru/company/cr_911_online_zayavka.php"));
        listOfferDetailItem
                .add(new OfferDetailItem("Описание", "https://zaimme.ru/img/migcredit-logo.png", "до 99 500 руб.",
                        "Имя", "от 0,27%", "до 48 недель",
                        "https://zaimme.ru/company/mig_credit_online_zayavka.php"));
        listOfferDetailItem
                .add(new OfferDetailItem("Описание", "https://zaimme.ru/img/smsfinance-logo.png", "до 30 000 руб.",
                        "Имя", "1,6%", "до 30 дней", "https://zaimme.ru/company/sms_finance_online_zayavka.php"));
        listOfferDetailItem
                .add(new OfferDetailItem("Описание", "https://zaimme.ru/img/ekapusta-logo.png", "до 30 000 руб.",
                        "Имя", "от 0%", "до 21 дня", "https://zaimme.ru/company/e_kapusta_online_zayavka.php"));
        listOfferDetailItem
                .add(new OfferDetailItem("Описание", "https://zaimme.ru/img/creditplus-logo.png", "до 15 000 руб.",
                        "Имя", "0%", "1 месяц", "https://zaimme.ru/company/credit_plus_online_zayavka.php"));
        listOfferDetailItem
                .add(new OfferDetailItem("Описание", "https://zaimme.ru/img/vivus-logo.png", "до 15 000 руб.", "Имя",
                        "от 1,12%", "до 30 дней", "https://zaimme.ru/company/vivus_online_zayavka.php"));
        listOfferDetailItem
                .add(new OfferDetailItem("Описание", "https://zaimme.ru/img/smart-credit-logo.png", "до 30 000 руб.",
                        "Имя", "от 1,9%", "до 30 дней", "https://zaimme.ru/company/smart_credit_online_zayavka.php"));
        listOfferDetailItem
                .add(new OfferDetailItem("Описание", "https://zaimme.ru/img/konga-logo.png", "до 12 000 руб.", "Имя",
                        "2%", "до 30 дней", "https://zaimme.ru/company/konga_online_zayavka.php"));
        listOfferDetailItem
                .add(new OfferDetailItem("Описание", "https://zaimme.ru/img/mili-logo.png", "до 30 000 руб.", "Имя",
                        "2%", "до 1 месяца", "https://zaimme.ru/company/mili_online_zayavka.php"));
        listOfferDetailItem
                .add(new OfferDetailItem("Описание", "https://zaimme.ru/img/oneclickmoney-logo.png", "до 25 000 руб.",
                        "Имя", "2,24%", "до 21 дня", "https://zaimme.ru/company/one_click_money_online_zayavka.php"));
        listOfferDetailItem
                .add(new OfferDetailItem("Описание", "https://zaimme.ru/img/cheslovo-logo.png", "до 30 000 руб.",
                        "Имя", "1%", "до 20 дней", "https://zaimme.ru/company/chestnoe_slovo_online_zayavka.php"));
        listOfferDetailItem
                .add(new OfferDetailItem("Описание", "https://zaimme.ru/img/lime-logo.png", "до 100 000 руб.", "Имя",
                        "от 0,77%", "до 168 дней", "https://zaimme.ru/company/lime_online_zayavka.php"));
        listOfferDetailItem
                .add(new OfferDetailItem("Описание", "https://zaimme.ru/img/zaimon-logo.png", "до 11 000 руб.", "Имя",
                        "от 1%", "до 31 дня", "https://zaimme.ru/company/zaimon_online_zayavka.php"));
        listOfferDetailItem
                .add(new OfferDetailItem("Описание", "https://zaimme.ru/img/bistrodengi-logo.png", "до 25 000 руб.",
                        "Имя", "2%", "1 месяц", "https://zaimme.ru/company/bistrodengi_online_zayavka.php"));
        for (OfferDetailItem item : listOfferDetailItem) {
            key = mOfferDetail.push().getKey();
            Log.d(TAG, "В OfferDetailItem будет добавлен узел с ключом " + key);
            listOfferCategory.add(new OfferCategoryItem(item.getDescription(), key, item.getImage(),
                    item.getMoney(), item.getName(), item.getPercent(), item.getTime(), item.getUrl()));
            mOfferDetail.child(Objects.requireNonNull(key)).setValue(item);
        }
        mOfferCategory.push().setValue(new newCategory("Без проверок", listOfferCategory));
        Log.d(TAG, "fillOfferDetail: Закончено заполнения OfferDetailItem");
    }

    private void getAdsImages() {
        mAdsRef.addListenerForSingleValueEvent(new ValueEventListener() {
            @Override
            public void onCancelled(@NonNull final DatabaseError databaseError) {
                Log.e(TAG, "onCancelled: ", databaseError.toException());
            }

            @Override
            public void onDataChange(@NonNull final DataSnapshot dataSnapshot) {
                for (final DataSnapshot item : dataSnapshot.getChildren()) {
                    mAdsImagesList.add(Objects.requireNonNull(item.getValue()).toString());
                }
                carouselView.setPageCount(mAdsImagesList.size());
                mImageListener = (position, imageView) -> Glide.with(getBaseContext())
                        .load(mAdsImagesList.toArray()[position])
                        .into(imageView);
                carouselView.setImageListener(mImageListener);
            }
        });
    }

    private void getOffersByCategoryName(final String categoryName) {
        mOfferCategory.orderByChild("name").equalTo(categoryName)
                .addListenerForSingleValueEvent(new ValueEventListener() {
                    @Override
                    public void onCancelled(@NonNull final DatabaseError databaseError) {

                    }

                    @Override
                    public void onDataChange(@NonNull final DataSnapshot dataSnapshot) {
                        if (dataSnapshot.getValue() != null) {
                            for (DataSnapshot item : dataSnapshot.getChildren()) {
                                Query offersQuery = item.getRef().child("offers").orderByKey();
                                FirebaseRecyclerOptions<OfferCategoryItem> listOptions
                                        = new FirebaseRecyclerOptions.Builder<OfferCategoryItem>()
                                        .setQuery(offersQuery, OfferCategoryItem.class)
                                        .build();
                                mOffersAdapter = new FirebaseRecyclerAdapter<OfferCategoryItem, OfferViewHolder>(
                                        listOptions) {
                                    @NonNull
                                    @Override
                                    public OfferViewHolder onCreateViewHolder(@NonNull final ViewGroup parent,
                                            final int viewType) {
                                        View itemView = LayoutInflater.from(parent.getContext())
                                                .inflate(R.layout.offer_item, parent, false);
                                        return new OfferViewHolder(itemView);
                                    }

                                    @Override
                                    protected void onBindViewHolder(@NonNull final OfferViewHolder viewHolder,
                                            final int position,
                                            @NonNull final OfferCategoryItem model) {
                                        viewHolder.txt_money.setText(model.getMoney());
                                        viewHolder.txt_offer_name.setText(model.getName());
                                        viewHolder.txt_percent.setText(model.getPercent());
                                        viewHolder.txt_time.setText(model.getTime());

                                        Glide.with(getBaseContext())
                                                .load(model.getImage())
                                                .apply(new RequestOptions().fitCenter())
                                                .into(viewHolder.img_offer_logo);

                                        viewHolder.img_more.setOnClickListener(v -> {
                                            Intent offerDetailIntent = new Intent(Loans.this, OfferDetail.class);
                                            offerDetailIntent.putExtra("offerId", model.getId());
                                            startActivity(offerDetailIntent);
                                        });

                                        viewHolder.setItemClickListener(
                                                (View view, int position1, boolean isLongClick) -> Toast
                                                        .makeText(Loans.this,
                                                                "Был выбран оффер: " + mOffersAdapter.getItem(
                                                                        position1).getName(), Toast.LENGTH_SHORT)
                                                        .show());
                                    }
                                };
                                mOffersAdapter.startListening();
                                mRecyclerView.setAdapter(mOffersAdapter);
                            }

                        }
                    }
                });
    }

    private void initCarousel() {
        //Carousel
        carouselView = findViewById(R.id.carousel);
        mImageListener = (position, imageView) -> Glide.with(getBaseContext())
                .load(mAdsImagesList.toArray()[position])
                .into(imageView);
        carouselView.setImageListener(mImageListener);
        carouselView.setImageClickListener(
                position -> Toast.makeText(Loans.this, "Выбран элемент: " + position, Toast.LENGTH_SHORT).show());
        getAdsImages();
    }

    private void initFirebase() {
        //Firebase Init
        final FirebaseDatabase database = FirebaseDatabase.getInstance();
        mAdsRef = database.getReference("Ads");
        mOfferDetail = database.getReference("OfferDetail");
        mOfferCategory = database.getReference("OfferCategory");
    }

    private void initRecyclerView() {
        //Recycler
        mRecyclerView = findViewById(R.id.loansList);
        mRecyclerView.setHasFixedSize(true);
        RecyclerView.LayoutManager layoutManager = new LinearLayoutManager(this);
        mRecyclerView.setLayoutManager(layoutManager);
    }

    private void initTabs() {
        mTabLayout = findViewById(R.id.tabs);
        mTabLayout.addOnTabSelectedListener(new OnTabSelectedListener() {
            @Override
            public void onTabReselected(final Tab tab) {
            }

            @Override
            public void onTabSelected(final Tab tab) {
                //Загружаем офферы по названию выбранного таба
                if (tab.getText() != null) {
                    getOffersByCategoryName(tab.getText().toString());
                }
            }

            @Override
            public void onTabUnselected(final Tab tab) {
            }
        });
    }

    @SuppressWarnings("EmptyMethod")
    private void initValues() {
         /*
        Заполнение офферов
        fillOfferDetailOnline();
        fillOfferDetailMgnovenney();
        fillOfferDetailbesprocentnie();
        fillOfferDetailKruglosutochno();
        fillOfferDetailbez_proverok();
        fillOfferDetail_s_plohoi_kreditnoy_istoriey();
        fillOfferDetail_na_kartu();
        fillOfferDetail_na_qiwi_koshelek();
        fillOfferDetail_na_yandex_koshelek();
        fillOfferDetail_na_bankovskiy_schet();
        fillOfferDetail_nalichnimy();
        fillOfferDetail_cherez_kontakt();
        fillOfferDetail_cherez_zolotaya_korona();
        fillOfferDetail_cherez_unistream();
        fillOfferDetail_ALL();
         */

    }

}
