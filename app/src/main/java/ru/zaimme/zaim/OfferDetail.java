package ru.zaimme.zaim;

import android.content.Intent;
import android.graphics.Color;
import android.net.Uri;
import android.os.Bundle;
import android.support.annotation.NonNull;
import android.support.v7.app.AppCompatActivity;
import android.support.v7.widget.Toolbar;
import android.util.Log;
import android.view.MenuItem;
import android.view.View;
import android.widget.Button;
import android.widget.ImageView;
import android.widget.TextView;
import android.widget.Toast;
import com.bumptech.glide.Glide;
import com.bumptech.glide.request.RequestOptions;
import com.google.firebase.database.DataSnapshot;
import com.google.firebase.database.DatabaseError;
import com.google.firebase.database.DatabaseReference;
import com.google.firebase.database.FirebaseDatabase;
import com.google.firebase.database.ValueEventListener;
import com.mikepenz.fontawesome_typeface_library.FontAwesome.Icon;
import com.mikepenz.iconics.IconicsDrawable;
import java.util.Objects;
import ru.zaimme.zaim.Model.OfferDetailItem;

public class OfferDetail extends AppCompatActivity {

    private static final String TAG = "OfferDetail";

    private Button btn_request;

    private ImageView img_logo;

    private DatabaseReference mOfferDetail;

    private String offerId;

    private TextView txt_offer_name, txt_money, txt_percent, txt_time;

    private TextView txt_title, txt_text;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_offer_detail);

        // Handle Toolbar
        Toolbar toolbar = findViewById(R.id.toolbar);
        setSupportActionBar(toolbar);
        //set the back arrow in the toolbar
        Objects.requireNonNull(getSupportActionBar()).setDisplayHomeAsUpEnabled(true);
        getSupportActionBar().setTitle("");
        getSupportActionBar().setHomeAsUpIndicator(
                new IconicsDrawable(this, Icon.faw_arrow_left)
                        .color(Color.WHITE)
                        .sizeDp(IconicsDrawable.TOOLBAR_ICON_SIZE)
                        .paddingDp(IconicsDrawable.TOOLBAR_ICON_PADDING)
        );

        initFirebase();
        initUI();
        initListeners();
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        switch (item.getItemId()) {
            case android.R.id.home:
                onBackPressed();
                return true;

            default:
                return super.onOptionsItemSelected(item);
        }
    }

    public void openWebPage(String url) {
        Uri webpage = Uri.parse(url);
        Intent intent = new Intent(Intent.ACTION_VIEW, webpage);
        if (intent.resolveActivity(getPackageManager()) != null) {
            startActivity(intent);
        }
    }

    private void initFirebase() {
        //Firebase Init
        final FirebaseDatabase database = FirebaseDatabase.getInstance();
        mOfferDetail = database.getReference("OfferDetail");
    }

    private void initListeners() {
        //Firebase Listener
        mOfferDetail.orderByKey().equalTo(offerId).addListenerForSingleValueEvent(new ValueEventListener() {
            @Override
            public void onCancelled(@NonNull final DatabaseError databaseError) {
                Toast.makeText(OfferDetail.this, databaseError.getMessage(), Toast.LENGTH_SHORT).show();
            }

            @Override
            public void onDataChange(@NonNull final DataSnapshot dataSnapshot) {
                try {
                    OfferDetailItem offer = new OfferDetailItem();
                    if (dataSnapshot != null) {
                        for (DataSnapshot item : dataSnapshot.getChildren()) {
                            offer = item.getValue(OfferDetailItem.class);
                            final OfferDetailItem finalOffer = offer;
                            //Заголовок
                            txt_title.setText(Objects.requireNonNull(offer).getName());
                            getSupportActionBar().setTitle(Objects.requireNonNull(offer).getName());
                            txt_text.setText(offer.getDescription());

                            //Карточка
                            txt_offer_name.setText(offer.getName());
                            txt_money.setText(offer.getMoney());
                            txt_percent.setText(offer.getPercent());
                            txt_time.setText(offer.getTime());
                            Glide.with(getBaseContext())
                                    .load(offer.getImage())
                                    .apply(new RequestOptions().fitCenter())
                                    .into(img_logo);

                            btn_request.setOnClickListener(v -> openWebPage(Objects.requireNonNull(finalOffer).getUrl()));
                        }
                    }
                } catch (Exception e) {
                    String msg = "Ошибка при загрузке оффера с ID=" + offerId;
                    Log.e(TAG, msg, e);
                    Toast.makeText(OfferDetail.this, msg, Toast.LENGTH_SHORT).show();
                }
            }
        });
    }

    private void initUI() {
        btn_request = findViewById(R.id.btn_request);
        txt_text = findViewById(R.id.txt_text);
        txt_title = findViewById(R.id.txt_title);
        final View includeView = findViewById(R.id.include);
        txt_offer_name = includeView.findViewById(R.id.txt_offer_name);
        txt_percent = includeView.findViewById(R.id.txt_percent);
        txt_money = includeView.findViewById(R.id.txt_money);
        txt_time = includeView.findViewById(R.id.txt_time);
        img_logo = includeView.findViewById(R.id.img_offer_logo);

        //Скрываем кнопку с тремя точками
        includeView.findViewById(R.id.img_more).setVisibility(View.GONE);

        //Получение переданного OfferID
        if (getIntent() != null) {
            offerId = getIntent().getStringExtra("offerId");
        }
    }
}
