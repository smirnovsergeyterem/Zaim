package ru.zaimme.zaim;


import android.content.Intent;
import android.graphics.Color;
import android.net.Uri;
import android.os.Bundle;
import android.support.annotation.NonNull;
import android.support.v7.app.AppCompatActivity;
import android.support.v7.widget.Toolbar;
import android.util.Log;
import android.view.MenuItem;
import android.widget.Button;
import android.widget.ImageView;
import android.widget.TextView;
import android.widget.Toast;
import com.bumptech.glide.Glide;
import com.bumptech.glide.request.RequestOptions;
import com.google.firebase.database.DataSnapshot;
import com.google.firebase.database.DatabaseError;
import com.google.firebase.database.DatabaseReference;
import com.google.firebase.database.FirebaseDatabase;
import com.google.firebase.database.ValueEventListener;
import com.mikepenz.fontawesome_typeface_library.FontAwesome.Icon;
import com.mikepenz.iconics.IconicsDrawable;
import java.util.Objects;
import ru.zaimme.zaim.Model.Cards.CardDetailItem;

public class CardCreditDetail extends AppCompatActivity {

    private static final String TAG = "CardCreditDetail";

    private Button btn_request;

    private String cardCreditId;

    private ImageView image;

    private DatabaseReference mCardCreditDetail;

    private TextView title, subtitle, bonus, sum, decision_time, percent, additionalInfo;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_card_credit_detail);

        // Handle Toolbar
        Toolbar toolbar = findViewById(R.id.toolbar);
        setSupportActionBar(toolbar);
        //set the back arrow in the toolbar
        Objects.requireNonNull(getSupportActionBar()).setDisplayHomeAsUpEnabled(true);
        getSupportActionBar().setTitle("");
        getSupportActionBar().setHomeAsUpIndicator(
                new IconicsDrawable(this, Icon.faw_arrow_left)
                        .color(Color.WHITE)
                        .sizeDp(IconicsDrawable.TOOLBAR_ICON_SIZE)
                        .paddingDp(IconicsDrawable.TOOLBAR_ICON_PADDING)
        );

        initFirebase();
        initUI();
        initListeners();
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        switch (item.getItemId()) {
            case android.R.id.home:
                onBackPressed();
                return true;

            default:
                return super.onOptionsItemSelected(item);
        }
    }

    public void openWebPage(String url) {
        Uri webpage = Uri.parse(url);
        Intent intent = new Intent(Intent.ACTION_VIEW, webpage);
        if (intent.resolveActivity(getPackageManager()) != null) {
            startActivity(intent);
        }
    }

    private void initFirebase() {
        //Firebase Init
        final FirebaseDatabase database = FirebaseDatabase.getInstance();
        mCardCreditDetail = database.getReference("CardDetail");
    }

    private void initListeners() {
        //Firebase Listener
        mCardCreditDetail.orderByKey().equalTo(cardCreditId).addListenerForSingleValueEvent(new ValueEventListener() {
            @Override
            public void onCancelled(@NonNull final DatabaseError databaseError) {
                Toast.makeText(CardCreditDetail.this, databaseError.getMessage(), Toast.LENGTH_SHORT).show();
            }

            @Override
            public void onDataChange(final DataSnapshot dataSnapshot) {
                try {
                    CardDetailItem cardDetailItem = new CardDetailItem();
                    if (dataSnapshot != null) {
                        for (DataSnapshot item : dataSnapshot.getChildren()) {
                            cardDetailItem = item.getValue(CardDetailItem.class);
                            //Заголовок
                            getSupportActionBar().setTitle(cardDetailItem.getTitle());
                            title.setText(cardDetailItem.getTitle());
                            subtitle.setText(cardDetailItem.getSubTitle());
                            bonus.setText(cardDetailItem.getBonus());
                            sum.setText(cardDetailItem.getSum());
                            decision_time.setText(cardDetailItem.getDecisionTime());
                            percent.setText(cardDetailItem.getPercent());
                            additionalInfo.setText(cardDetailItem.getAdditionalInfo());

                            final CardDetailItem finalCardCreditItem = cardDetailItem;
                            btn_request.setOnClickListener(v -> openWebPage(finalCardCreditItem.getUrl()));

                            Glide.with(getBaseContext())
                                    .load(cardDetailItem.getImage())
                                    .apply(new RequestOptions().fitCenter())
                                    .into(image);

                        }
                    }
                } catch (Exception e) {
                    String msg = "Ошибка при загрузке кредитной карты с ID=" + cardCreditId;
                    Log.e(TAG, msg, e);
                    Toast.makeText(CardCreditDetail.this, msg, Toast.LENGTH_SHORT).show();
                }
            }
        });
    }

    private void initUI() {
        title = findViewById(R.id.title);
        subtitle = findViewById(R.id.subtitle);
        bonus = findViewById(R.id.bonus);
        sum = findViewById(R.id.sum);
        decision_time = findViewById(R.id.decision_time);
        percent = findViewById(R.id.percent);
        additionalInfo = findViewById(R.id.additionalInfo);
        btn_request = findViewById(R.id.btn_request);
        image = findViewById(R.id.image);

        //Получение переданного cardCreditId
        if (getIntent() != null) {
            cardCreditId = getIntent().getStringExtra("cardCreditId");
        }
    }
}
