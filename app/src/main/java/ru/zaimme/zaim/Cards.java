package ru.zaimme.zaim;


import android.content.Intent;
import android.graphics.Color;
import android.os.Bundle;
import android.support.annotation.NonNull;
import android.support.design.widget.TabLayout;
import android.support.design.widget.TabLayout.OnTabSelectedListener;
import android.support.design.widget.TabLayout.Tab;
import android.support.v7.app.AppCompatActivity;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.support.v7.widget.Toolbar;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.MenuItem;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Toast;
import com.bumptech.glide.Glide;
import com.bumptech.glide.request.RequestOptions;
import com.firebase.ui.database.FirebaseRecyclerAdapter;
import com.firebase.ui.database.FirebaseRecyclerOptions;
import com.google.firebase.database.DataSnapshot;
import com.google.firebase.database.DatabaseError;
import com.google.firebase.database.DatabaseReference;
import com.google.firebase.database.FirebaseDatabase;
import com.google.firebase.database.Query;
import com.google.firebase.database.ValueEventListener;
import com.mikepenz.fontawesome_typeface_library.FontAwesome.Icon;
import com.mikepenz.iconics.IconicsDrawable;
import com.synnapps.carouselview.CarouselView;
import com.synnapps.carouselview.ImageListener;
import java.util.ArrayList;
import java.util.Objects;
import ru.zaimme.zaim.Model.Cards.CardCategory;
import ru.zaimme.zaim.Model.Cards.CardCategoryItem;
import ru.zaimme.zaim.Model.Cards.CardDetailItem;
import ru.zaimme.zaim.Model.Debt.DebtCardCategory;
import ru.zaimme.zaim.Model.Debt.DebtCardCategoryItem;
import ru.zaimme.zaim.Model.Debt.DebtCardDetailItem;
import ru.zaimme.zaim.Model.Installment.InstallmentCardCategory;
import ru.zaimme.zaim.Model.Installment.InstallmentCardCategoryItem;
import ru.zaimme.zaim.Model.Installment.InstallmentCardDetailItem;
import ru.zaimme.zaim.ViewHolder.CardDebtViewHolder;
import ru.zaimme.zaim.ViewHolder.CardInstallmentViewHolder;
import ru.zaimme.zaim.ViewHolder.CardViewHolder;

public class Cards extends AppCompatActivity {

    private static final String TAG = "CardsTAG";

    private CarouselView carouselView;

    private final ArrayList<String> mAdsImagesList = new ArrayList<>();

    private DatabaseReference mAdsRef;

    private DatabaseReference mCardCategory;

    private DatabaseReference mCardDetail;

    private FirebaseRecyclerAdapter<CardCategoryItem, CardViewHolder> mCardsAdapter;

    private FirebaseRecyclerAdapter<DebtCardCategoryItem, CardDebtViewHolder> mDebtCardsAdapter;

    private ImageListener mImageListener;

    private FirebaseRecyclerAdapter<InstallmentCardCategoryItem, CardInstallmentViewHolder> mInstallmentCardsAdapter;

    private RecyclerView mRecyclerView;

    private TabLayout mTabLayout;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_cards);

        // Handle Toolbar
        Toolbar toolbar = findViewById(R.id.toolbar);
        setSupportActionBar(toolbar);
        //set the back arrow in the toolbar
        Objects.requireNonNull(getSupportActionBar()).setDisplayHomeAsUpEnabled(true);
        getSupportActionBar().setTitle("Карты");
        getSupportActionBar().setHomeAsUpIndicator(
                new IconicsDrawable(this, Icon.faw_arrow_left)
                        .color(Color.WHITE)
                        .sizeDp(IconicsDrawable.TOOLBAR_ICON_SIZE)
                        .paddingDp(IconicsDrawable.TOOLBAR_ICON_PADDING)
        );

        initFirebase();
        initCarousel();
        initRecyclerView();
        initTabs();
        //Первичное заполнение офферов и категорий в FireBase
//        initValues();

        //По умолчанию первичная загрузка по названию первого таба
        getCardsByCategoryName(Objects.requireNonNull(
                Objects.requireNonNull(mTabLayout.getTabAt(mTabLayout.getSelectedTabPosition())).getText())
                .toString());
    }

    @Override
    protected void onResume() {
        super.onResume();
        if (mCardsAdapter != null) {
            mCardsAdapter.startListening();
        }
        if (mDebtCardsAdapter != null) {
            mDebtCardsAdapter.startListening();
        }
        if (mInstallmentCardsAdapter != null) {
            mInstallmentCardsAdapter.startListening();
        }
    }

    @Override
    protected void onPostResume() {
        super.onPostResume();
        carouselView.playCarousel();
    }

    @Override
    protected void onPause() {
        carouselView.pauseCarousel();
        super.onPause();
    }

    @Override
    protected void onStop() {
        super.onStop();
        if (mCardsAdapter != null) {
            mCardsAdapter.stopListening();
        }
        if (mDebtCardsAdapter != null) {
            mDebtCardsAdapter.stopListening();
        }
        if (mInstallmentCardsAdapter != null) {
            mInstallmentCardsAdapter.stopListening();
        }
    }

    @Override
    protected void onDestroy() {
        super.onDestroy();
        Glide.get(this).clearMemory();
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        switch (item.getItemId()) {
            case android.R.id.home:
                onBackPressed();
                return true;

            default:
                return super.onOptionsItemSelected(item);
        }
    }

    private void addCreditCards() {
        String key;
        ArrayList<CardDetailItem> listCardDetail = new ArrayList<>();
        ArrayList<CardCategoryItem> listCardCategory = new ArrayList<>();
        listCardDetail.add(new CardDetailItem("«Альфа-Банк»", "Кредитная карта «100 дней без %» Visa Classic",
                "Беспроцентный период: до 100 дней", "до 300 000", "от 23.99%", "от 2 минут",
                "https://zaimme.ru/images/cr-cards/visa_classic-a-b.png",
                "https://zaimme.ru/banks/credit-cards/alfabank_100_dney_bez_procentov_visa_clasic.php",
                "Требования и документы:									Возраст - от 18 лет									Паспорт + 2-й документ									Регистрация; Гражданство - РФ									Наличие постоянной регистрации или работы в регионе оформления карты.									Минимальный стаж на последнем месте работы: 6 месяцев.									Постоянный доход от 9 000 руб. после вычета налогов для Москвы и 5 000 руб. для регионов России.									Наличие контактного телефона (мобильного или домашнего по месту фактического проживания).									Прочая информация:									Кредитный лимит до 300 000 руб.									Снятие до 50 000 рублей в месяц без комиссии									Процентная ставка от 23,99%									Одобрение за 2 минуты онлайн.									Выпуск карты бесплатно.									Комиссия за выдачу наличных в банкоматах банка - 6,9%.									100 дней без процентов по кредиту на покупки и снятие наличных.									Беспроцентный период начинается при первой покупке, снятии наличных или иной операции по карте."));
        listCardDetail
                .add(new CardDetailItem("«Альфа-Банк»", "Карта рассрочки «Вместо денег»", "Рассрочка: до 24 мес.",
                        "до 100 000", "0%", "5 минут",
                        "https://zaimme.ru/images/cr-cards/alfa-bank-karta-rassrochki-vmesto-deneg.png",
                        "https://zaimme.ru/banks/karty-rassrochki/alfa_bank_karta_rassrochki_vmesto_deneg_online_zayavka.php",
                        "Требования и документы:									Возраст: от 18 до 85 лет.									Гражданство: РФ.									Документы: Паспорт.									+ документ, подтверждающий непрерывный стаж на последнем месте работы не меньше 3х мес.									Подтверждение дохода не требуется.									Прочая информация по карте #вместоденег:									Кредитный лимит до 100 000 руб.									Процентная ставка 0% годовых.									Рассрочка без процентов до 24 месяцев.									Карту принимают везде, по всему миру.									Рассрочка от партнеров банка - до 24 мес.									Рассрочка в остальных магазинах - до 4 мес.									Бесплатное оформление и обслуживание, без комиссий.									Срок действия карточки - 5 лет.									Процентная ставка вне периода рассрочки - 10% годовых.									Штраф за просрочку ежемесячного платежа - 500 руб.									Комиссии за досрочное погашение нет.									Снятие наличных и переводы на другие карты не предусмотрены.									Бесплатное пополнение на сайте банка, в мобильном приложении, в банкоматах и у банков-партнеров."));
        listCardDetail.add(new CardDetailItem("ПАО «Совкомбанк»", "Карта рассрочки «Халва»", "Рассрочка: до 12 мес.",
                "до 350 000", "0%", "от 10 минут",
                "https://zaimme.ru/images/cr-cards/kreditnaya-karta-sovkombank-halva.png",
                "https://zaimme.ru/banks/karty-rassrochki/sovkombank_karta_rassrochki_halva.php",
                "Требования и документы:									Возраст: от 20 до 80 лет									Гражданство: РФ									Документы: Паспорт									Прочая информация:									Кредитный лимит до 350 000 руб.									Процентная ставка 0% годовых									Выгодная беспроцентная рассрочка до 12 месяцев									Цена товара = сумме рассрочки									Возобновляемый лимит кредитования до 350 тыс. рублей									Выдача карты - бесплатно: без комиссии за годовое ведение счета									Бесконтактные платежи (технология PayPass)									Первоначальный взнос - 0%									Бесплатное пополнение онлайн									0% за пользование рассрочкой									Бесплатное оформление и обслуживание."));
        listCardDetail
                .add(new CardDetailItem("«КИВИ Банк» (АО)", "Карта рассрочки «Совесть»", "Рассрочка: до 12 мес.",
                        "до 300 000", "0%", "от 10 минут",
                        "https://zaimme.ru/images/cr-cards/qiwi-sovest-debet-card.png",
                        "https://zaimme.ru/banks/karty-rassrochki/qiwi_bank_karta_rassrochki_sovest.php",
                        "Требования и документы:									Возраст: от 18 до 65 лет									Гражданство: РФ									Документы: Паспорт									Прочая информация:									Мгновенные покупки в рассрочку.									Беспроцентная рассрочка от 1 до 12 месяцев									Без первоначального взноса и переплат. Цена товара = сумме рассрочки!									Возобновляемый лимит кредитования до 300 тыс. рублей.									Широкая партнерская сеть - в более 40 000 магазинов, ресторанов и сайтов по всей стране.									Выдача карты - бесплатно: без комиссии за годовое ведение счета!									Годовое облуживание бесплатно.									Комиссия за внесение платежа - 0 руб.									SMS-Информирование - бесплатно.									Ставка за использование рассрочки - 0% годовых. 									Комиссия за досрочное погашение - 0 руб.									Дата внесения ежемесячного платежа - До конца месяца, следующего за расчётным"));
        listCardDetail.add(new CardDetailItem("«Хоум Кредит Финанс Банк»", "Карта рассрочки «На все покупки»",
                "Рассрочка: до 12 мес.", "до 300 000", "0%", "от 15 минут",
                "https://zaimme.ru/images/cr-cards/karta-rassrochki-home-credit-bank.png",
                "https://zaimme.ru/banks/karty-rassrochki/home_credit_bank_karta_rassrochki.php",
                "Требования и документы:									Возраст: от 18 до 64 лет									Гражданство: РФ									Документы: Паспорт									Прочая информация:									0% - рассрочка на любые покупки.									Карту принимают везде.									Время на оплату рассрочки - 3 месяца на все покупки.									До 12 месяцев – на покупки у партнеров банка.									Сумма покупки выплачивается равными частями в течение всей рассрочки.									БЕСПЛАТНО - бслуживание карты и смс-пакет.									Процентная ставка по карте: 0% годовых – оплата товаров и услуг на задолженность, по которой действует Рассрочка.									Ставка 29,8% годовых – оплата товаров и услуг на задолженность, по которой не действует Рассрочка.									Снятие наличных не допускается.									Лимит кредитования устанавливается Банком индивидуально от 10 000 до 300 000 руб."));
        listCardDetail
                .add(new CardDetailItem("Touch Bank", "Кредитная карта «Touch Bank»", "Беспроцентный период: 61 день",
                        "до 1 000 000", "от 12%", "от 2 минут",
                        "https://zaimme.ru/images/cr-cards/touch-bank-credit-card.png",
                        "https://zaimme.ru/banks/credit-cards/touchbank_credit_card.php",
                        "Требования и документы:									Возраст от 23 до 65 лет									Документы: Паспорт. 									Стаж работы на последнем месте не менее 3 месяцев. 									Регистрация: РФ; Гражданство: РФ. 									Отсутствие  текущей просроченной задолженности по кредитам. 									Отсутствие просроченной задолженности по кредитам более 30 дней.									Прочая информация:									Выпуск кредитной карты - бесплатно.									Бесплатное пополнение.									Комиссия за снятие наличных - 0% по всему миру.									Бесплатная доставка карты.									Возврат до 3% от суммы покупок."));
        listCardDetail.add(new CardDetailItem("«Тинькофф Банк»", "Кредитная карта «Platinum»",
                "Беспроцентный период: до 55 дней", "до 300 000", "от 12,9%", "от 5 минут",
                "https://zaimme.ru/images/cr-cards/tinkoff-platinum.png",
                "https://zaimme.ru/banks/credit-cards/tinkoff_bank_platinum_kreditnaya_karta.php",
                "Требования и документы:									Возраст - от 18 лет									Паспорт									Стаж работы на последнем месте - 3 месяца									Регистрация и гражданство -  Российская Федерация									Прочая информация:									Комиссия за выдачу карты - Отсутствует									Годовое обслуживание - 590 руб.									Как получить карту на 55 дней без процентов?									Заполните простую анкету заявки и направьте ее в банк, дождитесь решения банка и получите карту с доставкой на дом от 1 дня."));
        listCardDetail.add(new CardDetailItem("«Тинькофф Банк»", "Кредитная карта «ALL GAMES»",
                "Беспроцентный период: до 55 дней", "до 700 000", "23.9%", "от 5 минут",
                "https://zaimme.ru/images/cr-cards/tink-a-games.png",
                "https://zaimme.ru/banks/credit-cards/tinkoff_bank_kreditnaya_karta_all_games.php",
                "Требования и документы:									Возраст - от 18 лет									Паспорт									Регистрация - РФ									Гражданство - РФ									Прочая информация:									Пополнение карты - без комиссии. 									Годовое обслуживание - 990 руб.									Условия начисления бонусных баллов:																			до 30% за покупки по спецпредложениям										5% с покупок в Steam, Origin, Xbox Games и PS Store										1,5% - со всех остальных покупок																		Заработанные баллы можно тратить (1 балл = 1 рубль):																			На игры и контент в магазинах Steam, Origin, Xbox Games и PlayStation Store, а также на сайте ru.4game.com										На покупки в играх										На любые покупки в магазинах игр и электроники"));
        listCardDetail.add(new CardDetailItem("«Тинькофф Банк»", "Кредитная карта «Lamoda»",
                "Беспроцентный период: до 55 дней", "до 700 000", "23.9%", "от 5 минут",
                "https://zaimme.ru/images/cr-cards/tink-lamo-card.png",
                "https://zaimme.ru/banks/credit-cards/tinkoff_bank_kreditnaya_karta_lamoda.php",
                "Требования и документы:									Возраст - от 18 лет									Паспорт									Регистрация - РФ									Гражданство - РФ									Прочая информация:									Кредитный лимит до 700 000 руб.									Процентная ставка от 23,9%									Пополнение карты - без комиссии.									Годовое обслуживание - 990 руб.									Условия начисления бонусных баллов:																			5% за покупку товаров на сайте Lamoda.ru										1% за любые другие покупки по карте."));
        listCardDetail.add(new CardDetailItem("«Тинькофф Банк»", "Кредитная карта «AliExpress»",
                "Беспроцентный период: до 55 дней", "до 700 000", "23.9%", "от 5 минут",
                "https://zaimme.ru/images/cr-cards/ae-exp-tink-card.png",
                "https://zaimme.ru/banks/credit-cards/tinkoff_bank_kreditnaya_karta_aliexpress.php",
                "Требования и документы:									Возраст - от 18 лет									Паспорт									Регистрация - РФ									Гражданство - РФ									Прочая информация:									Кредитный лимит до 700 000 руб.									Процентная ставка от 23,9%									Пополнение карты - без комиссии.									Годовое обслуживание - 990 руб.									Условия начисления бонусных баллов:																			5% за покупку товаров на сайте AliExpress										1% за любые другие покупки по карте.																		Заработанные баллы можно тратить (1 балл = 1 рубль):																			На любые покупки на сайте AliExpress стоимостью от 500 рублей"));
        listCardDetail.add(new CardDetailItem("«Тинькофф Банк»", "Кредитная карта «eBay»",
                "Беспроцентный период: до 55 дней", "до 700 000", "23.9%", "от 5 минут",
                "https://zaimme.ru/images/cr-cards/e-bay-tink-card.png",
                "https://zaimme.ru/banks/credit-cards/tinkoff_bank_kreditnaya_karta_ebay.php",
                "Требования и документы:									Возраст - от 18 лет									Паспорт									Регистрация - РФ									Гражданство - РФ									Прочая информация:									Кредитный лимит до 700 000 руб.									Процентная ставка от 23,9%									Пополнение карты - без комиссии.									Годовое обслуживание - 990 руб.									Условия начисления бонусных баллов:																			3% за покупку товаров на сайте ebay.com										3% за покупку любых товаров в интернете										1% за любые другие покупки по карте"));
        listCardDetail.add(new CardDetailItem("ПАО «Почта Банк»", "Кредитная карта «Элемент 120»",
                "Беспроцентный период: до 120 дней", "до 500 000", "27.9%", "от 1 минуты",
                "https://zaimme.ru/images/cr-cards/kreditnaya-karta-pochtabank-element-120.png",
                "https://zaimme.ru/banks/credit-cards/pochta_bank_kreditnaya_karta_element-120.php",
                "Требования и документы:									Возраст - от 22 до 60 лет									Паспорт									Регистрация - РФ									Гражданство - РФ									Прочая информация:									Срок действия карты - 60 месяцев.									Кредитный лимит до 500 000 рублей.									Привилегии владельца «Пакета онлайн-покупателя»: 									1. Товары по самой выгодной цене: Если товар не подошел или отличается от заказанного, вам будет возмещена стоимость обратной отправки его продавцу. 									2. Бесплатный ремонт техники: Если техника оказалась сломанной, ее бесплатно отремонтируют, а если поченить ее уже нельзя, то вам будет компенсирована полная ее стоимость. 									3. Не подошел товар - верни бесплатно! - Если товар не подошел или отличается от заказанного, вам будет возмещена стоимость обратной отправки его продавцу. 																		Комиссия за оформление карты 900 рублей.									Ежегодная к омиссия за обслуживание карты за первый 1 год - не взымается, за каждый следующий год - 900 рублей.									Комиссия за перевыпуск карты по окончанию срока действия карты - не взымается, в остальных случаях - 600 рублей.									Комиссия за оплату товаров/услуг - не взымается									Процентная ставка по Кредиту на операции оплаты товаров/услуг: при выполнении условий Беспроцентного период - 0% годовых, в остальных случаях - 27,9% годовых.									Длительность Беспроцентного периода - до 4-х месяцев.									Комиссия за выдачу наличными собственных денежных средств по Карте / без использования карты в банкоматах и пунктах выдачи наличных ПАО «Почта Банк» и банков Группы ВТБ: ВТБ (ПАО), ВТБ24 (ПАО), АО «ВТБ (Грузия)», ПАО «ВТБ» (Украина), ЗАО «ВТБ (Армения)», ЗАО ВТБ (Беларусь), ОАО ВТБ (Азербайджан), ДО АО ВТБ (Казахстан) - не взимается; в остальных случаях - 5,9% (минимум 300 рублей).									Процентная ставка по Кредиту, предоставленному на совершение операций получения наличных денежных средств: 27,9% годовых.									Лимит на операции получения наличных денежных средств: 100 000 рублей в день / 300 000 рублей в месяц."));
        listCardDetail.add(new CardDetailItem("«Альфа-Банк»", "Кредитная карта VISA Platinum «100 дней без %»",
                "Беспроцентный период: до 100 дней", "до 1 000 000", "от 23.99%", "от 2 минут",
                "https://zaimme.ru/images/cr-cards/kreditnaya-karta-alfa-banka-100_dney_visa_platinum.png",
                "https://zaimme.ru/banks/credit-cards/alfabank_100_dney_bez_procentov_visa_platinum.php",
                "Требования и документы:									Возраст - от 18 лет									Паспорт + 2-й документ									Регистрация; Гражданство - РФ									Наличие постоянной регистрации или работы в регионе оформления карты.									Минимальный стаж на последнем месте работы: 6 месяцев.									Постоянный доход от 9 000 руб. после вычета налогов для Москвы и 5 000 руб. для регионов России.									Наличие контактного телефона (мобильного или домашнего по месту фактического проживания).									Прочая информация:									Пользуйтесь 100 дней без %, по кредиту на любые покупки и снятие наличных; 									100 дней без % всегда возобновляются на следующий день после полного погашения задолженности; 									Кредитный лимит - до 1 000 000 руб.; 									Стоимость обслуживания - от 5 490 руб. в год; 									Процентная ставка — от 23,99% годовых, определяется индивидуально; 									Бесплатное пополнение с карт любых банков; 									Можно подключить к Apple Pay или Samsung Pay; 									Бесплатное мобильное приложение «Альфа-Мобайл»;"));
        listCardDetail.add(new CardDetailItem("Банк «Ренессанс Кредит»", "Кредитная карта MasterCard «Классическая»",
                "Беспроцентный период: до 55 дней", "до 200 000", "24,9% - 29,9%", "от 30 минут",
                "https://zaimme.ru/images/cr-cards/ren-credit-card.png",
                "https://zaimme.ru/banks/credit-cards/bank_renessance_credit_credit_card.php",
                "Требования и документы:									от 21 до 65 лет									Паспорт + 2-й документ									Регистрация; Гражданство - РФ									Наличие постоянной регистрации или работы в регионе оформления карты.									Минимальный стаж на последнем месте работы: 3 месяца.									Ежемесячный доход (учитывается совокупный доход клиента после налогооблажения): 12 000 руб. для жителей Москвы, 8 000 руб. для жителей других регионов.									Обязательно наличие личного мобильного телефона.									Получить карту можно только в отделении банка.									Дополнительный документ для оформления (на выбор): загран.паспорт, водительское удостоверение, именная банковская карта, диплом о среднем специальном или высшем образовании.									Прочая информация:									Кредитный лимит - до 200 000 руб.; 									Годовая ставка от 24,9% - 37%.									Выпуск и обслуживание карты – бесплатно.									Льготный период кредитования до 55 дней.									Получение карты в день обращения.									Программа лояльности «Простые радости» - базовое начисление 1% от суммы всех покупок по оплате товаров и услуг, акционное начисление до 20% в специальных категориях месяца."));
        listCardDetail.add(new CardDetailItem("«Альфа-Банк»", "Кредитная карта MasterCard «Перекресток»",
                "Беспроцентный период: до 60 дней", "до 300 000", "от 23,99%", "2 минуты",
                "https://zaimme.ru/images/cr-cards/perecrestok-a-b.png",
                "https://zaimme.ru/banks/credit-cards/alfabank_kreditnaya_karta_perekrestok.php",
                "Требования и документы:									от 18 лет									Паспорт									Регистрация; Гражданство - РФ									Наличие постоянной регистрации или работы в регионе оформления карты.									Минимальный стаж на последнем месте работы: 6 месяцев.									Постоянный доход от 9 000 руб. после вычета налогов для Москвы и 5 000 руб. для регионов России.									Наличие контактного телефона (мобильного или домашнего по месту фактического проживания).									Прочая информация:									Кредитный лимит - до 300 000 руб.; 									Баллы «Перекресток» за любые покупки.									2000 баллов «Перекресток» в подарок.									3 балла за каждые 10 руб. за покупки в Перекрестке.									1 балл за каждые 10 руб. других покупок.									10 бесплатных снятий наличных в месяц в любых банкоматах в России.									Бесплатное пополнение с карты другого банка.									Годовое обслуживание 490 руб."));
        listCardDetail.add(new CardDetailItem("«Альфа-Банк»", "Кредитная карта «РЖД Бонус»",
                "Беспроцентный период: до 60 дней", "до 150 000", "от 23,99%", "2 минуты",
                "https://zaimme.ru/images/cr-cards/kreditnaya-karta-alfa-banka-rgd-bonus.png",
                "https://zaimme.ru/banks/credit-cards/alfabank_kreditnaya_karta_rjd_bonus.php",
                "Требования и документы:									от 18 лет									Паспорт									Регистрация; Гражданство - РФ									Наличие постоянной регистрации или работы в регионе оформления карты.									Минимальный стаж на последнем месте работы: 6 месяцев.									Постоянный доход от 9 000 руб. после вычета налогов для Москвы и 5 000 руб. для регионов России.									Наличие контактного телефона (мобильного или домашнего по месту фактического проживания).									Прочая информация:									Кредитный лимит - до 150 000 руб.;									1,25 балла за каждые 30 руб., потраченные по карте;									500 баллов в подарок;									Пользуйтесь 60 дней без %, по кредиту на покупки и снятие наличных;									Кредитный лимит - до 150 000 руб.;									Стоимость обслуживания - от 790 руб. в год;									Можно подключить к Apple Pay или Samsung Pay;"));
        listCardDetail.add(new CardDetailItem("«Альфа-Банк»", "Кредитная карта «FIFA 2018»",
                "Беспроцентный период: до 60 дней", "до 500 000", "от 23,99%", "2 минуты",
                "https://zaimme.ru/images/cr-cards/kreditnaya-karta-alfa-banka-fifa-2018.png",
                "https://zaimme.ru/banks/credit-cards/alfabank_kreditnaya_karta_fifa_2018.php",
                "Требования и документы:									от 18 лет.									Паспорт + 2-й документ.									Регистрация; Гражданство - РФ.									Наличие постоянной регистрации или работы в регионе оформления карты.									Минимальный стаж на последнем месте работы: 6 месяцев.									Постоянный доход от 9 000 руб. после вычета налогов для Москвы и 5 000 руб. для регионов России.									Наличие контактного телефона (мобильного или домашнего по месту фактического проживания).									Прочая информация:									Карта VISA Rewards – 2018 FIFA World Cup – PayWave									Уникальная карта Чемпионата мира по футболу FIFA 2018™;									За баллы -  билеты на Чемпионат мира по футболу FIFA 2018;									Кредитный лимит - до 500 000 руб;									Выпуск кредитной карты - бесплатно;									Стоимость обслуживания - 1290 руб. в год;									Бонусные баллы за каждую покупку;									За каждые 50 руб. - 1 балл;									200 приветственных баллов – в подарок;									За каждые 300 руб. остатка на счете - дополнительные баллы;									Билеты на ЧМ, спортивная атрибутика ЧМ, технологичная сувенирная продукция;									Уникальные впечатления, которые невозможно купить за деньги;"));
        listCardDetail.add(new CardDetailItem("«Альфа-Банк»", "Кредитная карта «Cash Back»",
                "Беспроцентный период: до 60 дней", "до 500 000", "от 25,99%", "1-2 дня",
                "https://zaimme.ru/images/cr-cards/cashback-a-b.png",
                "https://zaimme.ru/banks/credit-cards/alfabank_kreditnaya_karta_cashback.php",
                "Требования и документы:									от 18 лет									Паспорт + 2-й документ									Регистрация; Гражданство - РФ									Наличие постоянной регистрации или работы в регионе оформления карты.									Минимальный стаж на последнем месте работы: 6 месяцев.									Постоянный доход от 9 000 руб. после вычета налогов для Москвы и 5 000 руб. для регионов России.									Наличие контактного телефона (мобильного или домашнего по месту фактического проживания).									Прочая информация:									Кредитный лимит - до 500 000 руб.;									Cash Back:																			10% от покупок на АЗС										5% от всех счетов в кафе и ресторанах										1% от остальных покупок										3% за покупку любых товаров в интернете																		Технология PayPass: загрузите карту и платите смартфоном."));
        listCardDetail.add(new CardDetailItem("«Райффайзен Банк»", "Кредитная карта «110 дней»",
                "Беспроцентный период: до 110 дней", "до 600 000", "29%", "2 дня",
                "https://zaimme.ru/images/cr-cards/rfb-credit-card.png",
                "https://zaimme.ru/banks/credit-cards/raiffeisen_bank_kreditnaya_karta.php",
                "Требования и документы:									Возраст от 23 до 67 лет									Паспорт (копия с разворотом страницы с фотографией и текущей регистрацией) + 2-й документ (справка по форме банка о полученных доходах, заверенная работодателем).									Регистрация; Гражданство - РФ									Прочая информация:									Кредитный лимит - до 600 000 руб.									Беспроцентный льготный период до 110 дней.									До 30% скидки у 4000 партнеров банка.									Получение карты в день обращения.									0% за обслуживание при ежемесячных тратах по карте от 8 000 руб."));
        listCardDetail.add(new CardDetailItem("Банк «Первомайский»", "Кредитная карта VISA «Классическая»",
                "Беспроцентный период: до 55 дней", "до 500 000", "от 22,9%", "от 15 минут",
                "https://zaimme.ru/images/cr-cards/kreditnaya-karta-bank-pervomaiskii.png",
                "https://zaimme.ru/banks/credit-cards/bank_pervomayskiy_kreditnaya_karta_klassicheskaya.php",
                "Требования и документы:									от 21 до 65 лет									Паспорт									Регистрация; Гражданство - РФ									Регистрация и проживание в городах присутствия Банка.									Прочая информация:									Кредитный лимит до 500 000 рублей, возобновляемый.									Льготный период на снятие наличных до 55 дней.									Cashback до 20% от каждой покупки									Оформление карты без подтверждения дохода.									На 15% меньше платеж по кредиту.									Бессрочная кредитная карта.									Бесплатная доставка карты."));
        listCardDetail.add(new CardDetailItem("«Восточный Банк»", "Кредитная карта «Сезонная»",
                "Беспроцентный период: до 56 дней", "до 300 000", "от 22,9%", "от 5 минут",
                "https://zaimme.ru/images/cr-cards/kreditnaya-karta-vostochnii-bank-sezonnaya.png",
                "https://zaimme.ru/banks/credit-cards/vostochniy_bank_kreditnaya_karta_sezonnaya.php",
                "Требования и документы:									Возраст: от 21 до 76 лет									Паспорт									Регистрация; Гражданство - РФ									Постоянная регистрация на территории РФ.									Стабильный ежемесячный доход и стаж в течение 3 мес.									Прочая информация:									Кредитный лимит до 300 000 рублей.									Льготный период кредитования до 56 дней (не распространяется на выдачу наличных).									CashBack на покупки и оплату услуг до 5%									Оформление карты доступно исключительно в регионах присутствия банка.									Процентная ставка 12% годовых на снятие наличных на первые три месяца.									Выдача в день обращения, только по паспорту."));
        listCardDetail.add(new CardDetailItem("«Восточный Банк»", "Кредитная карта «Cashback»",
                "Беспроцентный период: до 56 дней", "до 300 000", "от 29,9%", "от 5 минут",
                "https://zaimme.ru/images/cr-cards/kreditnaya-karta-vostochnii-bank-sezonnaya.png",
                "https://zaimme.ru/banks/credit-cards/vostochniy_bank_kreditnaya_karta_cashback.php",
                "Требования и документы:									от 21 до 76 лет									Паспорт									Регистрация; Гражданство - РФ									Постоянная регистрация на территории РФ.									Стабильный ежемесячный доход и стаж в течение 3 мес.									Прочая информация:									Возобновляемый кредитный лимит от 55 000 до 300 000 рублей.									Льготный период кредитования до 56 дней (не распространяется на выдачу наличных).									CashBack на покупки и оплату услуг до 5%									Ставка при безналичной оплате товаров и услуг - 29,9%									Выдача карты только по паспорту за 5 минут.									Годовое обслуживание карты - бесплатно.									Всегда 1% бонус за любые покупки.									Повышенный кэшбек за покупки в категориях: Косметика и парфюмерия – 5%; Цветы, подарки и сувениры – 5%; Аптеки – 5%."));
        listCardDetail.add(new CardDetailItem("«Азиатско-Тихоокеанский Банк»", "Кредитная карта «90 Даром»",
                "Беспроцентный период: до 92 дней", "до 300 000", "от 33%", "от 1 часа",
                "https://zaimme.ru/images/cr-cards/kreditnaya-karta-aziatsko-tihookeanskii-bank-stavka-19.png",
                "https://zaimme.ru/banks/credit-cards/aziatsko_tihookeanskiy_bank_kreditnaya_karta_90_darom.php",
                "Требования и документы:									от 18 до 65 лет									Паспорт									Регистрация; Гражданство - РФ									Регистрация в регионе присутствия банка.									Прочая информация:									Кредитный лимит до 300 000 рублей.									Льготный период на наличные и безналичные операции.									Кэшбэк 1% - банк возвращает 1% кэшбэк от стоимости всех покупок по карте без ограничений.									Срок кредита задаете сами - Ежемесячно вносите сумму не менее минимального платежа, а период погашения определяйте сами."));
        listCardDetail.add(new CardDetailItem("«Азиатско-Тихоокеанский Банк»", "Кредитная карта «Ставка 19»",
                "Беспроцентный период: -", "до 300 000", "от 19%", "от 1 часа",
                "https://zaimme.ru/images/cr-cards/kreditnaya-karta-aziatsko-tihookeanskii-bank-stavka-19.png",
                "https://zaimme.ru/banks/credit-cards/aziatsko_tihookeanskiy_bank_kreditnaya_karta_stavka_19.php",
                "Требования и документы:									от 18 до 65 лет									Паспорт									Регистрация; Гражданство - РФ									Регистрация в регионе присутствия банка.									Прочая информация:									Кредитный лимит до 300 000 рублей.									Низкая процентная ставка по кредитному продукту.									Кэшбэк 1% - банк возвращает 1% кэшбэк от стоимости всех покупок по карте без ограничений.									Срок кредита задаете сами - Ежемесячно вносите сумму не менее минимального платежа, а период погашения определяйте сами. 									Пример: вы берете 30 000 рублей на 12 месяцев с процентной ставкой 19% годовых. Переплата за весь срок кредита составит 4 515,58 руб. Итого Вы возвращаете 34 515,58."));
        listCardDetail.add(new CardDetailItem("«Азиатско-Тихоокеанский Банк»", "Кредитная карта «Мои правила»",
                "Беспроцентный период: до 56 дней", "до 700 000", "от 28%", "от 1 часа",
                "https://zaimme.ru/images/cr-cards/kreditnaya-karta-aziatsko-tihookeanskii-bank-moi-pravila.png",
                "https://zaimme.ru/banks/credit-cards/aziatsko_tihookeanskiy_bank_kreditnaya_karta_moi_pravila.php",
                "Требования и документы:									от 18 до 65 лет									Паспорт									Регистрация; Гражданство - РФ									Регистрация в регионе присутствия банка.									Прочая информация:									Кредитный лимит до 700 000 рублей.									56 дней без процентов - льготный период на наличные и безналичные операции.									Банк возвращает до 7% кэшбэк - супервыгодный возврат от суммы, потраченной на товары и услуги."));

        for (CardDetailItem item : listCardDetail) {
            key = mCardDetail.push().getKey();
            Log.d(TAG, "В CardDetailItem будет добавлен узел с ключом " + key);
            listCardCategory.add
                    (new CardCategoryItem
                            (
                                    item.getBonus(),
                                    item.getDecisionTime(),
                                    key,
                                    item.getImage(),
                                    item.getUrl(),
                                    item.getPercent(),
                                    item.getSubTitle(),
                                    item.getSum(),
                                    item.getTitle())
                    );
            mCardDetail.child(key).setValue(item);
        }
        mCardCategory.push().setValue(new CardCategory("Кредитные", listCardCategory));
        Log.d(TAG, "addCreditCards: заполнения CardDetailItem");
    }

    private void addDebtCards() {
        String key;
        ArrayList<DebtCardDetailItem> listCardDetail = new ArrayList<>();
        ArrayList<DebtCardCategoryItem> listDebtCardCategory = new ArrayList<>();
        listCardDetail.add(
                new DebtCardDetailItem("«Хоум Кредит Финанс Банк»", "Дебетовая карта «КОСМОС»",
                        "Выгодная дебетовая карта с привлекательными условиями от крупнейшего розничного банка в России",
                        "https://zaimme.ru/images/cr-cards/debetovaya_karta_home-credit-bank-kosmos.png",
                        "Преимущества карты «КОСМОС»:\n" +
                                "Обслуживание - бесплатное;\n" +
                                "7,5 % годовых при остатке на счете от 10 000 рублей до 500 000 рублей;\n" +
                                "CashBack баллами 1% от суммы любой покупки;\n" +
                                "CashBack баллами до 3% от суммы в категориях: АЗС, путешествия, кафе и рестораны\n" +
                                "CashBack баллами до 10% за онлайн-шоппинг в магазинах-партнёрах 1 балл = 1 рубль",
                        "https://zaimme.ru/banks/debet-cards/home_credit_bank_debetovaya_karta_kosmos.php",
                        "316"));
        listCardDetail.add(
                new DebtCardDetailItem(
                        "Тинькофф Банк",
                        "Дебетовая карта «Тинькофф Black»",
                        "Карта для тех кто не любит ждать - быстрые переводы, Cashback до 30%",
                        "https://zaimme.ru/images/cr-cards/debetovaya_karta_tinkoff_platinum.png",
                        "До 7% на остаток по счету\n" +
                                "Бесплатное снятие наличных в любом банкомате мира\n" +
                                "Cashback до 30% за любые покупки\n" +
                                "Для граждан любых стран",
                        "https://zaimme.ru/banks/debet-cards/tinkoff_bank_debetovaya_karta_tinkoff_black.php",
                        "2673"));
        listCardDetail.add(
                new DebtCardDetailItem(
                        "Альфа-Банк и «Wargaming»",
                        "Дебетовая карта «World of Tanks»",
                        "Уникальная дебетовая карта для танкистов, которые умеют считать",
                        "https://zaimme.ru/images/cr-cards/debetovaya_karta_world_of_tanks_ot_alfa_banka_i_wargaming.png",
                        "Уникальный продукт, имеющий ряд преимуществ для любителей игры «World of Tanks»:\n" +
                                "Goldback за повседневные покупки;\n" +
                                "До 8,5 единиц золота за каждые 100 рублей;\n" +
                                "Месяц премиум-аккаунта в подарок;\n" +
                                "Бесплатное пополнение с карты другого банка;\n" +
                                "Оформление осуществляется онлайн по всей России;",
                        "https://zaimme.ru/banks/debet-cards/alfa_bank_debetovaya_karta_wot.php",
                        "1326"));
        listCardDetail.add(
                new DebtCardDetailItem(
                        "Альфа-Банк и «Wargaming»",
                        "Дебетовая карта «World of Warships»",
                        "Уникальная дебетовая карта для победителей морского боя",
                        "https://zaimme.ru/images/cr-cards/debetovaya_karta_world_of_warships_ot_alfa_banka_i_wargaming.png",
                        "Уникальный продукт, имеющий ряд преимуществ для любителей игры «World of Warships»:\n" +
                                "Дублоны за повседневные покупки;\n" +
                                "До 8,5 дублонов за каждые 100 рублей;\n" +
                                "Месяц премиум-аккаунта в подарок;\n" +
                                "Бесплатное пополнение с карты другого банка\n" +
                                "Оформление осуществляется онлайн по всей России",
                        "https://zaimme.ru/banks/debet-cards/alfa_bank_debetovaya_karta_wow.php",
                        "1326"));
        listCardDetail.add(
                new DebtCardDetailItem(
                        "Промсвязьбанк",
                        "Дебетовая карта «ShoppingCard»",
                        "Универсальная дебетовая карта для выгодных покупок по всему миру",
                        "https://zaimme.ru/images/cr-cards/psb-sh-card-debet-card.png",
                        "Бесплатная горячая линия в Росcии: \n" +
                                "8 800 333 03 03\n" +
                                "Бесплатная курьерская доставка\n" +
                                "Удобное пополнение\n" +
                                "Скидка на доставку покупок\n" +
                                "Бонусы от ShopoTam\n" +
                                "Бесплатное страхование от мошеннических операций",
                        "https://zaimme.ru/banks/debet-cards/promsvyazbank_debetovaya_karta_shopping_card.php",
                        "3251"));
        listCardDetail.add(
                new DebtCardDetailItem(
                        "Альфа-Банк",
                        "Дебетовая карта «FIFA 2018»",
                        "Копите баллы и оплачивайте ими покупку билетов на чемпионат мира по футболу!",
                        "https://zaimme.ru/images/cr-cards/kreditnaya-karta-alfa-banka-fifa-2018.png",
                        "За баллы - билеты на Чемпионат мира по футболу FIFA 2018\n" +
                                "За каждые 50 рублей - 1 балл\n" +
                                "200 приветственных баллов – в подарок\n" +
                                "Годовое обслуживание – от 1200 рублей",
                        "https://zaimme.ru/banks/debet-cards/alfa_bank_debetovaya_karta_fifa_2018.php",
                        "1326"));
        listCardDetail.add(
                new DebtCardDetailItem(
                        "Альфа-Банк",
                        "Дебетовая карта «Cash Back»",
                        "Оплачивайте любые покупки по всему миру и возвращайте до 15% от покупок!",
                        "https://zaimme.ru/images/cr-cards/cashback-a-b.png",
                        "Cash back (АЗС) - 10%\n" +
                                "Cash back (кафе и рестораны) - 5%\n" +
                                "Скидки у партнеров по всему миру - до 15%\n" +
                                "Накопительный счет - до 7%",
                        "https://zaimme.ru/banks/debet-cards/alfa_bank_debetovaya_karta_cashback.php",
                        "1326"));
        listCardDetail.add(
                new DebtCardDetailItem(
                        "Альфа-Банк",
                        "Дебетовая карта «Перекресток»",
                        "Копите баллы за покупки в сети гипермаркетов «Перекресток» и тратьте их",
                        "https://zaimme.ru/images/cr-cards/perecrestok-a-b.png",
                        "За каждые 10 руб. покупки в Перекрестке- 3 балла «Перекресток»\n" +
                                "За каждые 10 руб. других покупок - 1 балл «Перекресток»\n" +
                                "2000 баллов «Перекресток» - в подарок\n" +
                                "Годовое обслуживание - 490 руб.",
                        "https://zaimme.ru/banks/debet-cards/alfa_bank_debetovaya_karta_perekrestok.php",
                        "1326"));
        listCardDetail.add(
                new DebtCardDetailItem(
                        "Альфа-Банк",
                        "Дебетовая карта «РЖД Бонус»",
                        "Копите баллы и оплачивайте ими Ж/Д билеты для путешествий",
                        "https://zaimme.ru/images/cr-cards/kreditnaya-karta-alfa-banka-rgd-bonus.png",
                        "За баллы «РЖД Бонус» - Ж/Д билеты\n" +
                                "За каждые 30 руб./1 $/0,8 € – 1 балл «РЖД Бонус»\n" +
                                "500 приветственных баллов – в подарок\n" +
                                "Годовое обслуживание - 490 руб.",
                        "https://zaimme.ru/banks/debet-cards/alfa_bank_debetovaya_karta_rjd_bonus.php",
                        "1326"));
        listCardDetail.add(
                new DebtCardDetailItem(
                        "Альфа-Банк",
                        "Дебетовая карта «NEXT»",
                        "Лучшие предложения партнеров Альфа-Банка только для держателей карты",
                        "https://zaimme.ru/images/cr-cards/a-b-karta_next.png",
                        "CashBack во всех кафе и ресторанах – от 5% до 10%.\n" +
                                "CashBack в любых кинотеатрах – 5%.\n" +
                                "Скидки и бонусы от партнеров банка.\n" +
                                "Пополнение с карт других банков – без комиссии.\n" +
                                "Обслуживание мобильного банка – бесплатно.\n" +
                                "Годовое обслуживание – всего 10 рублей в месяц.\n" +
                                "Индивидуальный дизайн на свой вкус.",
                        "https://zaimme.ru/banks/debet-cards/alfa_bank_debetovaya_karta_next.php",
                        "1326"));

        for (DebtCardDetailItem item : listCardDetail) {
            key = mCardDetail.push().getKey();
            Log.d(TAG, "В DebtCardDetailItem будет добавлен узел с ключом " + key);
            listDebtCardCategory.add
                    (new DebtCardCategoryItem(
                                    item.getTitle(),
                                    item.getSubTitle(),
                                    item.getDescription(),
                                    item.getImage(),
                                    key
                            )
                    );
            mCardDetail.child(key).setValue(item);
        }
        mCardCategory.push().setValue(new DebtCardCategory("Дебетовые", listDebtCardCategory));
        Log.d(TAG, "addDebtCards: заполнение DebtCardDetailItem");
    }

    private void addRassCards() {
        String key;
        ArrayList<InstallmentCardDetailItem> listCardDetail = new ArrayList<>();
        ArrayList<InstallmentCardCategoryItem> listInstallmentCardCategory
                = new ArrayList<>();
        listCardDetail.add(
                new InstallmentCardDetailItem(
                        "«Альфа-Банк»",
                        "Карта рассрочки «Вместо денег»",
                        "Рассрочка: до 24 мес.",
                        "0%",
                        "до 100 000",
                        "Кредитный лимит до 100 000 руб.\n"
                                + "Процентная ставка 0% годовых.\n"
                                + "Рассрочка без процентов до 24 месяцев.\n"
                                + "Карту принимают везде, по всему миру.\n"
                                + "Рассрочка от партнеров банка - до 24 мес.\n"
                                + "Рассрочка в остальных магазинах - до 4 мес.\n"
                                + "Бесплатное оформление и обслуживание, без комиссий.\n"
                                + "Срок действия карточки - 5 лет.\n"
                                + "Процентная ставка вне периода рассрочки - 10% годовых.\n"
                                + "Штраф за просрочку ежемесячного платежа - 500 руб.\n"
                                + "Комиссии за досрочное погашение нет.\n"
                                + "Снятие наличных и переводы на другие карты не предусмотрены.\n"
                                + "Бесплатное пополнение на сайте банка, в мобильном приложении, в банкоматах и у банков-партнеров.",
                        "5 минут",
                        "Возраст: от 18 до 85 лет.\n"
                                + "Гражданство: РФ.\n"
                                + "Документы: Паспорт.\n"
                                + "+ документ, подтверждающий непрерывный стаж на последнем месте работы не меньше 3х мес.\n"
                                + "Подтверждение дохода не требуется.",
                        "https://zaimme.ru/images/cr-cards/alfa-bank-karta-rassrochki-vmesto-deneg.png",
                        "1326",
                        "https://zaimme.ru/banks/karty-rassrochki/alfa_bank_karta_rassrochki_vmesto_deneg_online_zayavka.php"

                )
        );
        listCardDetail.add(
                new InstallmentCardDetailItem(
                        "ПАО «Совкомбанк»",
                        "Карта рассрочки «Халва»",
                        "Рассрочка: до 12 мес.",
                        "0%",
                        "до 350 000",
                        "Кредитный лимит до 350 000 руб.\n"
                                + "Процентная ставка 0% годовых\n"
                                + "Выгодная беспроцентная рассрочка до 12 месяцев\n"
                                + "Цена товара = сумме рассрочки\n"
                                + "Возобновляемый лимит кредитования до 350 тыс. рублей\n"
                                + "Выдача карты - бесплатно: без комиссии за годовое ведение счета\n"
                                + "Бесконтактные платежи (технология PayPass)\n"
                                + "Первоначальный взнос - 0%\n"
                                + "Бесплатное пополнение онлайн\n"
                                + "0% за пользование рассрочкой\n"
                                + "Бесплатное оформление и обслуживание.",
                        "от 10 минут",
                        "Возраст: от 20 до 80 лет\n"
                                + "Гражданство: РФ\n"
                                + "Документы: Паспорт",
                        "https://zaimme.ru/images/cr-cards/kreditnaya-karta-sovkombank-halva.png",
                        "963",
                        "https://zaimme.ru/banks/karty-rassrochki/sovkombank_karta_rassrochki_halva.php"

                )
        );
        listCardDetail.add(
                new InstallmentCardDetailItem(
                        "«КИВИ Банк» (АО)",
                        "Карта рассрочки «Совесть»",
                        "Рассрочка: до 12 мес.",
                        "0%",
                        "до 300 000",
                        "Мгновенные покупки в рассрочку.\n"
                                + "Беспроцентная рассрочка от 1 до 12 месяцев\n"
                                + "Без первоначального взноса и переплат. Цена товара = сумме рассрочки!\n"
                                + "Возобновляемый лимит кредитования до 300 тыс. рублей.\n"
                                + "Широкая партнерская сеть - в более 40 000 магазинов, ресторанов и сайтов по всей стране.\n"
                                + "Выдача карты - бесплатно: без комиссии за годовое ведение счета!\n"
                                + "Годовое облуживание бесплатно.\n"
                                + "Комиссия за внесение платежа - 0 руб.\n"
                                + "SMS-Информирование - бесплатно.\n"
                                + "Ставка за использование рассрочки - 0% годовых.\n"
                                + "Комиссия за досрочное погашение - 0 руб.\n"
                                + "Дата внесения ежемесячного платежа - До конца месяца, следующего за расчётным",
                        "от 10 минут",
                        "Возраст: от 18 до 65 лет\n"
                                + "Гражданство: РФ\n"
                                + "Документы: Паспорт",
                        "https://zaimme.ru/images/cr-cards/qiwi-sovest-debet-card.png",
                        "241",
                        "https://zaimme.ru/banks/karty-rassrochki/qiwi_bank_karta_rassrochki_sovest.php"

                )
        );
        listCardDetail.add(
                new InstallmentCardDetailItem(
                        "«Хоум Кредит Финанс Банк»",
                        "Карта рассрочки «На все покупки»",
                        "Рассрочка: до 12 мес.",
                        "0%",
                        "до 300 000",
                        "0% - рассрочка на любые покупки.\n"
                                + "Карту принимают везде.\n"
                                + "Время на оплату рассрочки - 3 месяца на все покупки.\n"
                                + "До 12 месяцев – на покупки у партнеров банка.\n"
                                + "Сумма покупки выплачивается равными частями в течение всей рассрочки.\n"
                                + "БЕСПЛАТНО - бслуживание карты и смс-пакет.\n"
                                + "Процентная ставка по карте: 0% годовых – оплата товаров и услуг на задолженность, по которой действует Рассрочка.\n"
                                + "Ставка 29,8% годовых – оплата товаров и услуг на задолженность, по которой не действует Рассрочка.\n"
                                + "Снятие наличных не допускается.\n"
                                + "Лимит кредитования устанавливается Банком индивидуально от 10 000 до 300 000 руб.",
                        "от 15 минут",
                        "Возраст: от 18 до 64 лет\n"
                                + "Гражданство: РФ\n"
                                + "Документы: Паспорт",
                        "https://zaimme.ru/images/cr-cards/karta-rassrochki-home-credit-bank.png",
                        "316",
                        "https://zaimme.ru/banks/karty-rassrochki/home_credit_bank_karta_rassrochki.php"

                )
        );

        for (InstallmentCardDetailItem item : listCardDetail) {
            key = mCardDetail.push().getKey();
            Log.d(TAG, "В InstallmentCardDetailItem будет добавлен узел с ключом " + key);
            listInstallmentCardCategory.add
                    (new InstallmentCardCategoryItem(
                                    item.getTitle(),
                                    item.getSubTitle(),
                                    item.getDecisionTime(),
                                    item.getImage(),
                                    item.getInstallment(),
                                    item.getPercent(),
                                    item.getSum(),
                                    key
                            )
                    );
            mCardDetail.child(key).setValue(item);
        }
        mCardCategory.push().setValue(new InstallmentCardCategory("Рассрочка", listInstallmentCardCategory));
        Log.d(TAG, "addInstallmentCards: заполнение InstallmentCardDetailItem");
    }

    private void getAdsImages() {
        mAdsRef.addListenerForSingleValueEvent(new ValueEventListener() {
            @Override
            public void onCancelled(@NonNull final DatabaseError databaseError) {
                Log.e(TAG, "onCancelled: ", databaseError.toException());
            }

            @Override
            public void onDataChange(@NonNull final DataSnapshot dataSnapshot) {
                for (final DataSnapshot item : dataSnapshot.getChildren()) {
                    mAdsImagesList.add(Objects.requireNonNull(item.getValue()).toString());
                }
                carouselView.setPageCount(mAdsImagesList.size());
                mImageListener = (position, imageView) -> Glide.with(getBaseContext())
                        .load(mAdsImagesList.toArray()[position])
                        .into(imageView);
                carouselView.setImageListener(mImageListener);
            }
        });

    }

    private void getCardsByCategoryName(final String categoryName) {
        Log.d(TAG, "getCardsByCategoryName: " + categoryName);
        switch (categoryName) {
            case "Кредитные":
                getCreditCards(categoryName);
                break;
            case "Дебетовые":
                getDebtCards(categoryName);
                break;
            case "Рассрочка":
                getInstallmentCards(categoryName);
                break;
            default:
                notImplemented();
                break;
        }

    }

    private void getCreditCards(String categoryName) {
        mCardCategory.orderByChild("name").equalTo(categoryName)
                .addListenerForSingleValueEvent(new ValueEventListener() {
                    @Override
                    public void onCancelled(@NonNull final DatabaseError databaseError) {
                        Log.e(TAG, "onCancelled: ", databaseError.toException());
                    }

                    @Override
                    public void onDataChange(@NonNull final DataSnapshot dataSnapshot) {
                        if (dataSnapshot.getValue() != null) {
                            for (DataSnapshot item : dataSnapshot.getChildren()) {
                                Query cardsQuery = item.getRef().child("cards").orderByKey();
                                FirebaseRecyclerOptions<CardCategoryItem> listOptions
                                        = new FirebaseRecyclerOptions.Builder<CardCategoryItem>()
                                        .setQuery(cardsQuery, CardCategoryItem.class)
                                        .build();
                                mCardsAdapter = new FirebaseRecyclerAdapter<CardCategoryItem, CardViewHolder>(
                                        listOptions) {
                                    @NonNull
                                    @Override
                                    public CardViewHolder onCreateViewHolder(@NonNull final ViewGroup parent,
                                            final int viewType) {
                                        View itemView = LayoutInflater.from(parent.getContext())
                                                .inflate(R.layout.card_item, parent, false);
                                        return new CardViewHolder(itemView);
                                    }

                                    @Override
                                    protected void onBindViewHolder(@NonNull final CardViewHolder holder,
                                            final int position,
                                            @NonNull final CardCategoryItem model) {
                                        holder.txt_title.setText(model.getTitle());
                                        holder.txt_bonus.setText(model.getBonus());
                                        holder.txt_percent.setText(model.getPercent());
                                        holder.txt_subtitle.setText(model.getSubTitle());
                                        holder.txt_decision_time.setText(model.getDecisionTime());
                                        holder.txt_sum.setText(model.getSum());

                                        Glide.with(getBaseContext())
                                                .load(model.getImage())
                                                .apply(new RequestOptions().fitCenter())
                                                .into(holder.img_logo);

                                        holder.setItemClickListener(
                                                this::onClick);
                                    }

                                    private void onClick(View view, int position1, boolean isLongClick) {
                                        String key = mCardsAdapter.getItem(position1).getId();
                                        Intent cardCreditDetailIntent = new Intent(Cards.this,
                                                CardCreditDetail.class);
                                        cardCreditDetailIntent.putExtra("cardCreditId", key);
                                        startActivity(cardCreditDetailIntent);
                                    }
                                };
                                mCardsAdapter.startListening();
                                mRecyclerView.setAdapter(mCardsAdapter);
                            }
                        } else {
                            if (mCardsAdapter != null) {
                                mCardsAdapter.stopListening();
                            }
                        }
                    }
                });
    }

    private void getDebtCards(String categoryName) {
        mCardCategory.orderByChild("name").equalTo(categoryName)
                .addListenerForSingleValueEvent(new ValueEventListener() {
                    @Override
                    public void onCancelled(@NonNull final DatabaseError databaseError) {
                        Log.e(TAG, "onCancelled: ", databaseError.toException());
                    }

                    @Override
                    public void onDataChange(@NonNull final DataSnapshot dataSnapshot) {
                        if (dataSnapshot.getValue() != null) {
                            for (DataSnapshot item : dataSnapshot.getChildren()) {
                                Query debtCardsQuery = item.getRef().child("cards").orderByKey();
                                FirebaseRecyclerOptions<DebtCardCategoryItem> listOptions
                                        = new FirebaseRecyclerOptions.Builder<DebtCardCategoryItem>()
                                        .setQuery(debtCardsQuery, DebtCardCategoryItem.class)
                                        .build();
                                mDebtCardsAdapter
                                        = new FirebaseRecyclerAdapter<DebtCardCategoryItem, CardDebtViewHolder>(
                                        listOptions) {
                                    @NonNull
                                    @Override
                                    public CardDebtViewHolder onCreateViewHolder(@NonNull final ViewGroup parent,
                                            final int viewType) {
                                        View itemView = LayoutInflater.from(parent.getContext())
                                                .inflate(R.layout.debt_card_item, parent, false);
                                        return new CardDebtViewHolder(itemView);
                                    }

                                    @Override
                                    protected void onBindViewHolder(@NonNull final CardDebtViewHolder holder,
                                            final int position,
                                            @NonNull final DebtCardCategoryItem model) {
                                        holder.txt_title.setText(model.getTitle());
                                        holder.txt_subtitle.setText(model.getSubTitle());
                                        holder.txt_description.setText(model.getDescription());

                                        Glide.with(getBaseContext())
                                                .load(model.getImage())
                                                .apply(new RequestOptions().fitCenter())
                                                .into(holder.img_logo);

                                        holder.setItemClickListener(
                                                this::onClick);
                                    }

                                    private void onClick(View view, int position1, boolean isLongClick) {
                                        String key = mDebtCardsAdapter.getItem(position1).getId();
                                        Intent debtCardDetailIntent = new Intent(Cards.this, CardDebtDetail.class);
                                        debtCardDetailIntent.putExtra("debtCardId", key);
                                        startActivity(debtCardDetailIntent);
                                    }
                                };
                                mDebtCardsAdapter.startListening();
                                mRecyclerView.setAdapter(mDebtCardsAdapter);
                            }
                        } else {
                            if (mDebtCardsAdapter != null) {
                                mDebtCardsAdapter.stopListening();
                            }
                        }
                    }
                });
    }

    private void getInstallmentCards(String categoryName) {
        mCardCategory.orderByChild("name").equalTo(categoryName)
                .addListenerForSingleValueEvent(new ValueEventListener() {
                    @Override
                    public void onCancelled(@NonNull final DatabaseError databaseError) {
                        Log.e(TAG, "onCancelled: ", databaseError.toException());
                    }

                    @Override
                    public void onDataChange(@NonNull final DataSnapshot dataSnapshot) {
                        if (dataSnapshot.getValue() != null) {
                            for (DataSnapshot item : dataSnapshot.getChildren()) {
                                Query installmentCardsQuery = item.getRef().child("cards").orderByKey();
                                FirebaseRecyclerOptions<InstallmentCardCategoryItem> listOptions
                                        = new FirebaseRecyclerOptions.Builder<InstallmentCardCategoryItem>()
                                        .setQuery(installmentCardsQuery, InstallmentCardCategoryItem.class)
                                        .build();
                                mInstallmentCardsAdapter
                                        = new FirebaseRecyclerAdapter<InstallmentCardCategoryItem, CardInstallmentViewHolder>(
                                        listOptions) {
                                    @NonNull
                                    @Override
                                    public CardInstallmentViewHolder onCreateViewHolder(
                                            @NonNull final ViewGroup parent,
                                            final int viewType) {
                                        View itemView = LayoutInflater.from(parent.getContext())
                                                .inflate(R.layout.installment_card_item, parent, false);
                                        return new CardInstallmentViewHolder(itemView);
                                    }

                                    @Override
                                    protected void onBindViewHolder(@NonNull final CardInstallmentViewHolder holder,
                                            final int position,
                                            @NonNull final InstallmentCardCategoryItem model) {
                                        holder.txt_title.setText(model.getTitle());
                                        holder.txt_subtitle.setText(model.getSubTitle());
                                        holder.txt_decision_time.setText(model.getDecisionTime());
                                        holder.txt_installment.setText(model.getInstallment());
                                        holder.txt_percent.setText(model.getPercent());
                                        holder.txt_sum.setText(model.getSum());

                                        Glide.with(getBaseContext())
                                                .load(model.getImage())
                                                .apply(new RequestOptions().fitCenter())
                                                .into(holder.img_logo);

                                        holder.setItemClickListener(
                                                this::onClick);
                                    }

                                    private void onClick(View view, int position1, boolean isLongClick) {
                                        String key = mInstallmentCardsAdapter.getItem(position1).getId();
                                        Intent installmentCardDetailIntent = new Intent(Cards.this,
                                                CardInstallmentDetail.class);
                                        installmentCardDetailIntent.putExtra("installmentCardId", key);
                                        startActivity(installmentCardDetailIntent);
                                    }
                                };
                                mInstallmentCardsAdapter.startListening();
                                mRecyclerView.setAdapter(mInstallmentCardsAdapter);
                            }
                        } else {
                            if (mInstallmentCardsAdapter != null) {
                                mInstallmentCardsAdapter.stopListening();
                            }
                        }
                    }
                });
    }

    private void initCarousel() {
        //Carousel
        carouselView = findViewById(R.id.carousel);
        mImageListener = (position, imageView) -> Glide.with(getBaseContext())
                .load(mAdsImagesList.toArray()[position])
                .into(imageView);
        carouselView.setImageListener(mImageListener);
        carouselView.setImageClickListener(
                position -> Toast.makeText(Cards.this, "Выбран элемент: " + position, Toast.LENGTH_SHORT).show());
        getAdsImages();
    }

    private void initFirebase() {
        //Firebase Init
        final FirebaseDatabase database = FirebaseDatabase.getInstance();
        mAdsRef = database.getReference("Ads");
        mCardCategory = database.getReference("CardCategory");
        mCardDetail = database.getReference("CardDetail");
    }

    private void initRecyclerView() {
        //Recycler
        mRecyclerView = findViewById(R.id.cardsList);
        mRecyclerView.setHasFixedSize(true);
        RecyclerView.LayoutManager layoutManager = new LinearLayoutManager(this);
        mRecyclerView.setLayoutManager(layoutManager);
    }

    private void initTabs() {
        mTabLayout = findViewById(R.id.tabs);
        mTabLayout.addOnTabSelectedListener(new OnTabSelectedListener() {
            @Override
            public void onTabReselected(final Tab tab) {
            }

            @Override
            public void onTabSelected(final Tab tab) {
                //Загружаем офферы по названию выбранного таба
                if (tab.getText() != null) {
                    getCardsByCategoryName(tab.getText().toString());
                }
            }

            @Override
            public void onTabUnselected(final Tab tab) {
            }
        });
    }

    private void initValues() {
//        addCreditCards();
//        addDebtCards();
//        addRassCards();
    }

    private void notImplemented() {
        Toast.makeText(this, "Не реализовано!", Toast.LENGTH_SHORT).show();
    }
}
