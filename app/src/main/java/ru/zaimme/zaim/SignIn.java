package ru.zaimme.zaim;

import static android.text.TextUtils.isEmpty;

import android.app.ProgressDialog;
import android.content.Context;
import android.content.Intent;
import android.os.Bundle;
import android.support.annotation.NonNull;
import android.support.design.widget.TextInputLayout;
import android.support.v7.app.AppCompatActivity;
import android.text.TextUtils;
import android.util.Log;
import android.util.Pair;
import android.view.View;
import android.view.inputmethod.InputMethodManager;
import android.widget.Button;
import android.widget.Toast;
import com.google.firebase.auth.FirebaseAuth;
import com.google.firebase.auth.FirebaseAuthInvalidCredentialsException;
import com.google.firebase.auth.FirebaseAuthInvalidUserException;
import com.google.firebase.auth.FirebaseUser;
import com.google.firebase.database.DataSnapshot;
import com.google.firebase.database.DatabaseError;
import com.google.firebase.database.DatabaseReference;
import com.google.firebase.database.FirebaseDatabase;
import com.google.firebase.database.ValueEventListener;
import com.google.firebase.iid.FirebaseInstanceId;
import com.jakewharton.rxbinding2.widget.RxTextView;
import com.rey.material.widget.CheckBox;
import io.paperdb.Paper;
import io.reactivex.Observable;
import io.reactivex.android.schedulers.AndroidSchedulers;
import io.reactivex.disposables.CompositeDisposable;
import io.reactivex.disposables.Disposable;
import java.util.Objects;
import java.util.concurrent.TimeUnit;
import java.util.regex.Matcher;
import java.util.regex.Pattern;
import ru.zaimme.zaim.Common.Common;
import ru.zaimme.zaim.Model.UserInfo;

public class SignIn extends AppCompatActivity {

    private static final String TAG = "SignUp";
    private final CompositeDisposable mDisposables = new CompositeDisposable();
    private Button btnSignIn;
    private Button btn_reset_password;
    private CheckBox ckbRemember;
    private FirebaseAuth mAuth;
    private DatabaseReference mUserInfo;

    private TextInputLayout tilEmail;

    private TextInputLayout tilPassword;

    private Observable<Boolean> emailObservable;

    private Observable<Boolean> passwordObservable;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_sign_in);
        mAuth = FirebaseAuth.getInstance();
        //Init Paper
        initPaper();

        //Init FireBase
        initFireBase();

        //Init UI
        initUI();

        // CombineLatest
        editTextObservables();

        btnSignIn.setOnClickListener(v -> signInWithFirebase());
        btn_reset_password.setOnClickListener(v -> resetPassword());
    }

    @Override
    protected void onStop() {
        super.onStop();
        if (!mDisposables.isDisposed()) {
            mDisposables.clear();
        }
    }

    @Override
    protected void onResume() {
        super.onResume();
        editTextObservables();
    }

    private void editTextObservables() {
        emailObservable = RxTextView.textChanges(Objects.requireNonNull(tilEmail.getEditText()))
                .skip(1)
                .debounce(300, TimeUnit.MILLISECONDS)
                .observeOn(AndroidSchedulers.mainThread())
                .map(string -> {
                    Pair<Boolean, CharSequence> result = isEmailValid(string);
                    boolean showError = result.first;
                    CharSequence errorText = result.second;
                    tilEmail.setErrorEnabled(!showError);
                    tilEmail.setError(errorText);
                    return showError;
                });


        passwordObservable = RxTextView.textChanges(Objects.requireNonNull(tilPassword.getEditText()))
                .skip(1)
                .debounce(300, TimeUnit.MILLISECONDS)
                .observeOn(AndroidSchedulers.mainThread())
                .map(string -> {
                    Pair<Boolean, CharSequence> result = isPasswordValid(string);
                    boolean showError = result.first;
                    CharSequence errorText = result.second;
                    tilPassword.setErrorEnabled(!showError);
                    tilPassword.setError(errorText);
                    return showError;
                });

        Disposable disposableSignIn = Observable.combineLatest(
                emailObservable,
                passwordObservable,
                (email, pass) -> email && pass)
                .distinctUntilChanged()
                .subscribe(result -> btnSignIn.setEnabled(result));

        Disposable disposableResetPassword = emailObservable
                .distinctUntilChanged()
                .subscribe(result -> {
                    Log.d(TAG, "editTextObservables: " + result);
                    btn_reset_password.setEnabled(result);
                });

        mDisposables.add(disposableResetPassword);
        mDisposables.add(disposableSignIn);
    }

    private void writeUserInfo(final FirebaseUser user) {
        if (user != null) {
            UserInfo userInfo = new UserInfo();
            String userId = user.getUid();
            mUserInfo.child(userId).addListenerForSingleValueEvent(new ValueEventListener() {
                @Override
                public void onDataChange(@NonNull DataSnapshot dataSnapshot) {
                    if (!dataSnapshot.exists()) {
                        // Создаем нового пользователя
                        mUserInfo.child(userId).setValue(userInfo).addOnCompleteListener(task -> {
                            if (task.isSuccessful()) {
                                Log.d(TAG, "Создан новый пользователь");
                                Common.user = userInfo;
                            } else {
                                Log.w(TAG, "Ошибка создания нового пользователя", task.getException());
                                Toast.makeText(SignIn.this, "Ошибка создания нового пользователя",
                                        Toast.LENGTH_SHORT).show();
                            }
                        });
                    } else {
                        //Обновляем только токен
                        UserInfo oldUserInfo = dataSnapshot.getValue(UserInfo.class);
                        String newDeviceToken = FirebaseInstanceId.getInstance().getToken();
                        Objects.requireNonNull(oldUserInfo).setToken(newDeviceToken);
                        mUserInfo.child(userId).setValue(oldUserInfo).addOnCompleteListener(task -> {
                            if (task.isSuccessful()) {
                                Common.user = oldUserInfo;
                                Log.d(TAG, "Токен пользователя " + userId + " был обновлен на " + newDeviceToken);
                            } else {
                                Log.d(TAG, "Не удалось обнвоить токен пользователя " + userId);
                            }
                        });
                    }
                }

                @Override
                public void onCancelled(@NonNull DatabaseError databaseError) {
                    Log.e(TAG, "onCancelled: ", databaseError.toException());
                }
            });
        }
    }

    private void hideKeyboard() {
        View view = getCurrentFocus();
        if (view != null) {
            ((InputMethodManager) Objects.requireNonNull(getSystemService(Context.INPUT_METHOD_SERVICE))).
                    hideSoftInputFromWindow(view.getWindowToken(), InputMethodManager.HIDE_NOT_ALWAYS);
        }
    }

    private void initFireBase() {
        FirebaseDatabase database = FirebaseDatabase.getInstance();
        mUserInfo = database.getReference("UserInfo");
    }

    private void initPaper() {
        Paper.init(this);
    }

    private void initUI() {
        tilEmail = findViewById(R.id.tilEmail);
        tilPassword = findViewById(R.id.tilPassword);
        btnSignIn = findViewById(R.id.btnSignIn);
        ckbRemember = findViewById(R.id.ckbRemember);
        btn_reset_password = findViewById(R.id.btn_reset_password);
    }

    private Pair<Boolean, CharSequence> isEmailValid(CharSequence charSequence) {
        if (TextUtils.isEmpty(charSequence)) {
            return new Pair<>(false, "Пустое поле");
        }
        Pattern pattern = Pattern.compile(".+@.+\\.[a-z]+");
        Matcher matcher = pattern.matcher(charSequence.toString());
        if (matcher.matches()) {
            return new Pair<>(true, "");
        } else {
            return new Pair<>(false, "Неверный e-mail адрес");
        }
    }

    private Pair<Boolean, CharSequence> isPasswordValid(CharSequence charSequence) {
        if (isEmpty(charSequence)) {
            return new Pair<>(false, "Пустое поле");
        }
        if (charSequence.length() < 6) {
            return new Pair<>(false, "Пароль слишком короткий");
        }
        return new Pair<>(true, "");
    }

    private void resetPassword() {
        String email = Objects.requireNonNull(tilEmail.getEditText()).getText().toString();
        mAuth.sendPasswordResetEmail(email)
                .addOnCompleteListener(task -> {
                    if (task.isSuccessful()) {
                        Toast.makeText(SignIn.this, "Письмо со сбросом пароля отправлено!",
                                Toast.LENGTH_SHORT).show();
                    } else {
                        Toast.makeText(SignIn.this, "Адрес не зарегистрирован!", Toast.LENGTH_SHORT).show();
                    }
                });
    }

    private void signInWithFirebase() {
        hideKeyboard();
        if (ckbRemember.isChecked()) {
            Paper.book().write(Common.EMAIL_KEY, Objects.requireNonNull(tilEmail.getEditText()).getText().toString());
            Paper.book().write(Common.PASSWORD_KEY, Objects.requireNonNull(tilPassword.getEditText()).getText().toString());
        }

        String email = Objects.requireNonNull(tilEmail.getEditText()).getText().toString();
        String password = Objects.requireNonNull(tilPassword.getEditText()).getText().toString();

        final ProgressDialog mDialog = new ProgressDialog(SignIn.this);
        mDialog.setMessage("Ожидайте...");
        mDialog.show();

        mAuth.signInWithEmailAndPassword(email, password).addOnCompleteListener(this,
                task -> {
                    if (task.isSuccessful()) {
                        Log.d(TAG, "signInWithEmail:success");
                        FirebaseUser user = mAuth.getCurrentUser();
                        writeUserInfo(user);
                        Intent homeIntent = new Intent(SignIn.this, Home.class);
                        startActivity(homeIntent);
                        finish();
                    } else {
                        // If sign in fails, display a message to the user.
                        Log.w(TAG, "signInWithEmail:failure", task.getException());
                        Toast.makeText(SignIn.this, "Ошибка аутентификации.",
                                Toast.LENGTH_SHORT).show();
                    }
                    mDialog.dismiss();
                })
                .addOnFailureListener(e -> {
                    if (e instanceof FirebaseAuthInvalidCredentialsException) {
                        Toast.makeText(SignIn.this, "Введен неправильный пароль", Toast.LENGTH_SHORT).show();
                    }
                    if (e instanceof FirebaseAuthInvalidUserException) {
                        Toast.makeText(SignIn.this, "Учетная запись не существует", Toast.LENGTH_SHORT).show();
                    }
                });
    }

}