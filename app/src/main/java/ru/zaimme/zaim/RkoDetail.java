package ru.zaimme.zaim;


import android.content.Intent;
import android.graphics.Color;
import android.net.Uri;
import android.os.Bundle;
import android.support.annotation.NonNull;
import android.support.v7.app.AppCompatActivity;
import android.support.v7.widget.Toolbar;
import android.util.Log;
import android.view.MenuItem;
import android.widget.Button;
import android.widget.ImageView;
import android.widget.TextView;
import android.widget.Toast;
import com.bumptech.glide.Glide;
import com.bumptech.glide.request.RequestOptions;
import com.google.firebase.database.DataSnapshot;
import com.google.firebase.database.DatabaseError;
import com.google.firebase.database.DatabaseReference;
import com.google.firebase.database.FirebaseDatabase;
import com.google.firebase.database.ValueEventListener;
import com.mikepenz.fontawesome_typeface_library.FontAwesome.Icon;
import com.mikepenz.iconics.IconicsDrawable;
import java.util.Objects;
import ru.zaimme.zaim.Model.RkoDetailModel;

public class RkoDetail extends AppCompatActivity {

    private static final String TAG = "RkoDetail";

    private Button btn_request;

    private ImageView image;

    private DatabaseReference mRkoDetail;

    private String rkoId;

    private TextView title, subtitle, description, account, advantages;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_rko_detail);

        // Handle Toolbar
        Toolbar toolbar = findViewById(R.id.toolbar);
        setSupportActionBar(toolbar);
        //set the back arrow in the toolbar
        Objects.requireNonNull(getSupportActionBar()).setDisplayHomeAsUpEnabled(true);
        getSupportActionBar().setTitle("");
        getSupportActionBar().setHomeAsUpIndicator(
                new IconicsDrawable(this, Icon.faw_arrow_left)
                        .color(Color.WHITE)
                        .sizeDp(IconicsDrawable.TOOLBAR_ICON_SIZE)
                        .paddingDp(IconicsDrawable.TOOLBAR_ICON_PADDING)
        );

        initFirebase();
        initUI();
        initListeners();
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        switch (item.getItemId()) {
            case android.R.id.home:
                onBackPressed();
                return true;

            default:
                return super.onOptionsItemSelected(item);
        }
    }

    public void openWebPage(String url) {
        Uri webpage = Uri.parse(url);
        Intent intent = new Intent(Intent.ACTION_VIEW, webpage);
        if (intent.resolveActivity(getPackageManager()) != null) {
            startActivity(intent);
        }
    }

    private void initFirebase() {
        //Firebase Init
        final FirebaseDatabase database = FirebaseDatabase.getInstance();
        mRkoDetail = database.getReference("RkoDetail");
    }

    private void initListeners() {
        //Firebase Listener
        mRkoDetail.orderByKey().equalTo(rkoId)
                .addListenerForSingleValueEvent(new ValueEventListener() {
                    @Override
                    public void onCancelled(@NonNull final DatabaseError databaseError) {
                        Toast.makeText(RkoDetail.this, databaseError.getMessage(), Toast.LENGTH_SHORT)
                                .show();
                    }

                    @Override
                    public void onDataChange(@NonNull final DataSnapshot dataSnapshot) {
                        try {
                            RkoDetailModel rkoDetailModel = new RkoDetailModel();
                            if (dataSnapshot != null) {
                                for (DataSnapshot item : dataSnapshot.getChildren()) {
                                    rkoDetailModel = item.getValue(RkoDetailModel.class);
                                    //Заголовок
                                    Glide.with(getBaseContext())
                                            .load(Objects.requireNonNull(rkoDetailModel).getImage())
                                            .apply(new RequestOptions().fitCenter())
                                            .into(image);
                                    title.setText(rkoDetailModel.getTitle());
                                    getSupportActionBar().setTitle(rkoDetailModel.getTitle());
                                    subtitle.setText(rkoDetailModel.getSubtitle());
                                    description.setText(rkoDetailModel.getDescription());
                                    account.setText(rkoDetailModel.getAccount());
                                    advantages.setText(rkoDetailModel.getAdvantages());

                                    final RkoDetailModel finalRkoDetailModel
                                            = rkoDetailModel;
                                    btn_request.setOnClickListener(
                                            v -> openWebPage(finalRkoDetailModel.getUrl()));


                                }
                            }
                        } catch (Exception e) {
                            String msg = "Ошибка при загрузке РКО с ID=" + rkoId;
                            Log.e(TAG, msg, e);
                            Toast.makeText(RkoDetail.this, msg, Toast.LENGTH_SHORT).show();
                        }
                    }
                });
    }

    private void initUI() {
        title = findViewById(R.id.title);
        subtitle = findViewById(R.id.subtitle);
        description = findViewById(R.id.description);
        account = findViewById(R.id.account);
        advantages = findViewById(R.id.advantages);
        btn_request = findViewById(R.id.btn_request);
        image = findViewById(R.id.image);

        //Получение переданного rkoId
        if (getIntent() != null) {
            rkoId = getIntent().getStringExtra("rkoId");
        }
    }
}
