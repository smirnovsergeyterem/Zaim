package ru.zaimme.zaim;


import android.content.Intent;
import android.graphics.Color;
import android.net.Uri;
import android.os.Bundle;
import android.support.annotation.NonNull;
import android.support.v7.app.AppCompatActivity;
import android.support.v7.widget.Toolbar;
import android.util.Log;
import android.view.MenuItem;
import android.widget.Button;
import android.widget.ImageView;
import android.widget.TextView;
import android.widget.Toast;
import com.bumptech.glide.Glide;
import com.bumptech.glide.request.RequestOptions;
import com.google.firebase.database.DataSnapshot;
import com.google.firebase.database.DatabaseError;
import com.google.firebase.database.DatabaseReference;
import com.google.firebase.database.FirebaseDatabase;
import com.google.firebase.database.ValueEventListener;
import com.mikepenz.fontawesome_typeface_library.FontAwesome.Icon;
import com.mikepenz.iconics.IconicsDrawable;
import java.util.Objects;
import ru.zaimme.zaim.Model.Debt.DebtCardDetailItem;


public class CardDebtDetail extends AppCompatActivity {

    private static final String TAG = "CardDebtDetail";

    private Button btn_request;

    private String debtCardId;

    private ImageView image;

    private DatabaseReference mDebtCardDetail;

    private TextView title, subtitle, description, additionalInfo, license;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_card_debt_detail);

        // Handle Toolbar
        Toolbar toolbar = findViewById(R.id.toolbar);
        setSupportActionBar(toolbar);
        //set the back arrow in the toolbar
        getSupportActionBar().setDisplayHomeAsUpEnabled(true);
        getSupportActionBar().setTitle("");
        getSupportActionBar().setHomeAsUpIndicator(
                new IconicsDrawable(this, Icon.faw_arrow_left)
                        .color(Color.WHITE)
                        .sizeDp(IconicsDrawable.TOOLBAR_ICON_SIZE)
                        .paddingDp(IconicsDrawable.TOOLBAR_ICON_PADDING)
        );

        initFirebase();
        initUI();
        initListeners();
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        switch (item.getItemId()) {
            case android.R.id.home:
                onBackPressed();
                return true;

            default:
                return super.onOptionsItemSelected(item);
        }
    }

    public void openWebPage(String url) {
        Uri webpage = Uri.parse(url);
        Intent intent = new Intent(Intent.ACTION_VIEW, webpage);
        if (intent.resolveActivity(getPackageManager()) != null) {
            startActivity(intent);
        }
    }

    private void initFirebase() {
        //Firebase Init
        final FirebaseDatabase database = FirebaseDatabase.getInstance();
        mDebtCardDetail = database.getReference("CardDetail");
    }

    private void initListeners() {
        //Firebase Listener
        mDebtCardDetail.orderByKey().equalTo(debtCardId).addListenerForSingleValueEvent(new ValueEventListener() {
            @Override
            public void onCancelled(@NonNull final DatabaseError databaseError) {
                Toast.makeText(CardDebtDetail.this, databaseError.getMessage(), Toast.LENGTH_SHORT).show();
            }

            @Override
            public void onDataChange(@NonNull final DataSnapshot dataSnapshot) {
                try {
                    DebtCardDetailItem debtCardDetailItem = new DebtCardDetailItem();
                    if (dataSnapshot != null) {
                        for (DataSnapshot item : dataSnapshot.getChildren()) {
                            debtCardDetailItem = item.getValue(DebtCardDetailItem.class);
                            //Заголовок
                            getSupportActionBar().setTitle(debtCardDetailItem.getTitle());
                            title.setText(Objects.requireNonNull(debtCardDetailItem).getTitle());
                            subtitle.setText(debtCardDetailItem.getSubTitle());
                            description.setText(debtCardDetailItem.getDescription());
                            license.setText(String.format("Лицензия банка №%s", debtCardDetailItem.getLicense()));
                            additionalInfo.setText(debtCardDetailItem.getAdditionalInfo());

                            final DebtCardDetailItem finalDebtCardDetailItem = debtCardDetailItem;
                            btn_request.setOnClickListener(v -> openWebPage(finalDebtCardDetailItem.getUrl()));

                            Glide.with(getBaseContext())
                                    .load(debtCardDetailItem.getImage())
                                    .apply(new RequestOptions().fitCenter())
                                    .into(image);
                        }
                    }
                } catch (Exception e) {
                    String msg = "Ошибка при загрузке дебетовой карты с ID=" + debtCardId;
                    Log.e(TAG, msg, e);
                    Toast.makeText(CardDebtDetail.this, msg, Toast.LENGTH_SHORT).show();
                }
            }
        });
    }

    private void initUI() {
        title = findViewById(R.id.title);
        subtitle = findViewById(R.id.subtitle);
        description = findViewById(R.id.description);
        additionalInfo = findViewById(R.id.additionalInfo);
        btn_request = findViewById(R.id.btn_request);
        image = findViewById(R.id.image);
        license = findViewById(R.id.license);

        //Получение переданного debtCardId
        if (getIntent() != null) {
            debtCardId = getIntent().getStringExtra("debtCardId");
        }
    }
}
