package ru.zaimme.zaim;

import static android.text.TextUtils.isEmpty;

import android.app.ProgressDialog;
import android.content.Context;
import android.graphics.Color;
import android.os.Bundle;
import android.support.design.widget.FloatingActionButton;
import android.support.design.widget.TextInputLayout;
import android.support.v7.app.AppCompatActivity;
import android.support.v7.widget.Toolbar;
import android.util.Log;
import android.util.Pair;
import android.view.MenuItem;
import android.view.View;
import android.view.inputmethod.InputMethodManager;
import android.widget.Toast;
import com.google.firebase.auth.FirebaseAuth;
import com.google.firebase.database.DatabaseReference;
import com.google.firebase.database.FirebaseDatabase;
import com.jakewharton.rxbinding2.widget.RxTextView;
import com.mikepenz.fontawesome_typeface_library.FontAwesome.Icon;
import com.mikepenz.iconics.IconicsDrawable;
import io.reactivex.Observable;
import io.reactivex.android.schedulers.AndroidSchedulers;
import io.reactivex.disposables.CompositeDisposable;
import io.reactivex.disposables.Disposable;
import java.util.Objects;
import java.util.concurrent.TimeUnit;
import ru.zaimme.zaim.Model.Message;

public class Support extends AppCompatActivity {

    private FloatingActionButton fab_send;

    private FirebaseAuth mAuth;

    private final CompositeDisposable mDisposables = new CompositeDisposable();

    private DatabaseReference messageRef;

    private TextInputLayout tilMessage;

    private TextInputLayout tilTheme;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_support);

        // Handle Toolbar
        Toolbar toolbar = findViewById(R.id.toolbar);
        setSupportActionBar(toolbar);
        //set the back arrow in the toolbar
        Objects.requireNonNull(getSupportActionBar()).setDisplayHomeAsUpEnabled(true);
        getSupportActionBar().setTitle("Поддержка");
        getSupportActionBar().setHomeAsUpIndicator(
                new IconicsDrawable(this, Icon.faw_arrow_left)
                        .color(Color.WHITE)
                        .sizeDp(IconicsDrawable.TOOLBAR_ICON_SIZE)
                        .paddingDp(IconicsDrawable.TOOLBAR_ICON_PADDING)
        );

        //Init FireBase
        initFireBase();

        //Init UI
        initUI();

        editTextObservables();

        fab_send.setOnClickListener(v -> sendMessage());
    }

    @Override
    protected void onStop() {
        super.onStop();
        if (!mDisposables.isDisposed()) {
            mDisposables.dispose();
        }
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        switch (item.getItemId()) {
            case android.R.id.home:
                onBackPressed();
                return true;

            default:
                return super.onOptionsItemSelected(item);
        }
    }

    private void clearFields() {
        Objects.requireNonNull(tilMessage.getEditText()).setText("");
        Objects.requireNonNull(tilTheme.getEditText()).setText("");
    }

    private void editTextObservables() {
        Observable<Boolean> themeObservable =
                RxTextView.textChanges(Objects.requireNonNull(tilTheme.getEditText()))
                        .skip(1)
                        .debounce(300, TimeUnit.MILLISECONDS)
                        .observeOn(AndroidSchedulers.mainThread())
                        .map(string -> {
                            Pair<Boolean, CharSequence> result = isValid(string);
                            boolean showError = result.first;
                            CharSequence errorText = result.second;
                            tilTheme.setErrorEnabled(!showError);
                            tilTheme.setError(errorText);
                            return showError;
                        });

        Observable<Boolean> messageObservable =
                RxTextView.textChanges(Objects.requireNonNull(tilMessage.getEditText()))
                        .skip(1)
                        .debounce(300, TimeUnit.MILLISECONDS)
                        .observeOn(AndroidSchedulers.mainThread())
                        .map(string -> {
                            Pair<Boolean, CharSequence> result = isValid(string);
                            boolean showError = result.first;
                            CharSequence errorText = result.second;
                            tilMessage.setErrorEnabled(!showError);
                            tilMessage.setError(errorText);
                            return showError;
                        });

        Disposable disposableSendMessage = Observable.combineLatest(
                themeObservable,
                messageObservable,
                (theme, message) -> theme && message)
                .distinctUntilChanged()
                .subscribe((Boolean result) -> {
                    Log.d("Support", "is FAB enabled: " + result);
                    fab_send.setEnabled(result);
                });

        mDisposables.add(disposableSendMessage);
    }

    private void hideKeyboard() {
        View view = getCurrentFocus();
        if (view != null) {
            ((InputMethodManager) Objects.requireNonNull(getSystemService(Context.INPUT_METHOD_SERVICE))).
                    hideSoftInputFromWindow(view.getWindowToken(), InputMethodManager.HIDE_NOT_ALWAYS);
        }
    }

    private void initFireBase() {
        mAuth = FirebaseAuth.getInstance();
        FirebaseDatabase database = FirebaseDatabase.getInstance();
        messageRef = database.getReference("Message");
    }

    private void initUI() {
        tilTheme = findViewById(R.id.tilTheme);
        tilMessage = findViewById(R.id.tilMessage);
        fab_send = findViewById(R.id.fab_send);
        fab_send.setEnabled(false);
    }

    private Pair<Boolean, CharSequence> isValid(CharSequence charSequence) {
        if (isEmpty(charSequence)) {
            return new Pair<>(false, "Пусто поле");
        }
        if (charSequence.length() < 3) {
            return new Pair<>(false, "Длина меньше 3 симоволов");
        }
        if (charSequence.length() > 600) {
            return new Pair<>(false, "Длина превышает 600 симоволов");
        }
        return new Pair<>(true, "");
    }

    private void sendMessage() {
        hideKeyboard();

        final String theme = Objects.requireNonNull(tilTheme.getEditText()).getText().toString();
        final String message = Objects.requireNonNull(tilMessage.getEditText()).getText().toString();

        if (theme.equals("")) {
            tilTheme.setError("Не заполнено поле");
        } else {
            tilTheme.setErrorEnabled(false);
        }
        if (message.equals("")) {
            tilMessage.setError("Не заполнено поле");
        } else {
            tilMessage.setErrorEnabled(false);
        }

        if (theme.equals("") || message.equals("")) {
            return;
        }
        final ProgressDialog mDialog = new ProgressDialog(Support.this);
        mDialog.setMessage("Ожидайте...");
        mDialog.show();

        Message msg = new Message(
                tilTheme.getEditText().getText().toString(),
                tilMessage.getEditText().getText().toString());

        messageRef.child(Objects.requireNonNull(mAuth.getCurrentUser()).getUid()).push().setValue(msg).addOnCompleteListener(task -> {
            mDialog.dismiss();
            clearFields();
            if (task.isSuccessful()) {
                Toast.makeText(Support.this, "Сообщение отправлено", Toast.LENGTH_SHORT).show();
            } else {
                Exception e = task.getException();
                Toast.makeText(Support.this, "Сообщение не отправлено. " + Objects.requireNonNull(e).getMessage(),
                        Toast.LENGTH_SHORT)
                        .show();
            }
        });

    }
}
