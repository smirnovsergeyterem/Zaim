package ru.zaimme.zaim.ViewHolder;

import android.support.annotation.NonNull;
import android.support.v7.widget.RecyclerView;
import android.view.View;
import android.widget.ImageView;
import android.widget.TextView;
import ru.zaimme.zaim.Interface.ItemClickListener;
import ru.zaimme.zaim.R;

@SuppressWarnings("unused")
public class CreditViewHolder extends RecyclerView.ViewHolder implements View.OnClickListener {

    public ImageView img_logo, img_more;

    public ItemClickListener mItemClickListener;

    public TextView txt_title, txt_description, txt_subtitle;

    public CreditViewHolder(@NonNull final View itemView) {
        super(itemView);
        txt_title = itemView.findViewById(R.id.txt_title);
        txt_description = itemView.findViewById(R.id.txt_description);
        txt_subtitle = itemView.findViewById(R.id.txt_subtitle);
        img_logo = itemView.findViewById(R.id.img_logo);
        img_more = itemView.findViewById(R.id.img_more);
        itemView.setOnClickListener(this);
    }

    public ItemClickListener getItemClickListener() {
        return mItemClickListener;
    }

    public void setItemClickListener(final ItemClickListener itemClickListener) {
        mItemClickListener = itemClickListener;
    }

    @Override
    public void onClick(final View v) {
        mItemClickListener.onClick(v, getAdapterPosition(), false);
    }
}
