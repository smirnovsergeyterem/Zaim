package ru.zaimme.zaim.ViewHolder;

import android.support.annotation.NonNull;
import android.support.v7.widget.RecyclerView;
import android.view.View;
import android.widget.ImageView;
import android.widget.TextView;
import ru.zaimme.zaim.Interface.ItemClickListener;
import ru.zaimme.zaim.R;

@SuppressWarnings("unused")
public class CardInstallmentViewHolder extends RecyclerView.ViewHolder implements View.OnClickListener {

    public ImageView img_logo;

    public ItemClickListener mItemClickListener;

    public TextView txt_title, txt_subtitle, txt_installment, txt_percent, txt_sum, txt_decision_time;

    public CardInstallmentViewHolder(@NonNull final View itemView) {
        super(itemView);
        txt_title = itemView.findViewById(R.id.txt_title);
        txt_subtitle = itemView.findViewById(R.id.txt_subtitle);
        txt_installment = itemView.findViewById(R.id.txt_installment);
        txt_percent = itemView.findViewById(R.id.txt_percent);
        txt_sum = itemView.findViewById(R.id.txt_sum);
        txt_decision_time = itemView.findViewById(R.id.txt_decision_time);
        img_logo = itemView.findViewById(R.id.img_logo);
        itemView.setOnClickListener(this);
    }

    public ItemClickListener getItemClickListener() {
        return mItemClickListener;
    }

    public void setItemClickListener(final ItemClickListener itemClickListener) {
        mItemClickListener = itemClickListener;
    }

    @Override
    public void onClick(final View v) {
        mItemClickListener.onClick(v, getAdapterPosition(), false);
    }

}
