package ru.zaimme.zaim.ViewHolder;

import android.support.v7.widget.RecyclerView;
import android.view.View;
import android.widget.ImageView;
import android.widget.TextView;
import ru.zaimme.zaim.Interface.ItemClickListener;
import ru.zaimme.zaim.R;

@SuppressWarnings("unused")
public class OfferViewHolder extends RecyclerView.ViewHolder implements View.OnClickListener {

    public ImageView img_offer_logo, img_more;

    public TextView txt_offer_name, txt_views, txt_money, txt_time, txt_percent;

    private ItemClickListener mItemClickListener;

    public OfferViewHolder(final View itemView) {
        super(itemView);
        txt_offer_name = itemView.findViewById(R.id.txt_offer_name);
        txt_views = itemView.findViewById(R.id.txt_views);
        txt_money = itemView.findViewById(R.id.txt_money);
        txt_time = itemView.findViewById(R.id.txt_time);
        txt_percent = itemView.findViewById(R.id.txt_percent);
        img_offer_logo = itemView.findViewById(R.id.img_offer_logo);
        img_more = itemView.findViewById(R.id.img_more);
        itemView.setOnClickListener(this);
    }

    public ItemClickListener getItemClickListener() {
        return mItemClickListener;
    }

    public void setItemClickListener(final ItemClickListener itemClickListener) {
        mItemClickListener = itemClickListener;
    }

    @Override
    public void onClick(final View v) {
        mItemClickListener.onClick(v, getAdapterPosition(), false);
    }
}
