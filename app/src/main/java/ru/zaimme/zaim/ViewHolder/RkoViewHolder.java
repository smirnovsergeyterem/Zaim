package ru.zaimme.zaim.ViewHolder;

import android.support.annotation.NonNull;
import android.support.v7.widget.RecyclerView;
import android.view.View;
import android.widget.ImageView;
import android.widget.TextView;
import ru.zaimme.zaim.Interface.ItemClickListener;
import ru.zaimme.zaim.R;

@SuppressWarnings("unused")
public class RkoViewHolder extends RecyclerView.ViewHolder implements View.OnClickListener {

    public ImageView logo;

    public ItemClickListener mItemClickListener;

    public TextView title, description, subtitle;

    public RkoViewHolder(@NonNull final View itemView) {
        super(itemView);
        logo = itemView.findViewById(R.id.logo);
        title = itemView.findViewById(R.id.title);
        subtitle = itemView.findViewById(R.id.subtitle);
        description = itemView.findViewById(R.id.description);
        itemView.setOnClickListener(this);
    }

    public ItemClickListener getItemClickListener() {
        return mItemClickListener;
    }

    public void setItemClickListener(final ItemClickListener itemClickListener) {
        mItemClickListener = itemClickListener;
    }

    @Override
    public void onClick(final View v) {
        mItemClickListener.onClick(v, getAdapterPosition(), false);
    }
}
