package ru.zaimme.zaim.ViewHolder;

import android.support.v7.widget.RecyclerView;
import android.view.View;
import android.widget.TextView;
import ru.zaimme.zaim.Interface.ItemClickListener;
import ru.zaimme.zaim.R;

@SuppressWarnings("unused")
public class ListViewHolder extends RecyclerView.ViewHolder implements View.OnClickListener {

    public TextView txt_title, txt_detail;

    private ItemClickListener mItemClickListener;

    public ListViewHolder(final View itemView) {
        super(itemView);
        txt_detail = itemView.findViewById(R.id.txt_detail);
        txt_title = itemView.findViewById(R.id.txt_title);
        itemView.setOnClickListener(this);
    }

    public ItemClickListener getItemClickListener() {
        return mItemClickListener;
    }

    public void setItemClickListener(final ItemClickListener itemClickListener) {
        mItemClickListener = itemClickListener;
    }

    @Override
    public void onClick(final View v) {
        mItemClickListener.onClick(v, getAdapterPosition(), false);
    }
}
