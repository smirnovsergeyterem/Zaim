package ru.zaimme.zaim;


import android.content.Intent;
import android.graphics.Color;
import android.os.Bundle;
import android.support.annotation.NonNull;
import android.support.v7.app.AppCompatActivity;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.support.v7.widget.Toolbar;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.MenuItem;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Toast;
import com.bumptech.glide.Glide;
import com.bumptech.glide.request.RequestOptions;
import com.firebase.ui.database.FirebaseRecyclerAdapter;
import com.firebase.ui.database.FirebaseRecyclerOptions;
import com.google.firebase.database.DataSnapshot;
import com.google.firebase.database.DatabaseError;
import com.google.firebase.database.DatabaseReference;
import com.google.firebase.database.FirebaseDatabase;
import com.google.firebase.database.Query;
import com.google.firebase.database.ValueEventListener;
import com.mikepenz.fontawesome_typeface_library.FontAwesome.Icon;
import com.mikepenz.iconics.IconicsDrawable;
import com.synnapps.carouselview.CarouselView;
import com.synnapps.carouselview.ImageListener;
import java.util.ArrayList;
import java.util.Objects;
import ru.zaimme.zaim.Model.RkoDetailModel;
import ru.zaimme.zaim.Model.RkoModel;
import ru.zaimme.zaim.ViewHolder.RkoViewHolder;

public class Rko extends AppCompatActivity {

    private static final String TAG = "Rko";

    private CarouselView carouselView;

    private final ArrayList<String> mAdsImagesList = new ArrayList<>();

    private DatabaseReference mAdsRef;

    private ImageListener mImageListener;

    private RecyclerView mRecyclerView;

    private DatabaseReference mRko;

    private FirebaseRecyclerAdapter<RkoModel, RkoViewHolder> mRkoAdapter;

    private DatabaseReference mRkoDetail;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_rko);

        // Handle Toolbar
        Toolbar toolbar = findViewById(R.id.toolbar);
        setSupportActionBar(toolbar);
        //set the back arrow in the toolbar
        Objects.requireNonNull(getSupportActionBar()).setDisplayHomeAsUpEnabled(true);
        getSupportActionBar().setTitle("РКО");
        getSupportActionBar().setHomeAsUpIndicator(
                new IconicsDrawable(this, Icon.faw_arrow_left)
                        .color(Color.WHITE)
                        .sizeDp(IconicsDrawable.TOOLBAR_ICON_SIZE)
                        .paddingDp(IconicsDrawable.TOOLBAR_ICON_PADDING)
        );

//        result = new DrawerBuilder()
//                .withActivity(this)
//                .withToolbar(toolbar)
//                .withActionBarDrawerToggle(false)
//                .withTranslucentStatusBar(false)
//                .withSavedInstance(savedInstanceState)
//                .buildView();

        initFirebase();
        initCarousel();
        initRecyclerView();
        loadRkoList();
    }

    @Override
    protected void onResume() {
        super.onResume();
        if (mRkoAdapter != null) {
            mRkoAdapter.startListening();
        }
    }

    @Override
    protected void onPostResume() {
        super.onPostResume();
        carouselView.playCarousel();
    }

    @Override
    protected void onPause() {
        carouselView.pauseCarousel();
        super.onPause();
    }

    @Override
    protected void onStop() {
        super.onStop();
        if (mRkoAdapter != null) {
            mRkoAdapter.stopListening();
        }
    }

    @Override
    protected void onDestroy() {
        super.onDestroy();
        Glide.get(this).clearMemory();
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        switch (item.getItemId()) {
            case android.R.id.home:
                onBackPressed();
                return true;

            default:
                return super.onOptionsItemSelected(item);
        }
    }

    private void getAdsImages() {
        mAdsRef.addListenerForSingleValueEvent(new ValueEventListener() {
            @Override
            public void onCancelled(@NonNull final DatabaseError databaseError) {
                Log.e(TAG, "onCancelled: ", databaseError.toException());
            }

            @Override
            public void onDataChange(@NonNull final DataSnapshot dataSnapshot) {
                for (final DataSnapshot item : dataSnapshot.getChildren()) {
                    mAdsImagesList.add(Objects.requireNonNull(item.getValue()).toString());
                }
                carouselView.setPageCount(mAdsImagesList.size());
                mImageListener = (position, imageView) -> Glide.with(getBaseContext())
                        .load(mAdsImagesList.toArray()[position])
                        .into(imageView);
                carouselView.setImageListener(mImageListener);
            }
        });
    }

    private void initCarousel() {
        //Carousel
        carouselView = findViewById(R.id.carousel);
        mImageListener = (position, imageView) -> Glide.with(getBaseContext())
                .load(mAdsImagesList.toArray()[position])
                .into(imageView);
        carouselView.setImageListener(mImageListener);
        carouselView.setImageClickListener(
                position -> Toast.makeText(this, "Выбран элемент: " + position, Toast.LENGTH_SHORT).show());
        getAdsImages();
    }

    private void initFirebase() {
        final FirebaseDatabase database = FirebaseDatabase.getInstance();
        mAdsRef = database.getReference("Ads");
        mRko = database.getReference("Rko");
        mRkoDetail = database.getReference("RkoDetail");
    }

    private void initRecyclerView() {
        //Recycler
        mRecyclerView = findViewById(R.id.rkoList);
        mRecyclerView.setHasFixedSize(true);
        RecyclerView.LayoutManager layoutManager = new LinearLayoutManager(this);
        mRecyclerView.setLayoutManager(layoutManager);
    }

    private void initValues() {
        ArrayList<RkoDetailModel> detailList = new ArrayList<>();
        detailList.add(new RkoDetailModel(
                "«Эксперт Банк»",
                "Выгодные условия для бизнеса.",
                "Открытие расчетного счета без ограничений. Кредиты до 5 000 000 рублей на развитие бизнеса и до 50 000 000 рублей для владельцев бизнеса.",
                "https://zaimme.ru/images/banks_logo/expert-bank-rko-logo.png",
                "- Свидетельство о государственной регистрации юридического лица.\n"
                        + "- Свидетельство о постановке юридического лица на учет в налоговом органе.\n"
                        + "- Учредительные документы юридического лица.\n"
                        + "- Решение/Протокол о создании юридического лица.\n"
                        + "- Выписка из Единого государственного реестра юридических лиц (ЕГРЮЛ).\n"
                        + "- Документ об избрании единоличного исполнительного органа юридического лица.",
                "- Удобные и индивидуальные условия по депозитам.\n"
                        + "- Кредиты для развития бизнеса до 5 000 000 рублей.\n"
                        + "- Кредиты для владельцев бизнеса до 50 000 000 рублей на любые цели на срок до 5 лет.\n"
                        + "- Банковские гарантии по упрощенной системе без посредников.\n"
                        + "- Масштабирование бизнеса с экономией на нологах.\n"
                        + "- Открытие расчетного счета без ограничений.\n"
                        + "- Выгодный зарплатный проект (индексация ЗП сотрудников деньгами банка).\n"
                        + "- Дистанционное обслуживание.",
                "https://zaimme.ru/banks/rko/expert_bank.php"
        ));
        detailList.add(new RkoDetailModel(
                "Банк «Открытие»",
                "Расчетный счет c бесплатным обслуживанием.",
                "При открытии счета Вы получите 3 месяца бесплатного обслуживания + 2000 руб. на первую рекламную кампанию в Google AdWords.",
                "https://zaimme.ru/images/banks_logo/otkritiye-bank-rko-logo.png",
                "- Бесплатное открытие счета в банке или у вас дома.\n"
                        + "- 3 мес. бесплатного обслуживания.\n"
                        + "- Бесплатное заверение документов для открытия счета.\n"
                        + "- 0 рублей за подключение к интернет-банку.\n"
                        + "- Вы ничего не платите за выпуск корпоративной карты для оплаты товаров и услуг.",
                "- 2000 руб. на первую рекламную кампанию в Google AdWords.\n"
                        + "- Персональный менеджер в банке.\n"
                        + "- Ваши товары и услуги увидят все клиенты и партнёры банка.\n"
                        + "- Бесплатная интернет-бухгалтерия от партнеров банка \"Моё Дело\" на 3 месяца.\n"
                        + "- 3 месяца бесплатного доступа к тендерам по всей России.\n"
                        + "- Дополнительный доход от банка до 8%.\n"
                        + "- Индивидуальные тарифы и овердрафт для избежания кассовых разрывов.\n"
                        + "- 3 месяца бесплатного доступа к тендерам по всей России.",
                "https://zaimme.ru/banks/rko/open-bank.php"
        ));
        detailList.add(new RkoDetailModel(
                "«Альфа-Банк»",
                "Расчетный счет за 2-3 дня с доставкой в офис.",
                "При открытии счета Вы получите промо-код на сумму 9000 руб. на первую рекламную кампанию в Яндекс.Директ. Открыть счет просто и удобно.",
                "https://zaimme.ru/images/banks_logo/alfa-bank-rko-logo.png",
                "0 руб. - Открытие счета и подключение интернет-банка.\n"
                        + "0 руб. - Интернет-банк и мобильный банк для управления счетом.\n"
                        + "0 руб. - Выпуск бизнес-карты для внесения и снятия наличных в любом банкомате.\n"
                        + "0 руб. - Первое внесение наличных на счет.\n"
                        + "0 руб. - Налоговые и бюджетные платежи, переводы юрлицам и ИП в Альфа-Банке.\n"
                        + "0 руб. - Обслуживание счета если нет оборотов.",
                "- 9 000 руб. на рекламу в Яндексе.\n"
                        + "- до 50 000 руб. на продвижение в социальных сетях.\n"
                        + "- Доступ ко всем рабочим инструментам «Битрикс24» на полгода бесплатно.\n"
                        + "- 15 месяцев онлайн-бухгалтерии Эльба для новых ИП бесплатно.\n"
                        + "- Сертификат на подбор персонала в HeadHunter.",
                "https://zaimme.ru/banks/rko/alfa_bank.php"
        ));
        for (RkoDetailModel item : detailList) {
            String id = mRkoDetail.push().getKey();
            mRkoDetail.child(Objects.requireNonNull(id)).setValue(item).addOnCompleteListener(task -> {
                if (task.isSuccessful()) {
                    Log.d(TAG, "onComplete: mRkoDetail");
                    RkoModel rkoModel = new RkoModel();
                    rkoModel.setDescription(item.getDescription());
                    rkoModel.setImage(item.getImage());
                    rkoModel.setSubtitle(item.getSubtitle());
                    rkoModel.setTitle(item.getTitle());

                    mRko.child(id).setValue(rkoModel).addOnCompleteListener(task1 -> {
                        if (task1.isSuccessful()) {
                            Log.d(TAG, "onComplete: mRko");
                        } else {
                            Log.d(TAG, "onComplete: failed mRko");
                        }
                    });
                } else {
                    Log.d(TAG, "onComplete: failed mRkoDetail");
                }
            });
        }
    }

    private void loadRkoList() {
        Query query = mRko.orderByKey();
        FirebaseRecyclerOptions<RkoModel> listOptions =
                new FirebaseRecyclerOptions.Builder<RkoModel>()
                        .setQuery(query, RkoModel.class)
                        .build();
        mRkoAdapter = new FirebaseRecyclerAdapter<RkoModel, RkoViewHolder>(listOptions) {
            @NonNull
            @Override
            public RkoViewHolder onCreateViewHolder(@NonNull final ViewGroup viewGroup, final int i) {
                View itemView = LayoutInflater.from(viewGroup.getContext())
                        .inflate(R.layout.rko_item, viewGroup, false);
                return new RkoViewHolder(itemView);
            }

            @Override
            protected void onBindViewHolder(@NonNull final RkoViewHolder holder, final int position,
                    @NonNull final RkoModel model) {
                holder.title.setText(model.getTitle());
                holder.subtitle.setText(model.getSubtitle());
                holder.description.setText(model.getDescription());

                Glide.with(getBaseContext())
                        .load(model.getImage())
                        .apply(new RequestOptions().fitCenter())
                        .into(holder.logo);
                holder.setItemClickListener((view, position1, isLongClick) -> {
                    Intent rkoDetailIntent = new Intent(Rko.this, RkoDetail.class);
                    rkoDetailIntent.putExtra("rkoId", mRkoAdapter.getRef(position1).getKey());
                    startActivity(rkoDetailIntent);
                });
            }
        };
        mRkoAdapter.startListening();
        mRecyclerView.setAdapter(mRkoAdapter);

    }
}
