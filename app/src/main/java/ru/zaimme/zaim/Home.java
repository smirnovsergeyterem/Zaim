package ru.zaimme.zaim;

import android.content.Intent;
import android.graphics.Color;
import android.os.Build;
import android.os.Bundle;
import android.support.annotation.NonNull;
import android.support.v7.app.AlertDialog;
import android.support.v7.app.AlertDialog.Builder;
import android.support.v7.app.AppCompatActivity;
import android.support.v7.view.ActionMode;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.support.v7.widget.Toolbar;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.Menu;
import android.view.MenuInflater;
import android.view.MenuItem;
import android.view.View;
import android.view.ViewGroup;
import android.widget.AdapterView;
import android.widget.AdapterView.OnItemSelectedListener;
import android.widget.ArrayAdapter;
import android.widget.Spinner;
import android.widget.Toast;
import com.bumptech.glide.Glide;
import com.firebase.ui.database.FirebaseRecyclerAdapter;
import com.firebase.ui.database.FirebaseRecyclerOptions;
import com.google.firebase.auth.FirebaseAuth;
import com.google.firebase.database.DataSnapshot;
import com.google.firebase.database.DatabaseError;
import com.google.firebase.database.DatabaseReference;
import com.google.firebase.database.FirebaseDatabase;
import com.google.firebase.database.Query;
import com.google.firebase.database.ValueEventListener;
import com.mikepenz.materialdrawer.Drawer;
import com.mikepenz.materialize.util.UIUtils;
import com.synnapps.carouselview.CarouselView;
import com.synnapps.carouselview.ImageListener;
import io.paperdb.Paper;
import java.util.ArrayList;
import java.util.Objects;
import ru.zaimme.zaim.Common.Common;
import ru.zaimme.zaim.Model.City;
import ru.zaimme.zaim.Model.List;
import ru.zaimme.zaim.Model.Region;
import ru.zaimme.zaim.ViewHolder.ListViewHolder;

public class Home extends AppCompatActivity {

    private class ActionBarCallBack implements ActionMode.Callback {

        @Override
        public boolean onActionItemClicked(ActionMode mode, MenuItem item) {
            return false;
        }

        @Override
        public boolean onCreateActionMode(ActionMode mode, Menu menu) {
            if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.LOLLIPOP) {
                getWindow().setStatusBarColor(UIUtils.getThemeColorFromAttrOrRes(Home.this,
                        R.attr.colorPrimaryDark,
                        R.color.material_drawer_primary_dark));
            }

            mode.getMenuInflater().inflate(R.menu.cab, menu);
            return true;
        }

        @Override
        public void onDestroyActionMode(ActionMode mode) {
            if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.LOLLIPOP) {
                getWindow().setStatusBarColor(Color.TRANSPARENT);
            }
        }

        @Override
        public boolean onPrepareActionMode(ActionMode mode, Menu menu) {
            return false;
        }
    }

    private static final String TAG = "Home";

    ArrayAdapter<City> cityAdapter;

    ArrayAdapter<Region> regionAdapter;

    private CarouselView carouselView;

    private ArrayList<City> cityList = new ArrayList<>();

    private ArrayList<String> mAdsImagesList = new ArrayList<>();

    private DatabaseReference mAdsRef;

    private ImageListener mImageListener;

    private RecyclerView.LayoutManager mLayoutManager;

    private FirebaseRecyclerAdapter<List, ListViewHolder> mListAdapter;

    private RecyclerView mRecyclerView;

    private DatabaseReference mRef;

    private DatabaseReference mRegionsRef;

    private DatabaseReference mUserInfo;

    private ArrayList<Region> regionList = new ArrayList<>();

    private Drawer resultDrawer;

    private String selectedCity, selectedRegion;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_home);
        Paper.init(this);

        Toolbar toolbar = findViewById(R.id.toolbar);
        setSupportActionBar(toolbar);
        Objects.requireNonNull(getSupportActionBar()).setTitle("Zaimme.ru");
        resultDrawer = DrawerUtil.getDrawer(this, toolbar);

        //Firebase
        final FirebaseDatabase database = FirebaseDatabase.getInstance();
        mRef = database.getReference("List");
        mAdsRef = database.getReference("Ads");
        mRegionsRef = database.getReference("Region");
        mUserInfo = database.getReference("UserInfo");

        //Carousel
        carouselView = findViewById(R.id.carousel);
        mImageListener = (position, imageView) -> Glide.with(getBaseContext())
                .load(mAdsImagesList.toArray()[position])
                .into(imageView);
        carouselView.setImageListener(mImageListener);
        carouselView.setImageClickListener(
                position -> Toast.makeText(Home.this, "Выбран элемент: " + position, Toast.LENGTH_SHORT).show());

        //Recycler
        mRecyclerView = findViewById(R.id.list);
        mRecyclerView.setHasFixedSize(true);
        mLayoutManager = new LinearLayoutManager(this);
        mRecyclerView.setLayoutManager(mLayoutManager);

        LoadOfferList();
        loadAdsImages();
    }

    @Override
    protected void onResume() {
        super.onResume();
        if (mListAdapter != null) {
            mListAdapter.startListening();
        }
    }

    @Override
    protected void onPostResume() {
        super.onPostResume();
        carouselView.playCarousel();
    }

    @Override
    protected void onPause() {
        carouselView.pauseCarousel();
        super.onPause();
    }

    @Override
    protected void onStop() {
        super.onStop();
        if (mListAdapter != null) {
            mListAdapter.stopListening();
        }
    }

    @Override
    public void onBackPressed() {
        //handle the back press :D close the drawer first and if the drawer is closed close the activity
        if (resultDrawer != null && resultDrawer.isDrawerOpen()) {
            resultDrawer.closeDrawer();
        } else {
            super.onBackPressed();
        }
    }

    @Override
    public boolean onCreateOptionsMenu(Menu menu) {
        MenuInflater inflater = getMenuInflater();
        inflater.inflate(R.menu.cab, menu);
        return true;
    }

    @Override
    public boolean onOptionsItemSelected(final MenuItem item) {
        switch (item.getItemId()) {
            case R.id.item_location:
                showAlertDialog();
                return true;

            default:
                return super.onOptionsItemSelected(item);
        }
    }

    private void LoadOfferList() {
        Query queryList = mRef.orderByKey();
        FirebaseRecyclerOptions<List> listOptions = new FirebaseRecyclerOptions.Builder<List>()
                .setQuery(queryList, List.class)
                .build();

        mListAdapter = new FirebaseRecyclerAdapter<List, ListViewHolder>(listOptions) {
            @NonNull
            @Override
            public ListViewHolder onCreateViewHolder(@NonNull final ViewGroup parent, final int viewType) {
                View itemView = LayoutInflater.from(parent.getContext())
                        .inflate(R.layout.list_item, parent, false);
                return new ListViewHolder(itemView);
            }

            @Override
            protected void onBindViewHolder(@NonNull final ListViewHolder viewHolder, final int position,
                    @NonNull final List model) {
                viewHolder.txt_title.setText(model.getTitle());
                viewHolder.txt_detail.setText(model.getDetail());

                viewHolder.setItemClickListener(
                        (View view, int position1, boolean isLongClick) -> {
                            boolean goDefault = false;
                            switch (position1) {
                                case 0: {
                                    Intent homeIntent = new Intent(Home.this, Loans.class);
                                    startActivity(homeIntent);
                                    break;
                                }
                                case 1: {
                                    Intent intent = new Intent(Home.this, Credit.class);
                                    startActivity(intent);
                                    break;
                                }
                                case 2: {
                                    Intent intent = new Intent(Home.this, Cards.class);
                                    startActivity(intent);
                                    break;
                                }
                                case 3: {
                                    Intent intent = new Intent(Home.this, CreditHistory.class);
                                    startActivity(intent);
                                    break;
                                }
                                case 5: {
                                    Intent intent = new Intent(Home.this, Rko.class);
                                    startActivity(intent);
                                    break;
                                }
                                case 6: {
                                    Intent intent = new Intent(Home.this, Insurance.class);
                                    startActivity(intent);
                                    break;
                                }
                                default: {
                                    goDefault = true;
                                    break;
                                }
                            }
                            if (goDefault) {
                                Toast.makeText(Home.this, "Не реализовано!!!", Toast.LENGTH_SHORT).show();
                            }
                        });
            }
        };

        mListAdapter.startListening();
        mRecyclerView.setAdapter(mListAdapter);
    }

    private void loadAdsImages() {
        mAdsRef.addListenerForSingleValueEvent(new ValueEventListener() {
            @Override
            public void onCancelled(@NonNull final DatabaseError databaseError) {
                Log.e(TAG, "onCancelled: ", databaseError.toException());
            }

            @Override
            public void onDataChange(@NonNull final DataSnapshot dataSnapshot) {
                for (final DataSnapshot item : dataSnapshot.getChildren()) {
                    mAdsImagesList.add(Objects.requireNonNull(item.getValue()).toString());
                }
                carouselView.setPageCount(mAdsImagesList.size());
                mImageListener = (position, imageView) -> Glide.with(getBaseContext())
                        .load(mAdsImagesList.toArray()[position])
                        .into(imageView);
                carouselView.setImageListener(mImageListener);
            }
        });
    }

    private void showAlertDialog() {
        AlertDialog.Builder alert = new Builder(Home.this);
        alert.setTitle("Выбор города");
        alert.setMessage("Выберите ваш адресс: ");

        LayoutInflater inflater = this.getLayoutInflater();
        View viewAlert = inflater.inflate(R.layout.location_layout, null);

        Spinner spr_region = viewAlert.findViewById(R.id.spr_region);
        Spinner spr_city = viewAlert.findViewById(R.id.spr_city);

        regionAdapter = new ArrayAdapter<>(getApplicationContext(), R.layout.spinner_item, regionList);
        regionAdapter.setDropDownViewResource(R.layout.spinner_dropdown_item);

        mRegionsRef.orderByKey().addListenerForSingleValueEvent(new ValueEventListener() {
            @Override
            public void onCancelled(@NonNull final DatabaseError databaseError) {

            }

            @Override
            public void onDataChange(@NonNull final DataSnapshot dataSnapshot) {
                for (DataSnapshot item : dataSnapshot.getChildren()) {
                    regionList.add(item.getValue(Region.class));
                }
                spr_region.setAdapter(regionAdapter);
            }
        });
        spr_region.setOnItemSelectedListener(new OnItemSelectedListener() {
            @Override
            public void onItemSelected(final AdapterView<?> parent, final View view, final int position,
                    final long id) {
                selectedRegion = regionList.get(position).getName();
                cityList = regionList.get(position).getCities();
                cityAdapter = new ArrayAdapter<>(getApplicationContext(), R.layout.spinner_item,
                        cityList);
                cityAdapter.setDropDownViewResource(R.layout.spinner_dropdown_item);
                spr_city.setAdapter(cityAdapter);
                cityAdapter.notifyDataSetChanged();
            }

            @Override
            public void onNothingSelected(final AdapterView<?> parent) {

            }
        });

        spr_city.setOnItemSelectedListener(new OnItemSelectedListener() {
            @Override
            public void onItemSelected(final AdapterView<?> parent, final View view, final int position,
                    final long id) {
                selectedCity = cityList.get(position).getCityName();
            }

            @Override
            public void onNothingSelected(final AdapterView<?> parent) {

            }
        });

        alert.setView(viewAlert);
        alert.setIcon(R.drawable.ic_location);

        alert.setPositiveButton("ДА", (dialog, which) -> {
            if (!selectedCity.isEmpty() && !selectedRegion.isEmpty()) {
                Common.user.setRegion(selectedRegion);
                Common.user.setCity(selectedCity);
                mUserInfo.child(Objects.requireNonNull(FirebaseAuth.getInstance().getCurrentUser()).getUid()).setValue(Common.user);
                Toast.makeText(this, "Локация сохранена", Toast.LENGTH_SHORT).show();
            }
            dialog.dismiss();
        });

        alert.setNegativeButton("НЕТ", (dialog, which) -> dialog.dismiss());
        alert.show();
    }
}
