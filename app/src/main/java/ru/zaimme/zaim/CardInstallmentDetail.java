package ru.zaimme.zaim;


import android.content.Intent;
import android.graphics.Color;
import android.net.Uri;
import android.os.Bundle;
import android.support.annotation.NonNull;
import android.support.v7.app.AppCompatActivity;
import android.support.v7.widget.Toolbar;
import android.util.Log;
import android.view.MenuItem;
import android.widget.Button;
import android.widget.ImageView;
import android.widget.TextView;
import android.widget.Toast;
import com.bumptech.glide.Glide;
import com.bumptech.glide.request.RequestOptions;
import com.google.firebase.database.DataSnapshot;
import com.google.firebase.database.DatabaseError;
import com.google.firebase.database.DatabaseReference;
import com.google.firebase.database.FirebaseDatabase;
import com.google.firebase.database.ValueEventListener;
import com.mikepenz.fontawesome_typeface_library.FontAwesome.Icon;
import com.mikepenz.iconics.IconicsDrawable;
import java.util.Objects;
import ru.zaimme.zaim.Model.Installment.InstallmentCardDetailItem;

public class CardInstallmentDetail extends AppCompatActivity {

    private static final String TAG = "CardInstallmentDetail";

    private Button btn_request;

    private ImageView image;

    private String installmentCardId;

    private DatabaseReference mInstallmentCardDetail;

    private TextView title, subtitle, installment, additionalInfo, license, sum, decision_time, percent, documents;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_card_installment_detail);

        // Handle Toolbar
        Toolbar toolbar = findViewById(R.id.toolbar);
        setSupportActionBar(toolbar);
        //set the back arrow in the toolbar
        Objects.requireNonNull(getSupportActionBar()).setDisplayHomeAsUpEnabled(true);
        getSupportActionBar().setTitle("");
        getSupportActionBar().setHomeAsUpIndicator(
                new IconicsDrawable(this, Icon.faw_arrow_left)
                        .color(Color.WHITE)
                        .sizeDp(IconicsDrawable.TOOLBAR_ICON_SIZE)
                        .paddingDp(IconicsDrawable.TOOLBAR_ICON_PADDING)
        );

        initFirebase();
        initUI();
        initListeners();
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        switch (item.getItemId()) {
            case android.R.id.home:
                onBackPressed();
                return true;

            default:
                return super.onOptionsItemSelected(item);
        }
    }

    public void openWebPage(String url) {
        Uri webpage = Uri.parse(url);
        Intent intent = new Intent(Intent.ACTION_VIEW, webpage);
        if (intent.resolveActivity(getPackageManager()) != null) {
            startActivity(intent);
        }
    }

    private void initFirebase() {
        //Firebase Init
        final FirebaseDatabase database = FirebaseDatabase.getInstance();
        mInstallmentCardDetail = database.getReference("CardDetail");
    }

    private void initListeners() {
        //Firebase Listener
        mInstallmentCardDetail.orderByKey().equalTo(installmentCardId)
                .addListenerForSingleValueEvent(new ValueEventListener() {
                    @Override
                    public void onCancelled(@NonNull final DatabaseError databaseError) {
                        Toast.makeText(CardInstallmentDetail.this, databaseError.getMessage(), Toast.LENGTH_SHORT)
                                .show();
                    }

                    @Override
                    public void onDataChange(@NonNull final DataSnapshot dataSnapshot) {
                        try {
                            InstallmentCardDetailItem installmentCardDetailItem = new InstallmentCardDetailItem();
                            if (dataSnapshot != null) {
                                for (DataSnapshot item : dataSnapshot.getChildren()) {
                                    installmentCardDetailItem = item.getValue(InstallmentCardDetailItem.class);
                                    //Заголовок
                                    Glide.with(getBaseContext())
                                            .load(installmentCardDetailItem.getImage())
                                            .apply(new RequestOptions().fitCenter())
                                            .into(image);
                                    getSupportActionBar().setTitle(installmentCardDetailItem.getTitle());
                                    title.setText(installmentCardDetailItem.getTitle());
                                    subtitle.setText(installmentCardDetailItem.getSubTitle());
                                    installment.setText(installmentCardDetailItem.getInstallment());
                                    sum.setText(installmentCardDetailItem.getSum());
                                    decision_time.setText(installmentCardDetailItem.getDecisionTime());
                                    percent.setText(installmentCardDetailItem.getPercent());
                                    license.setText(String.format("Лицензия банка №%s",
                                            installmentCardDetailItem.getLicense()));
                                    additionalInfo.setText(installmentCardDetailItem.getAdditionalInfo());
                                    documents.setText(installmentCardDetailItem.getDocuments());

                                    final InstallmentCardDetailItem finalInstallmentCardDetailItem
                                            = installmentCardDetailItem;
                                    btn_request.setOnClickListener(
                                            v -> openWebPage(finalInstallmentCardDetailItem.getUrl()));


                                }
                            }
                        } catch (Exception e) {
                            String msg = "Ошибка при загрузке карты рассрочки с ID=" + installmentCardId;
                            Log.e(TAG, msg, e);
                            Toast.makeText(CardInstallmentDetail.this, msg, Toast.LENGTH_SHORT).show();
                        }
                    }
                });
    }

    private void initUI() {
        title = findViewById(R.id.title);
        subtitle = findViewById(R.id.subtitle);
        installment = findViewById(R.id.installment);
        additionalInfo = findViewById(R.id.additionalInfo);
        documents = findViewById(R.id.documents);
        sum = findViewById(R.id.sum);
        decision_time = findViewById(R.id.decision_time);
        percent = findViewById(R.id.percent);
        btn_request = findViewById(R.id.btn_request);
        image = findViewById(R.id.image);
        license = findViewById(R.id.license);

        //Получение переданного debtCardId
        if (getIntent() != null) {
            installmentCardId = getIntent().getStringExtra("installmentCardId");
        }
    }
}
