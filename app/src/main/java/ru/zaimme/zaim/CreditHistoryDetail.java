package ru.zaimme.zaim;


import android.content.Intent;
import android.graphics.Color;
import android.net.Uri;
import android.os.Bundle;
import android.support.v7.app.AppCompatActivity;
import android.support.v7.widget.Toolbar;
import android.view.MenuItem;
import android.widget.Button;
import android.widget.ImageView;
import android.widget.TextView;
import com.bumptech.glide.Glide;
import com.mikepenz.fontawesome_typeface_library.FontAwesome.Icon;
import com.mikepenz.iconics.IconicsDrawable;
import java.util.Objects;
import ru.zaimme.zaim.Model.CreditHistoryModel;

public class CreditHistoryDetail extends AppCompatActivity {

    private static final String TAG = "CreditHistoryDetail";

    private CreditHistoryModel model;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_credit_history_detail);

        // Handle Toolbar
        Toolbar toolbar = findViewById(R.id.toolbar);
        setSupportActionBar(toolbar);
        //set the back arrow in the toolbar
        Objects.requireNonNull(getSupportActionBar()).setDisplayHomeAsUpEnabled(true);
        getSupportActionBar().setTitle("");
        getSupportActionBar().setHomeAsUpIndicator(
                new IconicsDrawable(this, Icon.faw_arrow_left)
                        .color(Color.WHITE)
                        .sizeDp(IconicsDrawable.TOOLBAR_ICON_SIZE)
                        .paddingDp(IconicsDrawable.TOOLBAR_ICON_PADDING)
        );

        initUI();
    }

    @Override
    protected void onDestroy() {
        super.onDestroy();
        Glide.get(this).clearMemory();
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        switch (item.getItemId()) {
            case android.R.id.home:
                onBackPressed();
                return true;

            default:
                return super.onOptionsItemSelected(item);
        }
    }

    public void openWebPage(String url) {
        Uri webpage = Uri.parse(url);
        Intent intent = new Intent(Intent.ACTION_VIEW, webpage);
        if (intent.resolveActivity(getPackageManager()) != null) {
            startActivity(intent);
        }
    }

    private void initUI() {
        final TextView title = findViewById(R.id.title);
        final TextView paragraph_text1 = findViewById(R.id.paragraph_text1);
        final TextView paragraph_text2 = findViewById(R.id.paragraph_text2);
        final TextView paragraph_title1 = findViewById(R.id.paragraph_title1);
        final TextView paragraph_title2 = findViewById(R.id.paragraph_title2);
        final ImageView image = findViewById(R.id.image);
        final Button btn_request = findViewById(R.id.btn_request);
        if (getIntent() != null) {
            model = (CreditHistoryModel) getIntent().getSerializableExtra("creditHistoryModel");
            if (model != null) {
                title.setText(model.getTitle());
                getSupportActionBar().setTitle(model.getTitle());
                paragraph_text1.setText(model.getParagraph_text1());
                paragraph_text2.setText(model.getParagraph_text2());
                paragraph_title1.setText(model.getParagraph_title1());
                paragraph_title2.setText(model.getParagraph_title2());

                Glide.with(this).load(model.getImage()).into(image);

                btn_request.setOnClickListener(view -> openWebPage(model.getUrl()));
            }
        }
    }
}
