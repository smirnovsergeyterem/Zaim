package ru.zaimme.zaim;

import android.app.ProgressDialog;
import android.content.Context;
import android.os.Bundle;
import android.support.design.widget.TextInputLayout;
import android.support.v7.app.AppCompatActivity;
import android.text.TextUtils;
import android.util.Log;
import android.util.Pair;
import android.view.View;
import android.view.inputmethod.InputMethodManager;
import android.widget.Button;
import android.widget.Toast;

import com.google.firebase.auth.FirebaseAuth;
import com.google.firebase.auth.FirebaseAuthUserCollisionException;
import com.google.firebase.auth.FirebaseUser;
import com.google.firebase.auth.UserProfileChangeRequest;
import com.google.firebase.database.FirebaseDatabase;
import com.jakewharton.rxbinding2.widget.RxCompoundButton;
import com.jakewharton.rxbinding2.widget.RxTextView;
import com.rey.material.widget.CheckBox;

import java.util.Objects;
import java.util.concurrent.TimeUnit;
import java.util.regex.Matcher;
import java.util.regex.Pattern;

import io.paperdb.Paper;
import io.reactivex.Observable;
import io.reactivex.android.schedulers.AndroidSchedulers;
import io.reactivex.disposables.CompositeDisposable;
import io.reactivex.disposables.Disposable;
import ru.zaimme.zaim.Common.Common;

import static android.text.TextUtils.isEmpty;

public class SignUp extends AppCompatActivity {

    private static final String TAG = "SignUp";

    private Button btnSignUp;

    private CheckBox ckbAccept;

    private FirebaseAuth mAuth;

    private final CompositeDisposable mDisposables = new CompositeDisposable();

    private TextInputLayout tilEmail;

    private TextInputLayout tilName;

    private TextInputLayout tilPassword;


    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_sign_up);

        mAuth = FirebaseAuth.getInstance();

        //Init FireBase
        FirebaseDatabase database = FirebaseDatabase.getInstance();

        tilName = findViewById(R.id.tilName);
        tilEmail = findViewById(R.id.tilEmail);
        tilPassword = findViewById(R.id.tilPassword);
        btnSignUp = findViewById(R.id.btnSignUp);
        ckbAccept = findViewById(R.id.ckbAccept);

        //CombineLatest
        editTextObservables();

        btnSignUp.setOnClickListener(v -> signUpFirebaseAuth());
    }

    @Override
    protected void onStop() {
        super.onStop();
        if (!mDisposables.isDisposed()) {
            mDisposables.clear();
        }
    }

    private void editTextObservables() {
        Observable<Boolean> nameObservable =
                RxTextView.textChanges(Objects.requireNonNull(tilName.getEditText()))
                        .skip(1)
                        .debounce(300, TimeUnit.MILLISECONDS)
                        .observeOn(AndroidSchedulers.mainThread())
                        .map(string -> {
                            Pair<Boolean, CharSequence> result = isNameValid(string);
                            boolean showError = result.first;
                            CharSequence errorText = result.second;
                            tilName.setErrorEnabled(!showError);
                            tilName.setError(errorText);
                            return showError;
                        });
        Observable<Boolean> emailObservable =
                RxTextView.textChanges(Objects.requireNonNull(tilEmail.getEditText()))
                        .skip(1)
                        .debounce(300, TimeUnit.MILLISECONDS)
                        .observeOn(AndroidSchedulers.mainThread())
                        .map(string -> {
                            Pair<Boolean, CharSequence> result = isEmailValid(string);
                            boolean showError = result.first;
                            CharSequence errorText = result.second;
                            tilEmail.setErrorEnabled(!showError);
                            tilEmail.setError(errorText);
                            return showError;
                        });
        Observable<Boolean> passwordObservable =
                RxTextView.textChanges(Objects.requireNonNull(tilPassword.getEditText()))
                        .skip(1)
                        .debounce(300, TimeUnit.MILLISECONDS)
                        .observeOn(AndroidSchedulers.mainThread())
                        .map(string -> {
                            Pair<Boolean, CharSequence> result = isPasswordValid(string);
                            boolean showError = result.first;
                            CharSequence errorText = result.second;
                            tilPassword.setErrorEnabled(!showError);
                            tilPassword.setError(errorText);
                            return showError;
                        });
        Observable<Boolean> ckbObservable = RxCompoundButton.checkedChanges(ckbAccept)
                .debounce(300, TimeUnit.MILLISECONDS)
                .observeOn(AndroidSchedulers.mainThread()).map(aBoolean -> aBoolean);

        Disposable disposableSignUp = Observable.combineLatest(
                nameObservable,
                emailObservable,
                passwordObservable,
                ckbObservable,
                (name, email, pass, accepted) -> name && email && pass && accepted)
                .distinctUntilChanged()
                .subscribe(result -> btnSignUp.setEnabled(result));

        mDisposables.add(disposableSignUp);
    }

    private void hideKeyboard() {
        View view = getCurrentFocus();
        if (view != null) {
            ((InputMethodManager) Objects.requireNonNull(getSystemService(Context.INPUT_METHOD_SERVICE))).
                    hideSoftInputFromWindow(view.getWindowToken(), InputMethodManager.HIDE_NOT_ALWAYS);
        }
    }

    private Pair<Boolean, CharSequence> isEmailValid(CharSequence charSequence) {
        if (TextUtils.isEmpty(charSequence)) {
            return new Pair<>(false, "Пустое поле");
        }
        Pattern pattern = Pattern.compile(".+@.+\\.[a-z]+");
        Matcher matcher = pattern.matcher(charSequence.toString());
        if (matcher.matches()) {
            return new Pair<>(true, "");
        } else {
            return new Pair<>(false, "Неверный e-mail адрес");
        }
    }

    private Pair<Boolean, CharSequence> isNameValid(CharSequence charSequence) {
        if (isEmpty(charSequence)) {
            return new Pair<>(false, "Пусто поле");
        }
        if (charSequence.length() < 3) {
            return new Pair<>(false, "Длина меньше 3 симоволов");
        }
        if (charSequence.length() > 20) {
            return new Pair<>(false, "Длина превышает 20 симоволов");
        }
        Pattern pattern = Pattern.compile("[ёЁа-яА-ЯA-Za-z]{3,20}");
        Matcher matcher = pattern.matcher(charSequence.toString());
        if (matcher.matches()) {
            return new Pair<>(true, "");
        } else {
            return new Pair<>(false, "Поле содержит недопустимые символы");
        }
    }

    private Pair<Boolean, CharSequence> isPasswordValid(CharSequence charSequence) {
        if (isEmpty(charSequence)) {
            return new Pair<>(false, "Пустое поле");
        }
        if (charSequence.length() < 6) {
            return new Pair<>(false, "Пароль слишком короткий");
        }
        return new Pair<>(true, "");
    }
    @Override
    protected void onResume() {
        super.onResume();
        editTextObservables();
    }
    private void signUpFirebaseAuth() {
        hideKeyboard();

        final String email = Objects.requireNonNull(tilEmail.getEditText()).getText().toString();
        final String name = Objects.requireNonNull(tilName.getEditText()).getText().toString();
        final String password = Objects.requireNonNull(tilPassword.getEditText()).getText().toString();

        if (email.equals("")) {
            tilEmail.setError("Не заполнено поле");
        } else {
            tilEmail.setErrorEnabled(false);
        }
        if (name.equals("")) {
            tilName.setError("Не заполнено поле");
        } else {
            tilName.setErrorEnabled(false);
        }
        if (password.equals("")) {
            tilPassword.setError("Не заполнено поле");
        } else {
            tilPassword.setErrorEnabled(false);
        }
        if (email.equals("") || name.equals("") || password.equals("")) {
            return;
        }
        final ProgressDialog mDialog = new ProgressDialog(SignUp.this);
        mDialog.setMessage("Ожидайте...");
        mDialog.show();

        mAuth.createUserWithEmailAndPassword(email, password)
                .addOnCompleteListener(this, task -> {
                    mDialog.dismiss();
                    if (task.isSuccessful()) {
                        Log.d(TAG, "createUserWithEmail:success");
                        FirebaseUser user = mAuth.getCurrentUser();
                        updateUserInfo(user, name);

                        Paper.init(this);
                        Paper.book().write(Common.EMAIL_KEY, "");
                        Paper.book().write(Common.PASSWORD_KEY, "");

                        finish();
                    } else {
                        Log.w(TAG, "createUserWithEmail:failure", task.getException());
                        Toast.makeText(SignUp.this, "Неверный логин/пароль",
                                Toast.LENGTH_SHORT).show();
                    }
                })
                .addOnFailureListener(e -> {
                    if (e instanceof FirebaseAuthUserCollisionException) {
                        Toast.makeText(SignUp.this, "Адрес уже используется", Toast.LENGTH_SHORT).show();
                    }
                });
    }

    private void updateUserInfo(FirebaseUser user, final String name) {
        if (user != null) {
            UserProfileChangeRequest changeRequest = new UserProfileChangeRequest.Builder()
                    .setDisplayName(name)
                    .build();
            user.updateProfile(changeRequest)
                    .addOnCompleteListener(task -> {
                        if (task.isSuccessful()) {
                            Log.d(TAG, "User profile updated.");
                        }
                    });
        }
    }
}
