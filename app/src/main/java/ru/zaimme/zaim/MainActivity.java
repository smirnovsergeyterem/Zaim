package ru.zaimme.zaim;

import android.app.ProgressDialog;
import android.content.Context;
import android.content.Intent;
import android.os.Bundle;
import android.support.annotation.NonNull;
import android.support.v7.app.AppCompatActivity;
import android.util.Log;
import android.view.View;
import android.view.inputmethod.InputMethodManager;
import android.widget.Button;
import android.widget.Toast;

import com.google.firebase.auth.FirebaseAuth;
import com.google.firebase.auth.FirebaseUser;
import com.google.firebase.database.DataSnapshot;
import com.google.firebase.database.DatabaseError;
import com.google.firebase.database.DatabaseReference;
import com.google.firebase.database.FirebaseDatabase;
import com.google.firebase.database.ValueEventListener;
import com.google.firebase.iid.FirebaseInstanceId;

import java.util.Objects;

import io.paperdb.Paper;
import ru.zaimme.zaim.Common.Common;
import ru.zaimme.zaim.Model.UserInfo;

public class MainActivity extends AppCompatActivity {

    private static final String TAG = "MainActivity";

    private FirebaseAuth mAuth;

    private Button mButtonSignIn;

    private Button mButtonSignUp;

    private DatabaseReference mUserInfo;


    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);
        initUI();
        initFireBase();
        setListeners();
        String email = Paper.book().read(Common.EMAIL_KEY);
        String password = Paper.book().read(Common.PASSWORD_KEY);
        if (email != null && password != null) {
            if (!email.isEmpty() && !password.isEmpty()) {
                loginWithFirebase(email, password);
            }
        }
    }

    private void hideKeyboard() {
        View view = getCurrentFocus();
        if (view != null) {
            ((InputMethodManager) Objects.requireNonNull(getSystemService(Context.INPUT_METHOD_SERVICE))).
                    hideSoftInputFromWindow(view.getWindowToken(), InputMethodManager.HIDE_NOT_ALWAYS);
        }
    }

    private void initFireBase() {
        mAuth = FirebaseAuth.getInstance();
        FirebaseDatabase database = FirebaseDatabase.getInstance();
        mUserInfo = database.getReference("UserInfo");
    }

    private void initUI() {
        Paper.init(this);
        mButtonSignUp = findViewById(R.id.btnSignUp);
        mButtonSignIn = findViewById(R.id.btnSignIn);
    }

    private void loginWithFirebase(final String email, final String password) {
        hideKeyboard();
        final ProgressDialog mDialog = new ProgressDialog(MainActivity.this);
        mDialog.setMessage("Ожидайте...");
        mDialog.show();

        mAuth.signInWithEmailAndPassword(email, password).addOnCompleteListener(this,
                task -> {
                    if (task.isSuccessful()) {
                        Log.d(TAG, "signInWithEmail:success");
                        writeUserInfo(mAuth.getCurrentUser());
                        Intent homeIntent = new Intent(MainActivity.this, Home.class);
                        startActivity(homeIntent);
                        finish();
                    } else {
                        Log.w(TAG, "signInWithEmail:failure", task.getException());
                        Toast.makeText(MainActivity.this, "Ошибка аутентификации.",
                                Toast.LENGTH_SHORT).show();
                    }
                    mDialog.dismiss();
                });
    }

    private void setListeners() {
        mButtonSignUp.setOnClickListener(v -> {
            Intent intentSignUp = new Intent(MainActivity.this, SignUp.class);
            startActivity(intentSignUp);
        });
        mButtonSignIn.setOnClickListener(v -> {
            Intent intentSignIn = new Intent(MainActivity.this, SignIn.class);
            startActivity(intentSignIn);
        });
    }

    private void writeUserInfo(final FirebaseUser user) {
        if (user != null) {
            UserInfo userInfo = new UserInfo();
            String userId = user.getUid();
            mUserInfo.child(userId).addListenerForSingleValueEvent(new ValueEventListener() {
                @Override
                public void onDataChange(@NonNull DataSnapshot dataSnapshot) {
                    if (!dataSnapshot.exists()) {
                        // Создаем нового пользователя
                        mUserInfo.child(userId).setValue(userInfo).addOnCompleteListener(task -> {
                            if (task.isSuccessful()) {
                                Log.d(TAG, "Создан новый пользователь");
                                Common.user = userInfo;
                            } else {
                                Log.w(TAG, "Ошибка создания нового пользователя", task.getException());
                                Toast.makeText(MainActivity.this, "Ошибка создания нового пользователя",
                                        Toast.LENGTH_SHORT).show();
                            }
                        });
                    } else {
                        //Обновляем только токен
                        UserInfo oldUserInfo = dataSnapshot.getValue(UserInfo.class);
                        String newDeviceToken = FirebaseInstanceId.getInstance().getToken();
                        Objects.requireNonNull(oldUserInfo).setToken(newDeviceToken);
                        mUserInfo.child(userId).setValue(oldUserInfo).addOnCompleteListener(task -> {
                            if (task.isSuccessful()) {
                                Common.user = oldUserInfo;
                                Log.d(TAG, "Токен пользователя " + userId + " был обновлен на " + newDeviceToken);
                            } else {
                                Log.d(TAG, "Не удалось обнвоить токен пользователя " + userId);
                            }
                        });
                    }
                }

                @Override
                public void onCancelled(@NonNull DatabaseError databaseError) {
                    Log.e(TAG, "onCancelled: ", databaseError.toException());
                }
            });
        }
    }
}
