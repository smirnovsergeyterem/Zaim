package ru.zaimme.zaim;

import android.graphics.Color;
import android.os.Bundle;
import android.support.v7.app.AppCompatActivity;
import android.support.v7.widget.Toolbar;
import android.util.Log;
import android.view.MenuItem;
import android.widget.CheckBox;
import com.google.firebase.auth.FirebaseAuth;
import com.google.firebase.auth.FirebaseUser;
import com.google.firebase.database.DatabaseReference;
import com.google.firebase.database.FirebaseDatabase;
import com.mikepenz.fontawesome_typeface_library.FontAwesome.Icon;
import com.mikepenz.iconics.IconicsDrawable;
import java.util.Objects;
import ru.zaimme.zaim.Common.Common;

public class Settings extends AppCompatActivity {

    private static final String TAG = "Settings";

    private CheckBox ckb_notification_push,
            ckb_notification_theme_action,
            ckb_notification_theme_bank_news,
            ckb_notification_theme_mfo_news,
            ckb_notification_theme_promocodes,
            ckb_notification_email;

    private FirebaseUser mUser;

    private DatabaseReference mUserInfo;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_settings);

        // Handle Toolbar
        Toolbar toolbar = findViewById(R.id.toolbar);
        setSupportActionBar(toolbar);
        //set the back arrow in the toolbar
        Objects.requireNonNull(getSupportActionBar()).setDisplayHomeAsUpEnabled(true);
        getSupportActionBar().setTitle("Настройки");
        getSupportActionBar().setHomeAsUpIndicator(
                new IconicsDrawable(this, Icon.faw_arrow_left)
                        .color(Color.WHITE)
                        .sizeDp(IconicsDrawable.TOOLBAR_ICON_SIZE)
                        .paddingDp(IconicsDrawable.TOOLBAR_ICON_PADDING)
        );

        initFirebase();
        initUI();
        setUIState();
        setListeners();
    }
    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        switch (item.getItemId()) {
            case android.R.id.home:
                onBackPressed();
                return true;

            default:
                return super.onOptionsItemSelected(item);
        }
    }
    @Override
    protected void onDestroy() {
        super.onDestroy();
        Log.d(TAG, "onDestroy: ");
    }

    @Override
    public void finish() {
        Log.d(TAG, "finish: ");
        super.finish();
    }

    private void initFirebase() {
        final FirebaseDatabase database = FirebaseDatabase.getInstance();
        mUserInfo = database.getReference("UserInfo");
        mUser = FirebaseAuth.getInstance().getCurrentUser();
    }

    private void initUI() {
        ckb_notification_push = findViewById(R.id.ckb_notification_push);
        ckb_notification_theme_action = findViewById(R.id.ckb_notification_theme_action);
        ckb_notification_theme_bank_news = findViewById(R.id.ckb_notification_theme_bank_news);
        ckb_notification_theme_mfo_news = findViewById(R.id.ckb_notification_theme_mfo_news);
        ckb_notification_theme_promocodes = findViewById(R.id.ckb_notification_theme_promocodes);
        ckb_notification_email = findViewById(R.id.ckb_notification_email);
    }

    private void setListeners() {
        ckb_notification_push.setOnCheckedChangeListener(
                (compoundButton, b) -> {
                    Log.d(TAG, "setListeners: ckb_notification_push" + b);
                    Common.user.setNotification_push(b);
                    updateState();
                });
        ckb_notification_theme_action.setOnCheckedChangeListener(
                (compoundButton, b) -> {
                    Log.d(TAG, "setListeners: ckb_notification_theme_action" + b);
                    Common.user.setNotification_theme_action(b);
                    updateState();
                });
        ckb_notification_theme_bank_news.setOnCheckedChangeListener(
                (compoundButton, b) -> {
                    Log.d(TAG, "setListeners: ckb_notification_theme_bank_news" + b);
                    Common.user.setNotification_theme_bank_news(b);
                    updateState();
                });

        ckb_notification_theme_mfo_news.setOnCheckedChangeListener(
                (compoundButton, b) -> {
                    Log.d(TAG, "setListeners: ckb_notification_theme_mfo_news" + b);
                    Common.user.setNotification_theme_mfo_news(b);
                    updateState();
                });
        ckb_notification_theme_promocodes.setOnCheckedChangeListener(
                (compoundButton, b) -> {
                    Log.d(TAG, "setListeners: ckb_notification_theme_promocodes" + b);
                    Common.user.setNotification_theme_promocodes(b);
                    updateState();
                });
        ckb_notification_email.setOnCheckedChangeListener(
                (compoundButton, b) -> {
                    Log.d(TAG, "setListeners: ckb_notification_email" + b);
                    Common.user.setNotification_email(b);
                    updateState();
                });
    }

    private void setUIState() {
        ckb_notification_push.setChecked(Common.user.getNotification_push());
        ckb_notification_theme_action.setChecked(Common.user.getNotification_theme_action());
        ckb_notification_theme_bank_news.setChecked(Common.user.getNotification_theme_bank_news());
        ckb_notification_theme_mfo_news.setChecked(Common.user.getNotification_theme_mfo_news());
        ckb_notification_theme_promocodes.setChecked(Common.user.getNotification_theme_promocodes());
        ckb_notification_email.setChecked(Common.user.getNotification_email());
    }

    private void updateState() {
        mUserInfo.child(mUser.getUid())
                .setValue(Common.user, (databaseError, databaseReference) -> {
                    if (databaseError != null) {
                        Log.e(TAG, "Data could not be saved ", databaseError.toException());
                        Log.d(TAG, "Data could not be saved: " + databaseError.getMessage());
                    } else {
                        Log.d(TAG, "Data saved successfully.");
                    }

                });
    }
}
