package ru.zaimme.zaim;

import android.app.Activity;
import android.content.Intent;
import android.support.v7.widget.Toolbar;
import android.widget.ImageView.ScaleType;
import android.widget.Toast;
import com.google.firebase.auth.FirebaseAuth;
import com.mikepenz.fontawesome_typeface_library.FontAwesome.Icon;
import com.mikepenz.materialdrawer.AccountHeader;
import com.mikepenz.materialdrawer.AccountHeaderBuilder;
import com.mikepenz.materialdrawer.Drawer;
import com.mikepenz.materialdrawer.DrawerBuilder;
import com.mikepenz.materialdrawer.model.DividerDrawerItem;
import com.mikepenz.materialdrawer.model.PrimaryDrawerItem;
import com.mikepenz.materialdrawer.model.interfaces.Nameable;
import io.paperdb.Paper;
import ru.zaimme.zaim.Common.Common;

public class DrawerUtil {

    public static Drawer getDrawer(final Activity activity, Toolbar toolbar) {
        final AccountHeader headerResult = new AccountHeaderBuilder()
                .withActivity(activity)
                .withTranslucentStatusBar(true)
                .withHeaderBackground(R.drawable.logo)
                .withHeaderBackgroundScaleType(ScaleType.CENTER_INSIDE)
                .build();
        return new DrawerBuilder()
                .withToolbar(toolbar)
                .withActivity(activity)
                .withDisplayBelowStatusBar(true)
                .withTranslucentStatusBar(true)
                .withAccountHeader(headerResult) //set the AccountHeader we created earlier for the header
                .withSliderBackgroundColorRes(R.color.md_white_1000)
                .withActionBarDrawerToggle(true)
                .withActionBarDrawerToggleAnimated(true)
                .withCloseOnClick(true)
                .withSelectedItem(-1)
                .addDrawerItems(
                        new PrimaryDrawerItem().withName("Займы").withIcon(Icon.faw_hand_holding_usd)
                                .withIdentifier(10),
                        new PrimaryDrawerItem().withName("Кредиты").withIcon(Icon.faw_credit_card).withIdentifier(11),
                        new PrimaryDrawerItem().withName("Карты").withIcon(Icon.faw_cc_visa).withIdentifier(12),
                        new PrimaryDrawerItem().withName("Кредитная история").withIcon(Icon.faw_history)
                                .withIdentifier(13),
                        new PrimaryDrawerItem().withName("Вклады").withIcon(Icon.faw_download).withIdentifier(14),
                        new PrimaryDrawerItem().withName("РКО").withIcon(Icon.faw_dollar_sign).withIdentifier(15),
                        new PrimaryDrawerItem().withName("Страхование").withIcon(Icon.faw_university)
                                .withIdentifier(16),
                        new PrimaryDrawerItem().withName("Инвестиции").withIcon(Icon.faw_coins).withIdentifier(17),
                        new DividerDrawerItem(),
                        new PrimaryDrawerItem().withName("Настройки")
                                .withIcon(Icon.faw_cog).withIdentifier(1),
                        new PrimaryDrawerItem().withName("Поддержка")
                                .withIcon(Icon.faw_bullhorn).withIdentifier(2),
                        new PrimaryDrawerItem().withName("О приложении")
                                .withIcon(Icon.faw_question).withIdentifier(3),
                        new DividerDrawerItem(),
                        new PrimaryDrawerItem().withName("Выход")
                                .withIcon(Icon.faw_sign_out_alt).withIdentifier(4)
                )
                .withOnDrawerItemClickListener((view, position, drawerItem) -> {
                    if (drawerItem != null && drawerItem.getIdentifier() == 3) {
                        Intent aboutIntent = new Intent(activity, About.class);
                        view.getContext().startActivity(aboutIntent);
                    }
                    if (drawerItem != null && drawerItem.getIdentifier() == 2) {
                        Intent supportIntent = new Intent(activity, Support.class);
                        view.getContext().startActivity(supportIntent);
                    }
                    if (drawerItem != null && drawerItem.getIdentifier() == 1) {
                        Intent settingsIntent = new Intent(activity, Settings.class);
                        view.getContext().startActivity(settingsIntent);
                    }
                    if (drawerItem != null && drawerItem.getIdentifier() == 4) {
                        Paper.book().write(Common.EMAIL_KEY, "");
                        Paper.book().write(Common.PASSWORD_KEY, "");
                        FirebaseAuth.getInstance().signOut();
                        Intent signinIntent = new Intent(activity, MainActivity.class);
                        signinIntent.addFlags(Intent.FLAG_ACTIVITY_NEW_TASK | Intent.FLAG_ACTIVITY_CLEAR_TASK);
                        view.getContext().startActivity(signinIntent);
                    }
                    if (drawerItem != null && between(drawerItem.getIdentifier())) {
                        boolean goDefault = false;
                        switch ((int) drawerItem.getIdentifier()) {
                            case 10: {
                                Intent intent = new Intent(activity, Loans.class);
                                view.getContext().startActivity(intent);
                                break;
                            }
                            case 11: {
                                Intent intent = new Intent(activity, Credit.class);
                                view.getContext().startActivity(intent);
                                break;
                            }
                            case 12: {
                                Intent intent = new Intent(activity, Cards.class);
                                view.getContext().startActivity(intent);
                                break;
                            }
                            case 13: {
                                Intent intent = new Intent(activity, CreditHistory.class);
                                view.getContext().startActivity(intent);
                                break;
                            }
                            case 15: {
                                Intent intent = new Intent(activity, Rko.class);
                                view.getContext().startActivity(intent);
                                break;
                            }
                            case 16: {
                                Intent intent = new Intent(activity, Insurance.class);
                                view.getContext().startActivity(intent);
                                break;
                            }

                            default: {
                                goDefault = true;
                                break;
                            }
                        }
                        if (goDefault) {
                            Toast.makeText(activity, "Не реализовано!!!", Toast.LENGTH_SHORT).show();
                        }
                    }
                    if (drawerItem instanceof Nameable) {
                        toolbar.setTitle(((Nameable) drawerItem).getName().getText(activity));
                    }
                    return false;
                })
                .build();
    }

    private static boolean between(long i) {
        return (i >= 10 && i <= 17);
    }

}
