package ru.zaimme.zaim.Model;

@SuppressWarnings("unused")
public class OfferDetailItem {

    private String description;

    private String image;

    private String money;

    private String name;

    private String percent;

    private String time;

    private String url;

    public OfferDetailItem() {
    }

    public OfferDetailItem(final String description, final String image, final String money, final String name,
            final String percent, final String time,
            final String url) {

        this.description = description;
        this.image = image;
        this.money = money;
        this.name = name;
        this.percent = percent;
        this.time = time;
        this.url = url;
    }

    public String getDescription() {

        return description;
    }

    public void setDescription(final String description) {
        this.description = description;
    }

    public String getImage() {
        return image;
    }

    public void setImage(final String image) {
        this.image = image;
    }

    public String getMoney() {
        return money;
    }

    public void setMoney(final String money) {
        this.money = money;
    }

    public String getName() {
        return name;
    }

    public void setName(final String name) {
        this.name = name;
    }

    public String getPercent() {
        return percent;
    }

    public void setPercent(final String percent) {
        this.percent = percent;
    }

    public String getTime() {
        return time;
    }

    public void setTime(final String time) {
        this.time = time;
    }

    public String getUrl() {
        return url;
    }

    public void setUrl(final String url) {
        this.url = url;
    }
}
