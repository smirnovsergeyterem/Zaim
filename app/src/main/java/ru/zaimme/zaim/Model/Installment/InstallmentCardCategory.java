package ru.zaimme.zaim.Model.Installment;

import java.util.List;

@SuppressWarnings("unused")
public class InstallmentCardCategory {

    private List<InstallmentCardCategoryItem> cards;

    private String name;

    public InstallmentCardCategory() {

    }

    public InstallmentCardCategory(final String name,
            final List<InstallmentCardCategoryItem> cards) {

        this.name = name;
        this.cards = cards;
    }

    public List<InstallmentCardCategoryItem> getCards() {
        return cards;
    }

    public void setCards(final List<InstallmentCardCategoryItem> cards) {
        this.cards = cards;
    }

    public String getName() {
        return name;
    }

    public void setName(final String name) {
        this.name = name;
    }
}
