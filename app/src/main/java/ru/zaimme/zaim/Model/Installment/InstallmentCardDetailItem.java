package ru.zaimme.zaim.Model.Installment;

@SuppressWarnings("unused")
public class InstallmentCardDetailItem {

    private String additionalInfo;

    private String decisionTime;

    private String documents;

    private String image;

    private String installment;

    private String license;

    private String percent;

    private String subTitle;

    private String sum;

    private String title;

    private String url;

    public InstallmentCardDetailItem() {

    }

    public InstallmentCardDetailItem(
            final String title,
            final String subTitle,
            final String installment,
            final String percent,
            final String sum,
            final String additionalInfo,
            final String decisionTime,
            final String documents,
            final String image,
            final String license,
            final String url
    ) {

        this.additionalInfo = additionalInfo;
        this.decisionTime = decisionTime;
        this.documents = documents;
        this.image = image;
        this.installment = installment;
        this.license = license;
        this.percent = percent;
        this.subTitle = subTitle;
        this.sum = sum;
        this.title = title;
        this.url = url;
    }

    public String getAdditionalInfo() {
        return additionalInfo;
    }

    public void setAdditionalInfo(final String additionalInfo) {
        this.additionalInfo = additionalInfo;
    }

    public String getDecisionTime() {
        return decisionTime;
    }

    public void setDecisionTime(final String decisionTime) {
        this.decisionTime = decisionTime;
    }

    public String getDocuments() {
        return documents;
    }

    public void setDocuments(final String documents) {
        this.documents = documents;
    }

    public String getImage() {
        return image;
    }

    public void setImage(final String image) {
        this.image = image;
    }

    public String getInstallment() {
        return installment;
    }

    public void setInstallment(final String installment) {
        this.installment = installment;
    }

    public String getLicense() {
        return license;
    }

    public void setLicense(final String license) {
        this.license = license;
    }

    public String getPercent() {
        return percent;
    }

    public void setPercent(final String percent) {
        this.percent = percent;
    }

    public String getSubTitle() {
        return subTitle;
    }

    public void setSubTitle(final String subTitle) {
        this.subTitle = subTitle;
    }

    public String getSum() {
        return sum;
    }

    public void setSum(final String sum) {
        this.sum = sum;
    }

    public String getTitle() {
        return title;
    }

    public void setTitle(final String title) {
        this.title = title;
    }

    public String getUrl() {
        return url;
    }

    public void setUrl(final String url) {
        this.url = url;
    }
}
