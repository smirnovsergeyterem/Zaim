package ru.zaimme.zaim.Model.Cards;

@SuppressWarnings("unused")
public class CardDetailItem {

    private String additionalInfo;

    private String bonus;

    private String decisionTime;

    private String image;

    private String percent;

    private String subTitle;

    private String sum;

    private String title;

    private String url;

    public CardDetailItem() {

    }

    public CardDetailItem(
            final String title,
            final String subTitle,
            final String bonus,
            final String sum,
            final String percent,
            final String decisionTime,
            final String image,
            final String url,
            final String additionalInfo
    ) {
        this.additionalInfo = additionalInfo;
        this.bonus = bonus;
        this.decisionTime = decisionTime;
        this.image = image;
        this.percent = percent;
        this.subTitle = subTitle;
        this.sum = sum;
        this.title = title;
        this.url = url;
    }

    public String getAdditionalInfo() {
        return additionalInfo;
    }

    public void setAdditionalInfo(final String additionalInfo) {
        this.additionalInfo = additionalInfo;
    }

    public String getBonus() {
        return bonus;
    }

    public void setBonus(final String bonus) {
        this.bonus = bonus;
    }

    public String getDecisionTime() {
        return decisionTime;
    }

    public void setDecisionTime(final String decisionTime) {
        this.decisionTime = decisionTime;
    }

    public String getImage() {
        return image;
    }

    public void setImage(final String image) {
        this.image = image;
    }

    public String getPercent() {
        return percent;
    }

    public void setPercent(final String percent) {
        this.percent = percent;
    }

    public String getSubTitle() {
        return subTitle;
    }

    public void setSubTitle(final String subTitle) {
        this.subTitle = subTitle;
    }

    public String getSum() {
        return sum;
    }

    public void setSum(final String sum) {
        this.sum = sum;
    }

    public String getTitle() {
        return title;
    }

    public void setTitle(final String title) {
        this.title = title;
    }

    public String getUrl() {
        return url;
    }

    public void setUrl(final String url) {
        this.url = url;
    }
}
