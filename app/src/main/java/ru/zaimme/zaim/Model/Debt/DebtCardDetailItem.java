package ru.zaimme.zaim.Model.Debt;

@SuppressWarnings("unused")
public class DebtCardDetailItem {

    private String additionalInfo;

    private String description;

    private String image;

    private String license;

    private String subTitle;

    private String title;

    private String url;

    public DebtCardDetailItem() {

    }

    public DebtCardDetailItem(String title, String subTitle, String description, String image, String additionalInfo,
            String url, String license) {

        this.title = title;
        this.image = image;
        this.subTitle = subTitle;
        this.description = description;
        this.additionalInfo = additionalInfo;
        this.url = url;
        this.license = license;
    }

    public String getAdditionalInfo() {
        return additionalInfo;
    }

    public void setAdditionalInfo(String additionalInfo) {
        this.additionalInfo = additionalInfo;
    }

    public String getDescription() {
        return description;
    }

    public void setDescription(String description) {
        this.description = description;
    }

    public String getImage() {
        return image;
    }

    public void setImage(String image) {
        this.image = image;
    }

    public String getLicense() {
        return license;
    }

    public void setLicense(String license) {
        this.license = license;
    }

    public String getSubTitle() {
        return subTitle;
    }

    public void setSubTitle(String subTitle) {
        this.subTitle = subTitle;
    }

    public String getTitle() {
        return title;
    }

    public void setTitle(String title) {
        this.title = title;
    }

    public String getUrl() {
        return url;
    }

    public void setUrl(String url) {
        this.url = url;
    }
}
