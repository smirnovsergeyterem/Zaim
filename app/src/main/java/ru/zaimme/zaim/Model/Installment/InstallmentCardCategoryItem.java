package ru.zaimme.zaim.Model.Installment;

@SuppressWarnings("unused")
public class InstallmentCardCategoryItem {

    private String decisionTime;

    private String id;

    private String image;

    private String installment;

    private String percent;

    private String subTitle;

    private String sum;

    private String title;

    public InstallmentCardCategoryItem() {
    }

    public InstallmentCardCategoryItem(
            final String title,
            final String subTitle,
            final String decisionTime,
            final String image,
            final String installment,
            final String percent,
            final String sum,
            final String id
    ) {

        this.decisionTime = decisionTime;
        this.id = id;
        this.image = image;
        this.installment = installment;
        this.percent = percent;
        this.subTitle = subTitle;
        this.sum = sum;
        this.title = title;
    }

    public String getDecisionTime() {

        return decisionTime;
    }

    public void setDecisionTime(final String decisionTime) {
        this.decisionTime = decisionTime;
    }

    public String getId() {
        return id;
    }

    public void setId(final String id) {
        this.id = id;
    }

    public String getImage() {
        return image;
    }

    public void setImage(final String image) {
        this.image = image;
    }

    public String getInstallment() {
        return installment;
    }

    public void setInstallment(final String installment) {
        this.installment = installment;
    }

    public String getPercent() {
        return percent;
    }

    public void setPercent(final String percent) {
        this.percent = percent;
    }

    public String getSubTitle() {
        return subTitle;
    }

    public void setSubTitle(final String subTitle) {
        this.subTitle = subTitle;
    }

    public String getSum() {
        return sum;
    }

    public void setSum(final String sum) {
        this.sum = sum;
    }

    public String getTitle() {
        return title;
    }

    public void setTitle(final String title) {
        this.title = title;
    }
}
