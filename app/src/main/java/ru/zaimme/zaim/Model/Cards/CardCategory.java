package ru.zaimme.zaim.Model.Cards;

import java.util.List;

@SuppressWarnings("unused")
public class CardCategory {
    private String name;

    private List<CardCategoryItem> cards;

    public CardCategory() {
    }

    public String getName() {
        return name;
    }

    public void setName(final String name) {
        this.name = name;
    }

    public List<CardCategoryItem> getCards() {
        return cards;
    }

    public void setCards(final List<CardCategoryItem> cards) {
        this.cards = cards;
    }

    public CardCategory(final String name, final List<CardCategoryItem> cards) {

        this.name = name;
        this.cards = cards;
    }
}
