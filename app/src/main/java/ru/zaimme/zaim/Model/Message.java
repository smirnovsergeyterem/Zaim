package ru.zaimme.zaim.Model;

@SuppressWarnings("unused")
public class Message {

    private String date;

    private String message;

    private String theme;

    public Message() {
    }

    public Message(final String theme, final String message) {
        this.theme = theme;
        this.message = message;
        this.date = String.valueOf(System.currentTimeMillis());
    }

    public String getDate() {
        return date;
    }

    public void setDate(final String date) {
        this.date = date;
    }

    public String getMessage() {
        return message;
    }

    public void setMessage(final String message) {
        this.message = message;
    }

    public String getTheme() {
        return theme;
    }

    public void setTheme(final String theme) {
        this.theme = theme;
    }
}
