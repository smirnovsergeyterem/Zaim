package ru.zaimme.zaim.Model.Debt;

import java.util.List;

@SuppressWarnings("unused")
public class DebtCardCategory {

    private List<DebtCardCategoryItem> cards;

    private String name;

    public DebtCardCategory() {

    }

    public DebtCardCategory(String name, List<DebtCardCategoryItem> cards) {

        this.name = name;
        this.cards = cards;
    }

    public List<DebtCardCategoryItem> getCards() {
        return cards;
    }

    public void setCards(List<DebtCardCategoryItem> cards) {
        this.cards = cards;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }
}
