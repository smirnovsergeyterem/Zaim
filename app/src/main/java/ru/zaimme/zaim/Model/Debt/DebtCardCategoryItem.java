package ru.zaimme.zaim.Model.Debt;

@SuppressWarnings("unused")
public class DebtCardCategoryItem {

    private String description;

    private String id;

    private String image;

    private String subTitle;

    private String title;

    public DebtCardCategoryItem() {
    }

    public DebtCardCategoryItem(String title, String subTitle, String description, String image, String id) {

        this.title = title;
        this.image = image;
        this.subTitle = subTitle;
        this.description = description;
        this.id = id;
    }

    public String getDescription() {
        return description;
    }

    public void setDescription(String description) {
        this.description = description;
    }

    public String getId() {
        return id;
    }

    public void setId(String id) {
        this.id = id;
    }

    public String getImage() {
        return image;
    }

    public void setImage(String image) {
        this.image = image;
    }

    public String getSubTitle() {
        return subTitle;
    }

    public void setSubTitle(String subTitle) {
        this.subTitle = subTitle;
    }

    public String getTitle() {
        return title;
    }

    public void setTitle(String title) {
        this.title = title;
    }
}
