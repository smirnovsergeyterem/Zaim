package ru.zaimme.zaim.Model;

import com.google.firebase.iid.FirebaseInstanceId;

@SuppressWarnings("unused")
public class UserInfo {

    private String city;

    private Boolean notification_email = true;

    private Boolean notification_push = true;

    private Boolean notification_theme_action = true;

    private Boolean notification_theme_bank_news = true;

    private Boolean notification_theme_mfo_news = true;

    private Boolean notification_theme_promocodes = true;

    private String region;

    private String token = FirebaseInstanceId.getInstance().getToken();

    public UserInfo() {
    }

    public UserInfo(final String city, final Boolean notification_email, final Boolean notification_push,
            final Boolean notification_theme_action, final Boolean notification_theme_bank_news,
            final Boolean notification_theme_mfo_news, final Boolean notification_theme_promocodes,
            final String region) {
        this.city = city;
        this.notification_email = notification_email;
        this.notification_push = notification_push;
        this.notification_theme_action = notification_theme_action;
        this.notification_theme_bank_news = notification_theme_bank_news;
        this.notification_theme_mfo_news = notification_theme_mfo_news;
        this.notification_theme_promocodes = notification_theme_promocodes;
        this.region = region;
    }

    public String getCity() {

        return city;
    }

    public void setCity(final String city) {
        this.city = city;
    }

    public Boolean getNotification_email() {
        return notification_email;
    }

    public void setNotification_email(final Boolean notification_email) {
        this.notification_email = notification_email;
    }

    public Boolean getNotification_push() {
        return notification_push;
    }

    public void setNotification_push(final Boolean notification_push) {
        this.notification_push = notification_push;
    }

    public Boolean getNotification_theme_action() {
        return notification_theme_action;
    }

    public void setNotification_theme_action(final Boolean notification_theme_action) {
        this.notification_theme_action = notification_theme_action;
    }

    public Boolean getNotification_theme_bank_news() {
        return notification_theme_bank_news;
    }

    public void setNotification_theme_bank_news(final Boolean notification_theme_bank_news) {
        this.notification_theme_bank_news = notification_theme_bank_news;
    }

    public Boolean getNotification_theme_mfo_news() {
        return notification_theme_mfo_news;
    }

    public void setNotification_theme_mfo_news(final Boolean notification_theme_mfo_news) {
        this.notification_theme_mfo_news = notification_theme_mfo_news;
    }

    public Boolean getNotification_theme_promocodes() {
        return notification_theme_promocodes;
    }

    public void setNotification_theme_promocodes(final Boolean notification_theme_promocodes) {
        this.notification_theme_promocodes = notification_theme_promocodes;
    }

    public String getRegion() {
        return region;
    }

    public void setRegion(final String region) {
        this.region = region;
    }

    public String getToken() {
        return token;
    }

    public void setToken(String token) {
        this.token = token;
    }
}
