package ru.zaimme.zaim.Model;

import java.io.Serializable;

@SuppressWarnings("unused")
public class CreditHistoryModel implements Serializable {

    private String image;

    private String paragraph_text1;

    private String paragraph_text2;

    private String paragraph_title1;

    private String paragraph_title2;

    private String title;

    private String url;

    public CreditHistoryModel() {

    }

    public CreditHistoryModel(
            final String image,
            final String url,
            final String title,
            final String paragraph_title1,
            final String paragraph_text1,
            final String paragraph_title2,
            final String paragraph_text2
    ) {

        this.image = image;
        this.url = url;
        this.title = title;
        this.paragraph_text1 = paragraph_text1;
        this.paragraph_text2 = paragraph_text2;
        this.paragraph_title1 = paragraph_title1;
        this.paragraph_title2 = paragraph_title2;
    }

    public String getImage() {
        return image;
    }

    public void setImage(final String image) {
        this.image = image;
    }

    public String getParagraph_text1() {
        return paragraph_text1;
    }

    public void setParagraph_text1(final String paragraph_text1) {
        this.paragraph_text1 = paragraph_text1;
    }

    public String getParagraph_text2() {
        return paragraph_text2;
    }

    public void setParagraph_text2(final String paragraph_text2) {
        this.paragraph_text2 = paragraph_text2;
    }

    public String getParagraph_title1() {
        return paragraph_title1;
    }

    public void setParagraph_title1(final String paragraph_title1) {
        this.paragraph_title1 = paragraph_title1;
    }


    public String getParagraph_title2() {
        return paragraph_title2;
    }

    public void setParagraph_title2(final String paragraph_title2) {
        this.paragraph_title2 = paragraph_title2;
    }

    public String getTitle() {
        return title;
    }

    public void setTitle(final String title) {
        this.title = title;
    }

    public String getUrl() {
        return url;
    }

    public void setUrl(final String url) {
        this.url = url;
    }
}
