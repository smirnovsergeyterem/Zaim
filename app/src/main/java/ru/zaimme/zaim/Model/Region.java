package ru.zaimme.zaim.Model;

import java.util.ArrayList;

@SuppressWarnings("unused")
public class Region {

    private ArrayList<City> cities;

    private String id;

    private String name;

    public Region() {

    }

    public Region(final String id, final String name, final ArrayList<City> citiesList) {

        this.id = id;
        this.name = name;
        this.cities = citiesList;
    }

    public ArrayList<City> getCities() {
        return cities;
    }

    public void setCities(final ArrayList<City> cities) {
        this.cities = cities;
    }

    public String getId() {
        return id;
    }

    public void setId(final String id) {
        this.id = id;
    }

    public String getName() {
        return name;
    }

    public void setName(final String name) {
        this.name = name;
    }

    @Override
    public String toString() {
        return name;
    }

}
