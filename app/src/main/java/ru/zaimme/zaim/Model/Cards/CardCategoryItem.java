package ru.zaimme.zaim.Model.Cards;

@SuppressWarnings("unused")
public class CardCategoryItem {

    private String bonus;

    private String decisionTime;

    private String id;

    private String image;

    private String percent;

    private String subTitle;

    private String sum;

    private String title;

    private String url;

    public CardCategoryItem() {

    }

    public CardCategoryItem(
            final String bonus,
            final String decisionTime,
            final String id,
            final String image,
            final String url,
            final String percent,
            final String subTitle,
            final String sum,
            final String title) {

        this.bonus = bonus;
        this.decisionTime = decisionTime;
        this.id = id;
        this.image = image;
        this.url = url;
        this.percent = percent;
        this.subTitle = subTitle;
        this.sum = sum;
        this.title = title;
    }

    public String getBonus() {
        return bonus;
    }

    public void setBonus(final String bonus) {
        this.bonus = bonus;
    }

    public String getDecisionTime() {
        return decisionTime;
    }

    public void setDecisionTime(final String decisionTime) {
        this.decisionTime = decisionTime;
    }

    public String getId() {
        return id;
    }

    public void setId(final String id) {
        this.id = id;
    }

    public String getImage() {
        return image;
    }

    public void setImage(final String image) {
        this.image = image;
    }

    public String getPercent() {
        return percent;
    }

    public void setPercent(final String percent) {
        this.percent = percent;
    }

    public String getSubTitle() {
        return subTitle;
    }

    public void setSubTitle(final String subTitle) {
        this.subTitle = subTitle;
    }

    public String getSum() {
        return sum;
    }

    public void setSum(final String sum) {
        this.sum = sum;
    }

    public String getTitle() {
        return title;
    }

    public void setTitle(final String title) {
        this.title = title;
    }

    public String getUrl() {
        return url;
    }

    public void setUrl(final String url) {
        this.url = url;
    }
}
