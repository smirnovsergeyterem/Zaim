package ru.zaimme.zaim.Model;

@SuppressWarnings("unused")
public class City {

    private String cityName;

    public City() {

    }

    public City(final String cityName) {

        this.cityName = cityName;
    }

    public String getCityName() {
        return cityName;
    }

    public void setCityName(final String cityName) {
        this.cityName = cityName;
    }

    @Override
    public String toString() {
        return cityName;
    }
}
