package ru.zaimme.zaim.Model;

@SuppressWarnings("unused")
public class RkoDetailModel {

    private String account;

    private String advantages;

    private String description;

    private String image;

    private String subtitle;

    private String title;

    private String url;

    public RkoDetailModel() {

    }

    public RkoDetailModel(
            final String title,
            final String subtitle,
            final String description,
            final String image,

            final String account,
            final String advantages,
            final String url
    ) {
        this.account = account;
        this.advantages = advantages;
        this.description = description;
        this.image = image;
        this.subtitle = subtitle;
        this.title = title;
        this.url = url;
    }

    public String getAccount() {
        return account;
    }

    public void setAccount(final String account) {
        this.account = account;
    }

    public String getAdvantages() {
        return advantages;
    }

    public void setAdvantages(final String advantages) {
        this.advantages = advantages;
    }

    public String getDescription() {
        return description;
    }

    public void setDescription(final String description) {
        this.description = description;
    }

    public String getImage() {
        return image;
    }

    public void setImage(final String image) {
        this.image = image;
    }

    public String getSubtitle() {
        return subtitle;
    }

    public void setSubtitle(final String subtitle) {
        this.subtitle = subtitle;
    }

    public String getTitle() {
        return title;
    }

    public void setTitle(final String title) {
        this.title = title;
    }

    public String getUrl() {
        return url;
    }

    public void setUrl(final String url) {
        this.url = url;
    }

}
