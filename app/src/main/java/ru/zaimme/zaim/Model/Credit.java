package ru.zaimme.zaim.Model;

@SuppressWarnings("unused")
public class Credit {

    private String image;

    private String subtitle;

    private String text;

    private String title;

    public Credit() {
    }

    public Credit(final String title, final String subtitle, final String image, final String text) {

        this.image = image;
        this.subtitle = subtitle;
        this.text = text;
        this.title = title;
    }

    public String getImage() {

        return image;
    }

    public void setImage(final String image) {
        this.image = image;
    }

    public String getSubtitle() {
        return subtitle;
    }

    public void setSubtitle(final String subtitle) {
        this.subtitle = subtitle;
    }

    public String getText() {
        return text;
    }

    public void setText(final String text) {
        this.text = text;
    }

    public String getTitle() {
        return title;
    }

    public void setTitle(final String title) {
        this.title = title;
    }
}
