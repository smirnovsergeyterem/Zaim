package ru.zaimme.zaim.Model;

@SuppressWarnings("unused")
public class RkoModel {

    private String description;

    private String image;

    private String subtitle;

    private String title;

    public RkoModel() {

    }

    public RkoModel(final String image, final String title, final String subtitle, final String description) {
        this.description = description;
        this.image = image;
        this.subtitle = subtitle;
        this.title = title;
    }

    public String getDescription() {

        return description;
    }

    public void setDescription(final String description) {
        this.description = description;
    }

    public String getImage() {
        return image;
    }

    public void setImage(final String image) {
        this.image = image;
    }

    public String getSubtitle() {
        return subtitle;
    }

    public void setSubtitle(final String subtitle) {
        this.subtitle = subtitle;
    }

    public String getTitle() {
        return title;
    }

    public void setTitle(final String title) {
        this.title = title;
    }
}
