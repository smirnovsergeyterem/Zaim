package ru.zaimme.zaim.Model;

import java.util.List;

@SuppressWarnings("unused")
public class newCategory {

    public String name;

    public List<OfferCategoryItem> offers;

    public newCategory() {
    }

    public newCategory(final String name, final List<OfferCategoryItem> offers) {
        this.name = name;
        this.offers = offers;
    }

    public String getName() {
        return name;
    }

    public void setName(final String name) {
        this.name = name;
    }

    public List<OfferCategoryItem> getOffers() {
        return offers;
    }

    public void setOffers(final List<OfferCategoryItem> offers) {
        this.offers = offers;
    }
}
