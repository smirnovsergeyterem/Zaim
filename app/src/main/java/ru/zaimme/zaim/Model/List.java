package ru.zaimme.zaim.Model;

@SuppressWarnings("unused")
public class List {

    public String detail;

    public String title;

    public List() {
    }

    public List(final String title, final String detail) {
        this.title = title;
        this.detail = detail;
    }

    public String getDetail() {
        return detail;
    }

    public void setDetail(final String detail) {
        this.detail = detail;
    }

    public String getTitle() {
        return title;
    }

    public void setTitle(final String title) {
        this.title = title;
    }
}
