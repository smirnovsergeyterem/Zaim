package ru.zaimme.zaim.Model;

@SuppressWarnings("unused")
public class CreditDetail {

    private String age;

    private String bankDescription;

    private String decision;

    private String documents;

    private String image;

    private String issue;

    private String percent;

    private String period;

    private String shortDescription;

    private String sum;

    private String url;

    public CreditDetail() {
    }

    public CreditDetail(
            final String sum,
            final String stavka,
            final String maxSrok,
            final String documents,
            final String desicionTime,
            final String age,

            final String shortDescription,
            final String dopolnitelno,
            final String portret,
            final String url,
            final String image
    ) {
        this.image = image;
        this.age = age;
        this.bankDescription = dopolnitelno;
        this.decision = desicionTime;
        this.documents = documents;
        this.issue = portret;
        this.percent = stavka;
        this.period = maxSrok;
        this.shortDescription = shortDescription;
        this.sum = sum;
        this.url = url;
    }

    public String getAge() {
        return age;
    }

    public void setAge(final String age) {
        this.age = age;
    }

    public String getBankDescription() {
        return bankDescription;
    }

    public void setBankDescription(final String bankDescription) {
        this.bankDescription = bankDescription;
    }

    public String getDecision() {
        return decision;
    }

    public void setDecision(final String decision) {
        this.decision = decision;
    }

    public String getDocuments() {
        return documents;
    }

    public void setDocuments(final String documents) {
        this.documents = documents;
    }

    public String getImage() {
        return image;
    }

    public void setImage(final String image) {
        this.image = image;
    }

    public String getIssue() {
        return issue;
    }

    public void setIssue(final String issue) {
        this.issue = issue;
    }

    public String getPercent() {
        return percent;
    }

    public void setPercent(final String percent) {
        this.percent = percent;
    }

    public String getPeriod() {
        return period;
    }

    public void setPeriod(final String period) {
        this.period = period;
    }

    public String getShortDescription() {
        return shortDescription;
    }

    public void setShortDescription(final String shortDescription) {
        this.shortDescription = shortDescription;
    }

    public String getSum() {
        return sum;
    }

    public void setSum(final String sum) {
        this.sum = sum;
    }

    public String getUrl() {
        return url;
    }

    public void setUrl(final String url) {
        this.url = url;
    }
}
