package ru.zaimme.zaim;

import android.annotation.SuppressLint;
import android.graphics.Color;
import android.os.Bundle;
import android.support.design.widget.TabLayout;
import android.support.design.widget.TabLayout.OnTabSelectedListener;
import android.support.design.widget.TabLayout.Tab;
import android.support.v7.app.AppCompatActivity;
import android.support.v7.widget.Toolbar;
import android.util.Log;
import android.view.MenuItem;
import android.webkit.WebView;
import com.mikepenz.fontawesome_typeface_library.FontAwesome.Icon;
import com.mikepenz.iconics.IconicsDrawable;
import java.util.Objects;

public class Insurance extends AppCompatActivity {

    private static final String TAG = "Insurance";

    private TabLayout mTabLayout;

    private final String urlKasko
            = "https://www.sravni.ru/f/widget-auto.html#zaimme.ru::::::false:kasko:false::false:false:false:false";

    private final String urlOsago
            = "https://www.sravni.ru/f/widget-auto.html#zaimme.ru::::::false:osago:false::false:false:false:false";

    private final String urlTravel = "https://www.sravni.ru/widget-vzr/";

    private WebView webview;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_insurance);

        // Handle Toolbar
        Toolbar toolbar = findViewById(R.id.toolbar);
        setSupportActionBar(toolbar);
        //set the back arrow in the toolbar
        Objects.requireNonNull(getSupportActionBar()).setDisplayHomeAsUpEnabled(true);
        getSupportActionBar().setTitle("Страхование");
        getSupportActionBar().setHomeAsUpIndicator(
                new IconicsDrawable(this, Icon.faw_arrow_left)
                        .color(Color.WHITE)
                        .sizeDp(IconicsDrawable.TOOLBAR_ICON_SIZE)
                        .paddingDp(IconicsDrawable.TOOLBAR_ICON_PADDING)
        );

        initWebView();
        initTabs();
        loadWebViewPage(mTabLayout.getSelectedTabPosition());
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        switch (item.getItemId()) {
            case android.R.id.home:
                onBackPressed();
                return true;

            default:
                return super.onOptionsItemSelected(item);
        }
    }

    private void initTabs() {
        mTabLayout = findViewById(R.id.tabs);
        mTabLayout.addOnTabSelectedListener(new OnTabSelectedListener() {
            @Override
            public void onTabReselected(final Tab tab) {
            }

            @Override
            public void onTabSelected(final Tab tab) {
                loadWebViewPage(tab.getPosition());
            }

            @Override
            public void onTabUnselected(final Tab tab) {
            }
        });
    }

    @SuppressLint("SetJavaScriptEnabled")
    private void initWebView() {
        webview = findViewById(R.id.webview);
        webview.getSettings().setJavaScriptEnabled(true);
    }

    private void loadWebViewPage(final int position) {
        Log.d(TAG, "loadWebViewPage: " + position);
        switch (position) {
            case 0: {
                webview.loadUrl(urlKasko);
                break;
            }
            case 1: {
                webview.loadUrl(urlOsago);
                break;
            }
            case 2: {
                webview.loadUrl(urlTravel);
                break;
            }
            default:
                break;
        }
    }
}
