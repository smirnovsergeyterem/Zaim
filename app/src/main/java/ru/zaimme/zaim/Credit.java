package ru.zaimme.zaim;

import android.content.Intent;
import android.graphics.Color;
import android.os.Bundle;
import android.support.annotation.NonNull;
import android.support.v7.app.AppCompatActivity;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.support.v7.widget.Toolbar;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.MenuItem;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Toast;
import com.bumptech.glide.Glide;
import com.bumptech.glide.request.RequestOptions;
import com.firebase.ui.database.FirebaseRecyclerAdapter;
import com.firebase.ui.database.FirebaseRecyclerOptions;
import com.google.firebase.database.DataSnapshot;
import com.google.firebase.database.DatabaseError;
import com.google.firebase.database.DatabaseReference;
import com.google.firebase.database.FirebaseDatabase;
import com.google.firebase.database.Query;
import com.google.firebase.database.ValueEventListener;
import com.mikepenz.fontawesome_typeface_library.FontAwesome.Icon;
import com.mikepenz.iconics.IconicsDrawable;
import com.synnapps.carouselview.CarouselView;
import com.synnapps.carouselview.ImageListener;
import java.util.ArrayList;
import java.util.Objects;
import ru.zaimme.zaim.ViewHolder.CreditViewHolder;

public class Credit extends AppCompatActivity {

    private static final String TAG = "Credit";

    private CarouselView carouselView;

    private final ArrayList<String> mAdsImagesList = new ArrayList<>();

    private DatabaseReference mAdsRef;

    private DatabaseReference mCredit;

    private FirebaseRecyclerAdapter<ru.zaimme.zaim.Model.Credit, CreditViewHolder> mCreditAdapter;

    private ImageListener mImageListener;

    private RecyclerView mRecyclerView;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_credit);

        // Handle Toolbar
        Toolbar toolbar = findViewById(R.id.toolbar);
        setSupportActionBar(toolbar);
        //set the back arrow in the toolbar
        Objects.requireNonNull(getSupportActionBar()).setDisplayHomeAsUpEnabled(true);
        getSupportActionBar().setTitle("Кредиты");
        getSupportActionBar().setHomeAsUpIndicator(
                new IconicsDrawable(this, Icon.faw_arrow_left)
                        .color(Color.WHITE)
                        .sizeDp(IconicsDrawable.TOOLBAR_ICON_SIZE)
                        .paddingDp(IconicsDrawable.TOOLBAR_ICON_PADDING)
        );

        initFirebase();
        initCarousel();
        initRecyclerView();
        loadCreditList();
    }

    @Override
    protected void onResume() {
        super.onResume();
        if (mCreditAdapter != null) {
            mCreditAdapter.startListening();
        }
    }

    @Override
    protected void onPostResume() {
        super.onPostResume();
        carouselView.playCarousel();
    }

    @Override
    protected void onPause() {
        carouselView.pauseCarousel();
        super.onPause();
    }

    @Override
    protected void onStop() {
        super.onStop();
        if (mCreditAdapter != null) {
            mCreditAdapter.stopListening();
        }
    }

    @Override
    protected void onDestroy() {
        super.onDestroy();
        Glide.get(this).clearMemory();
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        switch (item.getItemId()) {
            case android.R.id.home:
                onBackPressed();
                return true;

            default:
                return super.onOptionsItemSelected(item);
        }
    }

    private void getAdsImages() {
        mAdsRef.addListenerForSingleValueEvent(new ValueEventListener() {
            @Override
            public void onCancelled(@NonNull final DatabaseError databaseError) {
                Log.e(TAG, "onCancelled: ", databaseError.toException());
            }

            @Override
            public void onDataChange(@NonNull final DataSnapshot dataSnapshot) {
                for (final DataSnapshot item : dataSnapshot.getChildren()) {
                    mAdsImagesList.add(Objects.requireNonNull(item.getValue()).toString());
                }
                carouselView.setPageCount(mAdsImagesList.size());
                mImageListener = (position, imageView) -> Glide.with(getBaseContext())
                        .load(mAdsImagesList.toArray()[position])
                        .into(imageView);
                carouselView.setImageListener(mImageListener);
            }
        });
    }

    private void initCarousel() {
        //Carousel
        carouselView = findViewById(R.id.carousel);
        mImageListener = (position, imageView) -> Glide.with(getBaseContext())
                .load(mAdsImagesList.toArray()[position])
                .into(imageView);
        carouselView.setImageListener(mImageListener);
        carouselView.setImageClickListener(
                position -> Toast.makeText(this, "Выбран элемент: " + position, Toast.LENGTH_SHORT).show());
        getAdsImages();
    }

    private void initFirebase() {
        final FirebaseDatabase database = FirebaseDatabase.getInstance();
        mAdsRef = database.getReference("Ads");
        mCredit = database.getReference("Credit");
    }

    private void initRecyclerView() {
        //Recycler
        mRecyclerView = findViewById(R.id.creditsList);
        mRecyclerView.setHasFixedSize(true);
        RecyclerView.LayoutManager layoutManager = new LinearLayoutManager(this);
        mRecyclerView.setLayoutManager(layoutManager);
    }

    private void loadCreditList() {
        Query query = mCredit.orderByKey();
        FirebaseRecyclerOptions<ru.zaimme.zaim.Model.Credit> listOptions =
                new FirebaseRecyclerOptions.Builder<ru.zaimme.zaim.Model.Credit>()
                        .setQuery(query, ru.zaimme.zaim.Model.Credit.class)
                        .build();
        mCreditAdapter = new FirebaseRecyclerAdapter<ru.zaimme.zaim.Model.Credit, CreditViewHolder>(listOptions) {
            @NonNull
            @Override
            public CreditViewHolder onCreateViewHolder(@NonNull final ViewGroup viewGroup, final int i) {
                View itemView = LayoutInflater.from(viewGroup.getContext())
                        .inflate(R.layout.credit_item, viewGroup, false);
                return new CreditViewHolder(itemView);
            }

            @Override
            protected void onBindViewHolder(@NonNull final CreditViewHolder holder, final int position,
                    @NonNull final ru.zaimme.zaim.Model.Credit model) {
                holder.txt_title.setText(model.getTitle());
                holder.txt_subtitle.setText(model.getSubtitle());
                holder.txt_description.setText(model.getText());

                Glide.with(getBaseContext())
                        .load(model.getImage())
                        .apply(new RequestOptions().fitCenter())
                        .into(holder.img_logo);

                holder.img_more.setOnClickListener(v -> {
                    Intent creditDetailIntent = new Intent(Credit.this, CreditDetail.class);
                    creditDetailIntent.putExtra("creditId", mCreditAdapter.getRef(position).getKey());
                    creditDetailIntent.putExtra("creditName", mCreditAdapter.getItem(position).getTitle());
                    startActivity(creditDetailIntent);
                });
                holder.setItemClickListener(
                        (View view, int position1, boolean isLongClick) -> Toast
                                .makeText(Credit.this,
                                        "Был выбран оффер: " + mCreditAdapter.getItem(
                                                position1).getTitle(), Toast.LENGTH_SHORT)
                                .show());
            }
        };
        mCreditAdapter.startListening();
        mRecyclerView.setAdapter(mCreditAdapter);

    }
}
