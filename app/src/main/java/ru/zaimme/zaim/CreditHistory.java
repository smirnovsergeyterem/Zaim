package ru.zaimme.zaim;


import android.content.Intent;
import android.graphics.Color;
import android.net.Uri;
import android.os.Bundle;
import android.support.annotation.NonNull;
import android.support.design.widget.TabLayout;
import android.support.design.widget.TabLayout.OnTabSelectedListener;
import android.support.design.widget.TabLayout.Tab;
import android.support.v7.app.AppCompatActivity;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.support.v7.widget.Toolbar;
import android.view.LayoutInflater;
import android.view.MenuItem;
import android.view.View;
import android.view.ViewGroup;
import com.bumptech.glide.Glide;
import com.bumptech.glide.request.RequestOptions;
import com.firebase.ui.database.FirebaseRecyclerAdapter;
import com.firebase.ui.database.FirebaseRecyclerOptions;
import com.google.firebase.database.DatabaseReference;
import com.google.firebase.database.FirebaseDatabase;
import com.google.firebase.database.Query;
import com.mikepenz.fontawesome_typeface_library.FontAwesome.Icon;
import com.mikepenz.iconics.IconicsDrawable;
import java.util.ArrayList;
import java.util.Objects;
import ru.zaimme.zaim.Model.CreditHistoryModel;
import ru.zaimme.zaim.ViewHolder.CreditHistoryViewHolder;

public class CreditHistory extends AppCompatActivity {

    private DatabaseReference mCreditHistory;

    private FirebaseRecyclerAdapter<CreditHistoryModel, CreditHistoryViewHolder> mCreditHistoryAdapter;

    private RecyclerView mRecyclerView;

    private TabLayout mTabLayout;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_credit_history);

        // Handle Toolbar
        Toolbar toolbar = findViewById(R.id.toolbar);
        setSupportActionBar(toolbar);
        //set the back arrow in the toolbar
        Objects.requireNonNull(getSupportActionBar()).setDisplayHomeAsUpEnabled(true);
        getSupportActionBar().setTitle("Кредитная история");
        getSupportActionBar().setHomeAsUpIndicator(
                new IconicsDrawable(this, Icon.faw_arrow_left)
                        .color(Color.WHITE)
                        .sizeDp(IconicsDrawable.TOOLBAR_ICON_SIZE)
                        .paddingDp(IconicsDrawable.TOOLBAR_ICON_PADDING)
        );

        initFirebase();
        initRecyclerView();
        initTabs();
        doAction(mTabLayout.getSelectedTabPosition());
    }

    @Override
    protected void onResume() {
        super.onResume();
        if (mCreditHistoryAdapter != null && mTabLayout.getSelectedTabPosition() == 0) {
            mCreditHistoryAdapter.startListening();
        }
    }

    @Override
    protected void onStop() {
        super.onStop();
        if (mCreditHistoryAdapter != null) {
            mCreditHistoryAdapter.stopListening();
        }
    }

    @Override
    protected void onDestroy() {
        super.onDestroy();
        Glide.get(this).clearMemory();
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        switch (item.getItemId()) {
            case android.R.id.home:
                onBackPressed();
                return true;

            default:
                return super.onOptionsItemSelected(item);
        }
    }

    public void openWebPage(String url) {
        if (mCreditHistoryAdapter != null) {
            mCreditHistoryAdapter.stopListening();
        }
        Uri webpage = Uri.parse(url);
        Intent intent = new Intent(Intent.ACTION_VIEW, webpage);
        if (intent.resolveActivity(getPackageManager()) != null) {
            startActivity(intent);
        }
    }

    private void doAction(final int position) {
        switch (position) {
            case 0: {
                loadList();
                break;
            }
            case 1: {
                openWebPage("https://zaimme.ru/help/besplatno_proverit_kreditnuyu_istoriyu.php");
                break;
            }
            case 2: {
                openWebPage("https://zaimme.ru/help/progresscard_uluchshit_kreditnuyu_istoriyu.php");
                break;
            }
            default:
                break;
        }
    }

    private void initFirebase() {
        final FirebaseDatabase database = FirebaseDatabase.getInstance();
        mCreditHistory = database.getReference("CreditHistory");
    }

    private void initRecyclerView() {
        mRecyclerView = findViewById(R.id.historyList);
        mRecyclerView.setHasFixedSize(true);
        RecyclerView.LayoutManager layoutManager = new LinearLayoutManager(this);
        mRecyclerView.setLayoutManager(layoutManager);
    }

    private void initTabs() {
        mTabLayout = findViewById(R.id.tabs);
        mTabLayout.addOnTabSelectedListener(new OnTabSelectedListener() {
            @Override
            public void onTabReselected(final Tab tab) {
            }

            @Override
            public void onTabSelected(final Tab tab) {
                doAction(tab.getPosition());
            }

            @Override
            public void onTabUnselected(final Tab tab) {
            }
        });
    }

    private void insertData() {
        ArrayList<CreditHistoryModel> list = new ArrayList<>();
        list.add(new CreditHistoryModel(
                "https://zaimme.ru/images/ki-logo/myrate-logo.png",
                "https://zaimme.ru/help/kreditniy_rating_v_moy_rating.php",
                "МойРейтинг",
                "Проверка кредитной истории в режиме «онлайн» без регистрации.",
                "В одном отчете Вы сможете узнать: какие долги у Вас есть, есть ли у Вас просрочки, узнать, почему отказывают банки, как улучшить плохой кредитный рейтинг. Позволяет получить отчет + бесплатно рекомендации по улучшению кредитного рейтинга или оформить подписку на получение отчетов, тем самым Вы сможете контролировать личный кредитный рейтинг в течении полугода.\n"
                        + "\n",
                "Почему важно знать личный кредитный рейтинг?",
                "Знание кредитного рейтинга (КР) важно потому, что помогает не стать жертвой мошенников, снижает риски, ведь банк мог оформить на Вас, к примеру, банковскую карту, которая Вам не принадлежит и не редко уровень вашего КР может быть снижен по ошибке банка - чтобы этого избежать и разобраться в том, как исправить это - поможет сервис \"Мой рейтинг\"."
        ));
        list.add(new CreditHistoryModel(
                "https://zaimme.ru/images/ki-logo/cbki-logo.png",
                "https://zaimme.ru/help/kreditniy_rating_v_bki.php",
                "ЦБКИ",
                "Ваш кредитный рейтинг за 5 минут + рекомендации по исправлению!",
                "Получите разовый онлайн отчет или оформите подписку на 3 месяца, держите на пульсе Ваш кредитный рейтинг! В одном отчете Вы узнаете: Ваши реальные кредиты; просрочки, которые за Вами числятся; Количество денег уже выплаченных банкам; Рекомендации по исправлению рейтинга.",
                "Знание кредитного рейтинга - это отсутствие проблем:",
                "- Отсутствие проблем с картами, которые Вы не активировали, ведь Банки часто оформляют своим клиентам кредитные карты, которые даже без активизации могут числиться как действующий займ и отражаться в кредитном рейтинге.\n"
                        + "\n"
                        + "- Выявление ошибок, которые допустили сами банки - статистика говорит о том, что каждый третий кредитный рейтинг содержит ошибку, допущенную самим банком. Данный сервис поможет исправить эти ошибки."
        ));
        mCreditHistory.setValue(list);
    }

    private void loadList() {
        Query query = mCreditHistory.orderByKey();
        FirebaseRecyclerOptions<CreditHistoryModel> listOptions =
                new FirebaseRecyclerOptions.Builder<CreditHistoryModel>()
                        .setQuery(query, CreditHistoryModel.class)
                        .build();
        mCreditHistoryAdapter = new FirebaseRecyclerAdapter<CreditHistoryModel, CreditHistoryViewHolder>(
                listOptions) {
            @NonNull
            @Override
            public CreditHistoryViewHolder onCreateViewHolder(@NonNull final ViewGroup viewGroup, final int i) {
                View itemView = LayoutInflater.from(viewGroup.getContext())
                        .inflate(R.layout.credit_history_item, viewGroup, false);
                return new CreditHistoryViewHolder(itemView);
            }

            @Override
            protected void onBindViewHolder(@NonNull final CreditHistoryViewHolder holder, final int position,
                    @NonNull final CreditHistoryModel model) {
                holder.title.setText(model.getTitle());
                holder.subtitle.setText(model.getParagraph_title1());

                Glide.with(getBaseContext())
                        .load(model.getImage())
                        .apply(new RequestOptions().fitCenter())
                        .into(holder.logo);
                holder.setItemClickListener((view, position1, isLongClick) -> {
                    Intent creditDetailIntent = new Intent(CreditHistory.this, CreditHistoryDetail.class);
                    creditDetailIntent.putExtra("creditHistoryModel", model);
                    startActivity(creditDetailIntent);
                });
            }
        };
        mCreditHistoryAdapter.startListening();
        mRecyclerView.setAdapter(mCreditHistoryAdapter);
    }
}
