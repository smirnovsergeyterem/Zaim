package ru.zaimme.zaim;

import android.content.Intent;
import android.graphics.Color;
import android.net.Uri;
import android.os.Bundle;
import android.support.annotation.NonNull;
import android.support.v7.app.AppCompatActivity;
import android.support.v7.widget.Toolbar;
import android.util.Log;
import android.view.MenuItem;
import android.view.View;
import android.widget.Button;
import android.widget.TextView;
import android.widget.Toast;
import com.google.firebase.database.DataSnapshot;
import com.google.firebase.database.DatabaseError;
import com.google.firebase.database.DatabaseReference;
import com.google.firebase.database.FirebaseDatabase;
import com.google.firebase.database.ValueEventListener;
import com.mikepenz.fontawesome_typeface_library.FontAwesome.Icon;
import com.mikepenz.iconics.IconicsDrawable;
import java.util.Objects;

public class CreditDetail extends AppCompatActivity {

    private static final String TAG = "CreditDetail";

    private Button btn_request;

    private String creditId;

    private String creditName;

    private DatabaseReference mCreditDetail;

    private TextView txt_age;

    private TextView txt_bankDescription;

    private TextView txt_desicion;

    private TextView txt_documents;

    private TextView txt_issue;

    private TextView txt_issue_title;

    private TextView txt_percent;

    private TextView txt_shortDescription;

    private TextView txt_srok;

    private TextView txt_sum;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_credit_detail);

        // Handle Toolbar
        Toolbar toolbar = findViewById(R.id.toolbar);
        setSupportActionBar(toolbar);
        //set the back arrow in the toolbar
        Objects.requireNonNull(getSupportActionBar()).setDisplayHomeAsUpEnabled(true);
        getSupportActionBar().setTitle("");
        getSupportActionBar().setHomeAsUpIndicator(
                new IconicsDrawable(this, Icon.faw_arrow_left)
                        .color(Color.WHITE)
                        .sizeDp(IconicsDrawable.TOOLBAR_ICON_SIZE)
                        .paddingDp(IconicsDrawable.TOOLBAR_ICON_PADDING)
        );

        initFirebase();
        initUI();
        initListeners();
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        switch (item.getItemId()) {
            case android.R.id.home:
                onBackPressed();
                return true;

            default:
                return super.onOptionsItemSelected(item);
        }
    }

    public void openWebPage(String url) {
        Uri webpage = Uri.parse(url);
        Intent intent = new Intent(Intent.ACTION_VIEW, webpage);
        if (intent.resolveActivity(getPackageManager()) != null) {
            startActivity(intent);
        }
    }

    private void initFirebase() {
        //Firebase Init
        final FirebaseDatabase database = FirebaseDatabase.getInstance();
        mCreditDetail = database.getReference("CreditDetail");
    }

    private void initListeners() {
        //Firebase Listener
        mCreditDetail.orderByKey().equalTo(creditId).addListenerForSingleValueEvent(new ValueEventListener() {
            @Override
            public void onCancelled(@NonNull final DatabaseError databaseError) {
                Toast.makeText(CreditDetail.this, databaseError.getMessage(), Toast.LENGTH_SHORT).show();
            }

            @Override
            public void onDataChange(@NonNull final DataSnapshot dataSnapshot) {
                try {
                    ru.zaimme.zaim.Model.CreditDetail creditDetail;
                    for (DataSnapshot item : dataSnapshot.getChildren()) {
                        creditDetail = item.getValue(ru.zaimme.zaim.Model.CreditDetail.class);
                        //Заголовок
                        getSupportActionBar().setTitle(creditName);
                        txt_sum.setText(creditDetail.getSum());
                        txt_percent.setText(creditDetail.getPercent());
                        txt_srok.setText(creditDetail.getPeriod());
                        txt_documents.setText(creditDetail.getDocuments());
                        txt_desicion.setText(creditDetail.getDecision());
                        txt_age.setText(creditDetail.getAge());
                        txt_shortDescription.setText(creditDetail.getShortDescription());
                        txt_bankDescription.setText(creditDetail.getBankDescription());
                        if (creditDetail.getIssue().isEmpty()) {
                            txt_issue_title.setVisibility(View.GONE);
                        } else {
                            txt_issue_title.setVisibility(View.VISIBLE);
                            txt_issue.setText(creditDetail.getIssue());
                        }
                        final ru.zaimme.zaim.Model.CreditDetail finalCreditDetail = creditDetail;
                        btn_request.setOnClickListener(v -> openWebPage(finalCreditDetail.getUrl()));
                    }
                } catch (Exception e) {
                    String msg = "Ошибка при загрузке кредита с ID=" + creditId;
                    Log.e(TAG, msg, e);
                    Toast.makeText(CreditDetail.this, msg, Toast.LENGTH_SHORT).show();
                }
            }
        });
    }

    private void initUI() {
        btn_request = findViewById(R.id.btn_request);
        txt_sum = findViewById(R.id.txt_sum);
        txt_percent = findViewById(R.id.txt_percent);
        txt_srok = findViewById(R.id.txt_srok);
        txt_documents = findViewById(R.id.txt_documents);
        txt_desicion = findViewById(R.id.txt_desicion);
        txt_age = findViewById(R.id.txt_age);
        txt_shortDescription = findViewById(R.id.txt_shortDescription);
        txt_bankDescription = findViewById(R.id.txt_bankDescription);
        txt_issue = findViewById(R.id.txt_issue);
        txt_issue_title = findViewById(R.id.txt_issue_title);

        //Получение переданного creditId
        if (getIntent() != null) {
            creditId = getIntent().getStringExtra("creditId");
            creditName = getIntent().getStringExtra("creditName");
        }
    }
}
